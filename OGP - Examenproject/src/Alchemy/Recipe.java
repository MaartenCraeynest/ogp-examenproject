package Alchemy;
import java.util.*;
import be.kuleuven.cs.som.annotate.*;
/**
 * A class to create a recipe that can be executed in a laboratory.
 * 
 * @invar	This recipe is stored in a book
 * 			| hasValidRecipeBook()
 * @invar	There are as many ingredients in the recipe as there are ADD 
 * 			commands in the recipe
 * 			| hasValidAmountOfIngredients()
 * 
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 *
 */
public class Recipe {
	/**
	 * Creates a new blank recipe that will be stored in the 
	 * given book.
	 * @param 	fullBook
	 * 			the book where the recipe will be stored
	 * @effect	The recipe is moved to the book
	 * 			| move(book)
	 * @post	The recipe has no commands
	 * 			|this.getNbOfCommands() == 0
	 * @post 	The recipe has no ingredients
	 * 			|this.getNbOfIngredients() == 0
	 */
	public Recipe(RecipeBook fullBook) throws IllegalArgumentException{
		moveRecipe(fullBook);
	}
	
	/*********************************************************************
	 * recipeBook
	 *********************************************************************/
	/**
	 * A variable to keep track of where this recipe is stored.
	 * @invar	The variable is never null (after the object has been initiated)
	 * 			| recipeBook != null
	 */
	private RecipeBook recipeBook = null;
	
	/**
	 * Returns the book where this recipe is stored
	 */
	@Basic
	public RecipeBook getRecipeBook(){
		return recipeBook;
	}
	
	/**
	 * Sets the recipebook where this recipe is stored.
	 * @param 	newBook
	 * 			The new book where this recipe is stored.
	 * @post	The book of this recipe is equal to the given book
	 * 			| new.getRecipeBook() = newBook
	 */
	@Model
	private void setRecipeBook(RecipeBook newBook) {
			recipeBook = newBook;
	}
	
	/**
	 * Checks if the recipeBook of this recipe is valid
	 * @return 	The result is true if the recipebook is not null and contains
	 * 		 	this item
	 * 			| result == getRecipeBook()!= null && getRecipeBook().hasAsRecipe(this) 
	 */
	public boolean hasValidRecipeBook(){
		return getRecipeBook()!= null && getRecipeBook().hasAsRecipe(this);
	}
	
	/*********************************************************************
	 * commandsList
	 *********************************************************************/
	/**
	 * Variable to keep track of every step in the recipe
	 * @invar 	There are no null items in the arrayList
	 * 			| For each RecipeStep step in commandList
	 * 			|	step != null
	 */
	private ArrayList<RecipeStep> commandList = new ArrayList<RecipeStep>();
	
	/**
	 * Returns the number of steps that are currently in the recipe
	 */
	@Basic
	public int getNbOfSteps(){
		return commandList.size();
	}
	
	/**
	 * Get the command at a certain position
	 * @param	index
	 * 			The index of the RecipeStep called for
	 * @throws 	IndexOutOfBoundsException
	 * 			The index is not within range
	 * 			| index <= 0 || index > getNbOfSteps()
	 */
	@Basic
	public RecipeStep getRecipeCommandAt(int index){
		if (index <= 0 || index > getNbOfSteps())
			throw new IndexOutOfBoundsException();
		else
			return commandList.get(index-1);
	}
	
	/**
	 * Adds a command to the commandList
	 * @param	command
	 * 			The command to be added
	 * @param	index
	 * 			Where the command will be added
	 * @throws 	IndexOutOfBoundsException
	 * 			The index is not in bounds
	 * 			| index <= 0 || index > (getNbOfSteps() + 1)	
	 * @post	The number of steps in the recipe is increased by one
	 * 			| new.getNbOfSteps() == getNbOfsteps() + 1
	 * @post	The command at position index will be the given command
	 * 			| new.getRecipeCommandAt(index) == command		
	 * @post	All the commands after the given index have their index incremented by one
	 * 		 	|for each i in index..getNbOfSteps():
	 * 			|	new.getRecipeCommandAt(i+1) == getRecipeCommandAt(i)
	 */
	private void addRecipeCommandAt(RecipeStep command, int index) throws IndexOutOfBoundsException{
		if (index <= 0 || index > (getNbOfSteps()+1))
			throw new IndexOutOfBoundsException();
		commandList.add(index-1, command);
	}
	
	/**
	 * Removes a recipeCommand at the given index
	 * @param 	index
	 * 			The index of the command to be removed
	 * @throws	IndexOutOfBoundsException
	 * 			the index is not within the current bounds
	 * 			| index <= 0 || index > getNbOfSteps() 
	 * @post	The amount of steps of this recipe is decreased by one
	 * 			| new.getNbOfSteps() = getNbOfSteps() -1;
	 * @post	All the indices of the commands after the given index are 
	 * 			decreased by one
	 * 		 	|for each i in (index+1)..getNbOfSteps():
	 * 			|	new.RecipeCommandAt(i-1)== getRecipeCommandAt(i)
	 */		
	private void removeRecipeCommandAt(int index) throws IndexOutOfBoundsException{
		if (index <= 0 || index > getNbOfSteps())
			throw new IndexOutOfBoundsException();
		commandList.remove(index-1);
	}
	/*********************************************************************
	 * ingredientList
	 *********************************************************************/

	/**
	 * A list of strings that keeps track of the ingredients to be added by the 
	 * ADD command
	 * @invar	There are no null variables in the list
	 * 			| for each String str in ingredientList
	 * 			|	str != null
	 * @invar	There is a valid amount of ingredients in this list.
	 * 			| hasValidAmountOfIngredients()
	 */
	private ArrayList<String> ingredientList = new ArrayList<String>();
	
	/**
	 * Returns the ingredient at the position of the index
	 * @throws 	IndexOutOfBoundsException
	 * 			The index is not within the bounds
	 * 			| index <=0 || index > getNbOfIngredients()
	 */
	public String getIngredientAt(int index) throws IndexOutOfBoundsException{
		if (index <=0 || index > getNbOfIngredients())
			throw new IndexOutOfBoundsException();
		else 
			return ingredientList.get(index-1);
	}
	
	/**
	 * Adds an ingredient to the recipe
	 * @param	ingredient
	 * 			the ingredient to be added
	 * @param	index
	 * 			Where the ingredient will be added
	 * @throws 	IllegalArgumentException
	 * 			The ingredient does not follow the format
	 * 			| !isValidIngrdient(ingredient)
	 * @throws	IndexOutOfBoundsException
	 * 			The index is not in bounds
	 * 			| index<= 0 || index > getNbOfIngredients()
	 * @post	The amount of ingredients in this recipe is incremented by one.
	 * 			| new.getNbOfIngredients() == getNbOfIngredients() + 1
	 * @post	The ingredient at the given index is the given ingredient
	 * 			| new.getIngredientAt(index) == ingredient
	 * @post	All the ingredients after the given index have their index incremented by one
	 * 		 	|for each i in index..getNbOfIngredients():
	 * 			|	new.RecipeIngredientAtAt(i+1)==getRecipeIngredientAt(i)
	 */
	@Raw
	private void addIngredientAt(String ingredient, int index) throws IllegalArgumentException, IndexOutOfBoundsException{
		if (!isValidIngredient(ingredient))
			throw new IllegalArgumentException();
		if (index<= 0 || index > getNbOfIngredients()+1)
			throw new IndexOutOfBoundsException();
		
		ingredientList.add(index-1, ingredient);
	}
	
	/**
	 * Removes a ingredient from the ingredientlist at a 
	 * given index
	 * @param	index
	 * 			the index of the ingredient to be removed
	 * @throws	IndexOutOfBoundsException
	 * 			The index is not valid 
	 * 			| index <= 0 || index > getNbOfIngredients()
	 * @post	The number of ingredients in the recipe is reduced
	 * 			by one.
	 * 			| new.getNbOfIngredients() == getNbOfIngredients() - 1
	 * @post	All the indices of the ingredients after the given index are 
	 * 			decreased by one
	 * 		 	|for each i in index+1..getNbOfIngredients():
	 * 			|	new.getRecipeIngredientAtAt(i-1)==getRecipeIngredientAt(i)
	 * 
	 */
	@Raw
	private void removeIngredientAt(int index){
		if (index <= 0 || index > getNbOfIngredients())
			throw new IndexOutOfBoundsException();
		ingredientList.remove(index-1);
	}
	/**
	 * Returns the amount of ingredients that are used in this recipe
	 */
	public int getNbOfIngredients(){
		return ingredientList.size();
	}
	
	/**
	 * Checks if there are a valid amount of ingredients in this recipe
	 * @return 	True if for every ADD command in the recipe there is also
	 * 			an ingredient.
	 * 			|frequency=0;
	 * 			|for i in 0..getNbOfSteps()-1
	 * 			|	if(getCommandAt(i)==RecipeStep.ADD)
	 * 			|		then frequency++
	 * 			|result ==  (frequency==getNbOfIngredients())
	 */
	public boolean hasValidAmountOfIngredients(){
		return Collections.frequency(commandList, RecipeStep.ADD) == ingredientList.size();	
	}
	
	/**
	 * Map containing all valid quantities for an ingredient to have
	 */
	private final static Map<String,Quantity> VALID_QUANTITIES;
	static {
        HashMap<String, Quantity> aMap = new HashMap<String, Quantity>();
        // Add keywords that are not standard containers 
        aMap.put("Drop", new Quantity(Quantity.DROPAMOUNT));
        aMap.put("Drops", new Quantity(Quantity.DROPAMOUNT));
        aMap.put("Pinch", new Quantity(Quantity.PINCHAMOUNT));
        aMap.put("Pinches", new Quantity(Quantity.PINCHAMOUNT));
        
        aMap.put("Storeroom", new Quantity(Quantity.STOREROOMAMOUNT));
        aMap.put("Storerooms", new Quantity(Quantity.STOREROOMAMOUNT));
        // Keywords that are standard containers
        for (StdContainers cont : StdContainers.values()){
        	if (aMap.containsKey(cont.getName()) || aMap.containsKey(cont.getPlural())){
        		// There exist two containers with the same name
        		// Reset the map and keep it empty
        		aMap  = new HashMap<String, Quantity>();
        		// Display error message
        		System.out.println("There are two or more standardcontainers that have the same name");
        		break;
        	}
        	aMap.put(cont.getName(), cont.getCapacity());
        	aMap.put(cont.getPlural(), cont.getCapacity());
        }
        VALID_QUANTITIES = Collections.unmodifiableMap(aMap);
    }
	/**
	 * Return the quantity associated with the given String
	 * @param 	quantity
	 * 			The name of the Quantity or the plural of the name
	 * @return	The Quantity associated with the given name
	 * 			|VALID_QUANTITIES.get(quantity)
	 * @throws 	IllegalArgumentException
	 * 			If the valid quantities does not contain the given
	 * 			name
	 * 			|!VALID_QUANTITIES.containsKey(quantity)
	 */
	public static Quantity getQuantity(String quantity)throws IllegalArgumentException{
		if(!VALID_QUANTITIES.containsKey(quantity)){
			throw new IllegalArgumentException();
		}
		return VALID_QUANTITIES.get(quantity);
	}

	/**
	 * Checks if the ingredient can be added with an add command
	 * @param 	ingredient
	 * 			String that will be checked 
	 * @return	False if the string is null
	 * 			| ingredient == null
	 * @return 	True if the string follows the format 
	 * 				"whole_number quantity simplename_of_ingredient"
	 * 			where quantity is a string equal to one of the name or
	 * 			plural of one of the standard containers and where
	 * 			simplename_of_ingredient is a valid simplename for
	 * 			an unmixed or mixed type 
	 * 			| if !ingredient.matches("([1-9]\\d*) ([A-Za-z]+) ([A-Za-z ]{3,})")
	 * 			| 		result == false
	 * 			| container=ingredient.split(" " , 3)[1]
	 *		 	| ingredientname=ingredient.split(" " , 3)[2]
	 *			|
	 *			| if(!VALID_QUANTITIES.containsKey(container))
	 *			|	then result == false
	 * 			|
	 * 			| if(!(AlchemicTypeMixed.canHaveAsSimpleName(ingredient) && 
	 * 			|							AlchemicTypeUnmixed.canHaveAsSimpleName(ingredient)) && 
	 * 			|								AlchemicTypeMixed.canHaveAsSpecialName(ingredient))
	 * 			| then result == false
	 * 			| else result == true
	 */
	public static boolean isValidIngredient(String ingredient){
		if (ingredient == null)
			return false;
		if(!ingredient.matches("([1-9]\\d*) ([A-Za-z]+) ([A-Za-z ]{3,})")){
			return false;
		}
		String container=ingredient.split(" " , 3)[1];
		String ingredientname=ingredient.split(" " , 3)[2];
		if(!VALID_QUANTITIES.containsKey(container)){
			return false;
		}
		try {
			new AlchemicTypeMixed("Abc mixed with Cde",States.LIQUID, new Temperature(0,0), 0).canHaveAsSimpleName(ingredient) ;
		}
		catch (IllegalArgumentException a){
			// The name is not valid for specialname mixed type
			try {
				new AlchemicTypeUnmixed(ingredientname,States.LIQUID, new Temperature(0,0), 0);
			}
			catch (IllegalArgumentException b){
				// name is not valid as simple name for unmixed ingredient
				try{
					new AlchemicTypeMixed(ingredient,States.LIQUID, new Temperature(0,0), 0).canHaveAsSimpleName(ingredient);
				}
				catch (IllegalArgumentException c){
					// name is never valid
					return false;
				}
			}
		}
		
		return true;

	}

	/**
	 * Tries to extract a quantity from a string
	 * @param 	ingredientString
	 * 			String that will be scanned
	 * @return 	If the string doesn't match the standard pattern then null is 
	 * 			returned else:
	 * 			Splits the string in three parts on the first two whitespaces.
	 * 			Multiplies the number of the first part with the quantity returned 
	 * 			after looking op the string found in the second part.
	 * 			|if(!ingredientString.matches("([1-9]\\d*) ([A-Za-z]+) ([A-Za-z ]+)"))
	 *			|then result == null
	 *			|else number = ingredientString.split(" ", 3)[0];
	 *			|	  container = ingredientString.split(" " , 3)[1];
	 *			|     result == new Quantity(Integer.parseInt(number)*getQuantity(container).getQuantity());
	 *	
	 */
	public static Quantity extractQuantity(String ingredientString){
		if(!isValidIngredient(ingredientString)){
			return null;
		}
		String number = ingredientString.split(" ", 3)[0];
		String container=ingredientString.split(" " , 3)[1];
		return new Quantity(Integer.parseInt(number)*getQuantity(container).getQuantity());
	}
	
	/**
	 * Tries to extract a ingredientname from a string
	 * @param 	ingredientString
	 * 			String that will be scanned
	 * @return 	Null if the string doesn't match the ingredientpattern.
	 * 			else the result is the third string after cutting the string
	 * 			in three based on whitespaces
	 * 			| if (!isValidIngredient(ingredientString))
	 * 			| then result == null
	 * 			| else result == ingredientString.split(" " , 3)[2]
	 *	
	 */
	public static String extractName(String ingredientString){
		if(!isValidIngredient(ingredientString)){
			return null;
		}
		return ingredientString.split(" " , 3)[2];
	}
	
	
	
	/**
	 * Adds a step to the recipe that is not ADD.
	 * @param 	step 
	 * 			the type of step to be added
	 * @param	index 
	 * 			the index where the step is added
	 * 
	 * @effect	The step is added to the commandlist at
	 * 			the given index
	 * 			| addRecipeCommandAt(step, index)
	 * 
	 * @throws	IllegalArgumentException
	 * 			The step to be added is a ADD command.
	 * 			| step == RecipeStep.ADD
	 * @throws	IndexOutOfBoundsException
	 * 			The index is out of bounds
	 * 			| index <= 0 || index > getNbOfIngredients() + 1
	 * 
	 */
	public void addRecipeStepAt(RecipeStep step, int index) throws IllegalArgumentException, IndexOutOfBoundsException{
		if (step == RecipeStep.ADD)
			throw new IllegalArgumentException();
		if (index <= 0 || index > getNbOfSteps()+1)
			throw new IndexOutOfBoundsException();
		addRecipeCommandAt(step, index);
	}
	
	/**
	 * Adds a ingredient to the recipe at a certain index
	 * @param 	ingredient
	 * 			The ingredient to be added
	 * @param	stepNb
	 * 			At what step the ingredient has to be added
	 * 
	 * @effect 	A ADD command is added to the recipe at the given index
	 * 			| addRecipeCommandAt(RecipeStep.ADD, index)
	 * @effect	The ingredient is added to the ingredientlist at position
	 * 			relative to where the new ADD command is compared to the 
	 * 			pre-existing ADD commands.
	 * 			|int addCommands = 0;
	 *			|for int i in 1.. stepNb-1
	 *			|	if (getRecipeCommandAt(i) == RecipeStep.ADD)
	 *			|		then
	 *			|		addCommands++;
	 *			|	i++
	 *			|addIngredientAt(ingredient, addCommands++)
	 * @throws	IllegalArgumentException
	 * 			The ingredient and/or quantity is not recognized
	 * 			| isValidIngredient(ingredient)
	 * @throws 	IndexOutOfBoundsException
	 * 			The index is out of bounds
	 * 			| index <= 0 || index > getNbOfIngredients() + 1
	 */
	public void addIngredientToRecipeAt( String ingredient, int stepNb) 
			throws IllegalArgumentException,IndexOutOfBoundsException{
		if (!isValidIngredient(ingredient)){
			throw new IllegalArgumentException();
		}
		if (stepNb <= 0 || stepNb > (getNbOfSteps() + 1))
			throw new IndexOutOfBoundsException();
		else
		{
			int addCommands = 0;
			for (int i = 1; i < stepNb; i++){
				if (getRecipeCommandAt(i) == RecipeStep.ADD)
					addCommands++;
			}
			addCommands++;
			addRecipeCommandAt(RecipeStep.ADD, stepNb);
			// now in a raw state
			addIngredientAt(ingredient, addCommands);
		}
	}
	
	/**
	 * Removes the recipeStep at the given index. If the 
	 * step happens to be a ADD step then the corresponding
	 * ingredient is removed too.
	 * @param	index
	 * 			The index of the recipestep to be removed
	 * 
	 * @effect	If the Command at the given index is ADD then
	 * 			the ingredient that corresponds with this ADD command
	 * 			is removed.
	 * 			|int addCommands = 0;
	 *			|for int i in 1.. index-1
	 *			|	if (getRecipeCommandAt(i) == RecipeStep.ADD)
	 *			|		then
	 *			|		addCommands++;
	 *			|	i++
	 *			|removeIngredientAt(addCommands++)
	 * @effect	Remove the command at the given index
	 * 			| removeRecipeCommandAt(index)
	 * @throws	IndexOutOfBoundsException
	 * 			the index is not in bounds
	 * 			| index <= 0 || index > getNbOfSteps()
	 */
	public void removeStepAt(int index)throws IndexOutOfBoundsException{
		if (index <= 0 || index > getNbOfSteps()) {
			throw new IndexOutOfBoundsException();
		}
		if (getRecipeCommandAt(index) != RecipeStep.ADD){
			removeRecipeCommandAt(index);
			return;
		}
		else {
			int addCommands = 0;
			for (int i = 1; i < index; i++){
				if (getRecipeCommandAt(i) == RecipeStep.ADD)
					addCommands++;
			}
			addCommands++;
			removeRecipeCommandAt(index);
			// now in a raw state
			removeIngredientAt(addCommands);
		}
	}
	/**
	 * Moves a recipe from its current book to
	 * a new one
	 * @param 	book
	 * 			The book where the recipe will be moved to.
	 * @effect	The recipeBook of this recipe is set to the given book
	 * 			If the old recipebook was not null then the recipe
	 * 			is removes from that book
	 * 			The recipe is added to the new book
	 *			| setRecipeBook(book);
	 *			| if (this.getRecipeBook != null)
	 *			| then this.getRecipeBook.removeRecipe(this);
	 *			| book.addRecipe(this);
	 * @throws 	IllegalArgumentException
	 * 			The given book is null
	 * 			| book == null
	 */
	@Raw
	public void moveRecipe(RecipeBook book) throws IllegalArgumentException{
		if (book == null)
			throw new IllegalArgumentException();
		RecipeBook oldBook = getRecipeBook();
		setRecipeBook(book);
		if (oldBook != null){
			oldBook.removeRecipe(this);
		}
		book.addRecipe(this);
	}
}
