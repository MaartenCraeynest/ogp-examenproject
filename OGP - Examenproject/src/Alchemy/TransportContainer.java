package Alchemy;


import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
/**
 * Class that creates containers for transporting ingredients
 * between devices and the storage location in the lab.
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version	1.0
 *
 */
public class TransportContainer extends IngredientContainer{
	/**
	 * Creates an empty transportcontainer from a standardtype
	 * @param	stdType
	 * 			The type of container
	 * @effect	A new ingredientContainer object is created with the
	 * 			capacity of the stdType and allowedState of the stdType
	 * 			| IngredientContainer(stdType.getCapacity(), stdType.getAllowedStates())
	 * @throws	NullPointerException
	 * 			The standard type is null
	 * 			| stdType == null
	 */
	public TransportContainer(StdContainers stdType){
		super(stdType.getCapacity(), stdType.getAllowedStates());
	}
	
	/**
	 * Creates a transport container for an ingredient and adds the
	 * ingredient to the container.
	 * @effect	The best fitting container is created
	 * 			| this(StdContainers.getBestFit(ingredient))
	 * @effect	The container is filled with the ingredient
	 * 			| this.fill(ingredient)
	 */
	public TransportContainer(AlchemicIngredient ingredient) 
			throws IllegalArgumentException, IllegalStateException {
		this(StdContainers.getBestFit(ingredient));
		this.fill(ingredient);
	}
	

	/***********************************************************************
	 * terminated
	 **********************************************************************/
	/**
	 * Checks if this container is terminated
	 */
	private boolean terminated = false;
	
	/**
	 * Checks if this container is terminated
	 */
	@Basic
	public boolean isTerminated(){
		return terminated;
	}
	
	/**
	 * Sets this container as terminated
	 * @post	The container is terminated
	 * 			| getTerminated == true;
	 */
	@Model
	private void setTerminated(){
		terminated = true;
	}
	
	/**
	 * Terminates this transport container
	 * @effect	The container is emptied and 
	 * 			set as terminated.
	 * 			| empty()
	 * 			| setTerminated()
	 * @throws 	IllegalStateException	
	 * 			The container is already terminated
	 * 			| isTerminated()
	 * 
	 */
	public void terminate(){
		if (isTerminated())
			throw new IllegalStateException("The transportcontainer is already terminated");
		empty();
		setTerminated();
	}
	
	/*****************************************************************************
	 * Overrides
	 ****************************************************************************/
	/**
	 * Checks if the container can have an ingredient as its content
	 * @param	ingredient
	 * 			The ingredient to check
	 * @return	True the given ingredient is null or if this container is not 
	 * 			terminated
	 * 			result == (ingredient == null || !this.isTerminated())
	 */
	@Override
	public boolean canHaveAsIngredient(AlchemicIngredient ingredient){
		return super.canHaveAsIngredient(ingredient) && (ingredient == null || !this.isTerminated());
	}
}
