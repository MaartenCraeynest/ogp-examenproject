package Alchemy;

/**
 * A class representing a unmixed ingredient
 * 
 * @author Maarten Craeynest, Antony de Jonckheere
 * @version	1.0
 *
 */
public class AlchemicTypeUnmixed extends AlchemicType {
	/**
	 * Initializes a unmixed alchemic type with a given simple name, standard state,
	 * standard temperature and theoretic volatility
	 * @param	name
	 * 			The standardname of this unmixed ingredient
	 * @param 	standardstate
	 * 			The state to set the standard state of this unmixed 
	 * 			alchemic type to
	 * @param 	standardtemp
	 * 			The temperature to set the standard temperature of this
	 * 			mixed alchemic type to
	 * @param 	theoreticVolatility
	 * 			The volatility to set the theoretic volatility of this 
	 * 			mixed alchemic type to
	 * @effect	The new unmixed type is set as an alchemic type with given name,
	 * 			standardstate,standardtemp and theoreticVolatility
	 * 			|super(name, standardstate, standardtemp, theoreticVolatility)
	 */
	public AlchemicTypeUnmixed(String name,States standardstate,Temperature standardtemp,double theoreticVolatility) throws IllegalArgumentException{
		super(name, standardstate,standardtemp,theoreticVolatility);
	}
	
	/**
	 * Initializes a type with simple name "Water", standardstate liquid,
	 * standard temperature [0,20] and theoretic volatility 0
	 * @effect	initializes an alchemic type: Water
	 * 			|this("Water",States.LIQUID,new Temperature(0,20),0)
	 */
	public AlchemicTypeUnmixed(){
		this("Water",States.LIQUID,new Temperature(0,20),0);
	}
}
