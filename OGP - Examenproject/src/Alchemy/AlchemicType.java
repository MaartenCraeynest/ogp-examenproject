package Alchemy;
import be.kuleuven.cs.som.annotate.*;
/**
 * A class that represents any alchemic type with variables containing the name,
 * standard temperature, standardState and theoretical volatility.
 * @invar	The alchemic type must have a valid simple name
 * 			| canHaveAsSimpleName(getSimpleName())
 * @invar	The alchemic type must have a valid theoretic volatility
 * 			| isValidVolatility(getTheoreticVolatility())
 * @invar	The standard state is valid
 * 			| isValidStandardState(getStandardState())
 * @invar	The standard temperature is valid
 * 			| isValidStandardTemperature(getStandardTemperature())
 * @author  Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 *
 */
public abstract class AlchemicType {
	/**
	 * Initializes an alchemic type with given arguments
	 * @param 	simpleName
	 * 			The simple name to give to this alchemic type
	 * @param 	standardState
	 * 			The standard state to give to this alchemic type
	 * @param 	standardTemp
	 * 			The standard temperature to give to this 
	 * 			alchemic type
	 * @param 	theoreticVolatility
	 * 			The theoretic volatilitiy to give to this
	 * 			alchemic type
	 * @post	The Simple name of this alchemic type is set to
	 * 			the given name
	 * 			| new.getSimpleName() == simpleName
	 * @post	The standard state of this alchemic type is set to 
	 * 			the given standard state
	 * 			| new.getStandardState() == standardState
	 * @post	The standard temperature of this alchemic type 
	 * 			is set to the given standard temperature
	 * 			|new.getStandardTemperature() == standardtemp
	 * @post	The theoretic volatility of this alchemic type 
	 * 			is set to the given theoretic volatitility
	 * 			| new.getTheoreticVolatility()== theoreticVolatility 
	 * @throws	IllegalArgumentException
	 * 			If one or more of the given arguments are not valid
	 * 			| !isValidStandardTemperature(standardTemp) ||
	 * 			|	!isValidStandardState(standardSate)||
	 * 			|	!isValidTheoreticVolatility(theoreticVolatility) ||
	 * 			|	!canHaveAsSimpleName(simpleName)
	 */
	@Model
	protected AlchemicType(String simpleName,States standardState,Temperature standardTemp,double theoreticVolatility)throws IllegalArgumentException{
		if(!isValidStandardTemperature(standardTemp)||!isValidStandardState(standardState)||
				!isValidTheoreticVolatility(theoreticVolatility) || !canHaveAsSimpleName(simpleName)){
			throw new IllegalArgumentException();
		}
		this.simpleName = simpleName;
		this.standardState=standardState;
		this.standardTemp=standardTemp;
		this.theoreticVolatility=theoreticVolatility;
	}
	

	/****************************************************************
	 * standardState
	 ****************************************************************/
	/**
	 * Variable registering the standard state of this alchemic type
	 * Liquid or Powder
	 */
	private final States standardState; 	
	
	/**
	 * Returns the standard state of this alchemic type
	 */
	@Basic @Immutable
	public States getStandardState(){
		return standardState;
	}

	/**
	 * Checks if a given state is a valid standardstate
	 * @param	state
	 * 			The state to check 	
	 * @return	True if the state is not null
	 * 			| result == (state != null)
	 */
	public static boolean isValidStandardState(States state){
		return (state != null);
	}
	
	/**********************************************************
	 * standardTemperature
	 **********************************************************/
	/**
	 * Variable registering the standard temperature of this 
	 * alchemictype.
	 */
	private final Temperature standardTemp;
	
	/**
	 * Returns the standard temperature of this alchemic type 
	 */
	@Basic @Immutable
	public Temperature getStandardTemperature(){
		return standardTemp;
	}
	
	/**
	 * Checks if the temperature is a valid standardTemperature
	 * @param	temp
	 * 			The temperature to check
	 * @return	True if the temperature is not null
	 * 			| result == (temp != null)
	 */
	public static boolean isValidStandardTemperature(Temperature temp){
		return temp != null;
	}
	
	/************************************************************
	 * theoreticVolatility
	 ************************************************************/
	/**
	 * Variable registering the theoretic volatility as a double
	 * between 0 and 1
	 */
	private final double theoreticVolatility;
	
	/**
	 * Returns the theoretic volatility of this alchemic type
	 */
	@Basic @Immutable
	public final double getTheoreticVolatility(){
		return theoreticVolatility;
	}
	/**
	 * a method to determine if the given volatility is a valid 
	 * theoretic volatility for this alchemic type
	 * @param 	volatility
	 *			The volatility to set this theoretic volatility to	
	 * @return	True if the given volatility is greater than or equal 
	 * 			to zero and smaller or equal to 1
	 * 	 		|volatility<=1 && volatility>=0
	 */
	public static boolean isValidTheoreticVolatility(double volatility){
		return volatility<=1&&volatility>=0;
	}
	
	/**********************************************************************
	 * simpleName
	 ********************************************************************/
	/**
	 * The simple name of this ingredient
	 */
	public final String simpleName;
	
	/**
	 * Returns the simple name of this type
	 */
	public String getSimpleName(){
		return simpleName;
	}
	
	/**
	 * Method to determine if the given name is a valid simple name
	 * for an alchemic type
	 * @param 	name
	 * 			The simple name to give to this alchemic type
	 * @return	True if and only if the name only contains legal 
	 * 			characters, the total length without whitespaces 
	 * 			is not smaller than 3, every word in the name has 
	 * 			a length greater or equal to 2, the simple name 
	 * 			does not contain any illegal additions and every 
	 * 			seperate word in the name begins with a capital letter
	 *			| if(!(name.matches(acceptedSymbols)))
	 *			|	then result == false;
	 *			| if(name.replaceAll("\\s","").length()<3)
	 *			|	then result == false
	 * 			| foreach(String a in IllegalAdditions){
	 *			|	if(name.matches(a))
	 *			|	then result == false;
	 *			| foreach(String word in String name)
	 *			|	if(!(word.length()>1 && word.matches("[A-Z].*")))
	 *			|	then result == false;
	 */
	public boolean canHaveAsSimpleName(String name){
		if (name == null)
			return false;
		
		for(String a:IllegalAdditions){
			if(name.matches(a)){
				return false;
			}
		}
		if(!(name.replaceAll(acceptedSymbols,"").isEmpty())){
			return false; 
		}
		if(name.replaceAll("\\s","").length()<3){
			return false;
		}
		String[] individual= name.split("\\s");
		for(String b:individual){
			if(!(b.length()>1 && b.matches("[A-Z].*"))){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Variable registering the illegal symbols in the simple names of alchemicTypes
	 */
	private final static String[] IllegalAdditions= {"\\bCooled\\b","\\bHeated\\b","\\bDanger\\b","\\bVolatile\\b","\\bInert\\b","\\bmixed\\b","\\bwith\\b"};
	/**
	 * Returns the illegalAddictions for the simplename of a type
	 */
	@Basic @Immutable
	public static String[] getIllegalAdditions(){
		return IllegalAdditions;
	}
	/**
	 * Variable registering the accepted symbols in the simple names of alchemic types
	 * "," can never be an accepted symbol
	 */
	private final static String acceptedSymbols="[A-Za-z( )']";
	/**
	 * Returns the accepted symbols for the simple name
	 */
	@Basic @Immutable
	public static String getAcceptedSymbols(){
		return acceptedSymbols;
	}
	
	/**
	 * Returns the Simple name of this alchemic type.
	 * @return	The simple name
	 * 			| result == getSimpleName()
	 */
	@Override
	public String toString(){
			return this.getSimpleName();
	}
}
