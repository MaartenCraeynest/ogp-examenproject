package Alchemy;
/**
 * enum containing the possible steps in recipes
 * @author Maarten Craeynest, Antony De Jonckheere
 */
public enum RecipeStep {
	COOL, HEAT, MIX, ADD;
}
