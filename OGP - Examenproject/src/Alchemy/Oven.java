package Alchemy;

/**
 * A class that represents an oven.
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 */
public class Oven extends TempAlteringDevice {
	/**
	 * Creates a new oven that can heat up ingredients to a margin of error
	 * around its own temperature
	 * @param 	temperature
	 * 			The temperature this device will be set to.
	 * @effect	The Oven is a TempAlteringDevice set to the given 
	 * 			temperature with one input and one output
	 * 			| super(1,1,temperature)
	 */
	public Oven(Temperature temperature) throws IllegalArgumentException {
		super(1,1,temperature);
	}
	
	/*********************************************************
	 * Overrides
	 *********************************************************/
	/**
	 * Set the temperature of the ingredient currently in input 
	 * to the temperature of this Oven and move the ingredient to
	 * the output. If there are pre-existing output ingredients then they
	 * are terminated.
	 * @effect	If the oven can be processed then
	 * 			every output slot that has an ingredient is terminated and if the corresponding
	 * 			input slot has an ingredient that ingredient's temperature is set
	 * 			to the temperature of this oven and is moved to the outputcontainer	
	 * 			| if (canBeProcessed())
	 * 			| then 	for each I in 1..getMAXOUTPUT():
	 * 			|		if getOutputAt(i)
	 * 			|		then	getOutputAt(i).getIngredient().terminate();
	 * 			|		for each I in 1..getMAXINPUT():
	 * 			|			if getOutputAt(i).hasIngredient()
	 * 			|			then 	if (!getInputAt(i).getIngredient().getTemperature().isGreaterThan(this.getTemperature())
	 *			|						&& !getInputAt(i).getIngredient().getTemperature().equals(this.getTemperature()))
	 *			|					then 	roundedColdness = this.getTemperature().getColdness() * ( 1+ (Math.random()-0.5)/10))
	 *			|							roundedHotness  = this.getTemperature().getHotness()  * ( 1+ (Math.random()-0.5)/10))
	 *			|							getInputAt(i).getIngredient().setTemperature(new Temperature(roundedColdness, roundedHotness))
	 *			|					getInputAt(i).getIngredient().move(getOutputAt(i))
	 */
	@Override
	public void process() {
		if (!canBeProcessed())
			return;
		for (int i = 1; i <= getMAXOUTPUT(); i++){
			// Empty the last output(s)
			if (getOutputAt(i).hasIngredient())
				getOutputAt(i).getIngredient().terminate();
		}
		for (int i = 1; i <= getMAXINPUT(); i++){
			if (getInputAt(i).hasIngredient()){
				if (!getInputAt(i).getIngredient().getTemperature().isGreaterThan(this.getTemperature())
					&& !getInputAt(i).getIngredient().getTemperature().equals(this.getTemperature()))
				{
					// heat the ingredient(s)
					long roundedColdness = (long) (this.getTemperature().getColdness() * ( 1L+ (Math.random()-0.5)/10));
					long roundedHotness = (long) (this.getTemperature().getHotness() * ( 1L+ (Math.random()-0.5)/10));
					getInputAt(i).getIngredient().setTemperature(new Temperature(roundedColdness, roundedHotness));
				}
				// moves it to the output
				getInputAt(i).getIngredient().move(getOutputAt(i));
			}
		}
	}

	/**
	 * Adds this oven to a laboratory
	 * @param	lab
	 * 			The laboratory where this oven will be added.
	 * @effect	The oven's laboratory is set to the given laboratory and
	 * 			the laboratory's oven is set to this.
	 * 			|	setLaboratory(lab)
	 * 			|	lab.setOven(this)
	 * @throws	IllegalArgumentException
	 * 			The given laboratory is null
	 * 			| lab == null
	 * @throws	IllegalStateException
	 * 			The laboratory already has an oven
	 * 			| lab.hasOven()
	 * @throws	IllegalStateException
	 * 			The oven is already in a laboratory
	 * 			| hasLaboratory()
	 */
	@Override
	public void addTo(Laboratory lab) throws IllegalStateException, IllegalArgumentException {
		if (lab == null)
			throw new IllegalArgumentException("The new lab can not be null");
		if (lab.hasOven())
			throw new IllegalStateException("The laboratory already has an oven");
		if (hasLaboratory())
			throw new IllegalStateException("The oven is already in a laboratory");
		setLaboratory(lab);
		lab.setOven(this);
	}

	/**
	 * Removes this oven from its laboratory
	 * @effect	The oven's laboratory is set to null and
	 * 			that laboratory's oven is set to null
	 * 			| Laboratory temp = this.getLaboratory()
	 * 			| this.setLaboratory(null)
	 * 			| temp.setOven(null)
	 * @throws	IllegalStateException
	 * 			The oven isn't in a laboratory
	 * 			| !hasLaboratory()
	 */
	@Override
	public void removeFromLab() throws IllegalStateException {
		if (!hasLaboratory())
			throw new IllegalStateException();
		Laboratory temp = this.getLaboratory();
		this.setLaboratory(null);
		temp.setOven(null);
	}

}
