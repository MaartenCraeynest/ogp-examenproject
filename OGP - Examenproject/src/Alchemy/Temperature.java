package Alchemy;

import be.kuleuven.cs.som.annotate.*;
/**
 * A class to represent a immutable temperature as perceived in
 * Medieval times.
 * @invar	The temperature must be valid
 * 			| isValidTemperature(getTemperature())
 * 
 * @author  Maarten Craeynest, Antony De Jonckheere
 * @version	1.0
 */
@Value
public class Temperature {
	/**
	 * Initializes a Temperature with given coldness and hotness 
	 * if the given hotness and coldness satisfy the class invariants
	 * @param 	coldness
	 * 			The coldness to set
	 * @param 	hotness
	 * 			The hotness to set
	 * @post	The temperature is equal to the absolute value of the hotness 
	 * 			minus the absolute value of the given coldness. If the result would
	 * 			have been bigger (smaller) than the maximum (minimum) allowed then
	 * 			the maximum (minimum) is set as the temperature.
	 * 			| if (Math.abs(hotness) - Math.abs(coldness) > getMaximumTemperature())
	 * 			| then	new.getTemperature() == getMaximumTemperature()
	 * 			| else if (Math.abs(hotness) - Math.abs(coldness) < -getMaximumTemperature())
	 * 			|	   then new.getTemperature() == -getMaximumTemperature()
	 * 			|	   else new.getTemperature() = Math.abs(hotness) - Math.abs(coldness)
	 */
	public Temperature(long coldness,long hotness){
		if (Math.abs(hotness) - Math.abs(coldness) > getMaximumTemperature())
			temperature = getMaximumTemperature();
		else if (Math.abs(hotness) - Math.abs(coldness) < -getMaximumTemperature())
			temperature = -getMaximumTemperature();
		else
			temperature = Math.abs(hotness) - Math.abs(coldness);
	}
	
	/********************************************************
	 * MAXTEMP
	 *******************************************************/
	/**
	 * Variable registering the maximum hotness and coldness
	 * never goes higher than Long.MAX_VALUE
	 */
	private final static long MAXTEMP=10000;
	
	/**
	 * Method that returns the maximum hotness and coldness
	 */
	 @Basic @Immutable
	public static long getMaximumTemperature(){
		return MAXTEMP;
	}
	 

	/******************************************************************
	 * Temperature
	 ******************************************************************/
	/**
	* Variable registering the temperature
	*/
	private final long temperature;
	
	/**
	 * Returns the coldness of this temperature
	 * @return	The negation of the minimum value of the temperature of this 
	 * 			and zero
	 * 			| result == -Math.min(getTemperature(), 0)
	 */
	@Immutable	
	public long getColdness(){
		return -Math.min(getTemperature(), 0);
	}
	/**
	 * Returns the hotness of this temperature
	 * @return	The maximum value between this temperature
	 * 			and zero
	 * 			| result == Math.max(getTemperature(), 0)
	 */
	@Immutable
	public long getHotness(){
		return Math.max(getTemperature(), 0);
	}
	
	/**
	 * Returns the temperature in degrees
	 */
	@Basic @Immutable
	public long getTemperature(){
		return temperature;
	}
	
	/**
	 * Compares a temperature with this temperature and computes 
	 * the difference
	 * @param 	temp
	 * 			The temperature to compare with
	 * @return	The difference between the this temperature
	 * 			and the given temperature. If the difference is
	 * 			greater then the max/min value of long then 
	 * 			the max/min value is returned.
	 * 			| try
	 * 			|	difference = Math.subtractExact(this.getTemperature(),temp.getTemperature())
	 * 			| catch(ArithmeticException a){
	 * 			|		if(this.getTemperature()>temp.getTemperature())
	 * 			|			difference = Long.MAX_VALUE
	 * 			|		else
	 * 			|			difference = Long.MIN_VALUE
	 * 			|	}
	 * 			| result == difference
	 */
	public long compare(Temperature temp){
		long difference=0;
		try{
			difference=Math.subtractExact(this.getTemperature(),temp.getTemperature());
		}
		catch(ArithmeticException a){
			if(this.getTemperature()>temp.getTemperature())
				difference = Long.MAX_VALUE;
			else{
				difference = Long.MIN_VALUE;
			}
		}
		return difference;
	}
	
	/**
<<<<<<< HEAD
	 * Returns a new temperature object that is n degrees hotter then 
	 * this temperature. If the result would become bigger then
	 * the biggest temperature then the biggest temperature is returened.
=======
	 * Returns a new temperature object that is n degrees hotter than 
	 * this temperature. If the result would become greater than
	 * the maximum temperature then the maximum temperature is returned.
>>>>>>> branch 'master' of https://MaartenCraeynest@bitbucket.org/MaartenCraeynest/ogp-examenproject.git
	 * @param 	n
	 * 			The amount to heat the temperature by
	 * @return	A temperature that is cooled by the absolute value of the given
	 * 			number if the given number is negative.
	 * 			A temperature set at the maximum temperature if the 
	 * 			result of the current temperature added with the given value
	 * 			is bigger then the maximumtemperature.
	 * 			If there is no overflow then the resulting temperature has a hotness equal to the
	 * 			maximum of the previous temperature added with the amount, and 0. 
	 * 			The coldness is equal to the negative minimum of the previous 
	 * 			temperature minus the amount, and 0.
	 * 			| if  (n<0)
	 * 			| then result == cool(-n)
	 * 			| else if (getTemperature()>= getMaximumTemperature()-n)
	 * 			| 	   then result == new Temperature(0, getMaximumTemperature())
	 * 			|	   else result == new Temperature(-Math.min(getTemperature() + n, 0),
	 * 			|									   Math.max(getTemperature() + n, 0))	
	 */
	public Temperature heat(long n){
		if(n<0){
			return cool(-n);
		}
		if (getTemperature()>= getMaximumTemperature() - n)
			// Overflow
			return new Temperature(0 , getMaximumTemperature());
		else {
			long resultingTemp = getTemperature() + n;
			return new Temperature(Math.min(resultingTemp, 0), Math.max(resultingTemp, 0));
		}
	}
	
	/**
	 * Returns a new temperature object that is n degrees cooler than 
	 * this temperature. If the result would become smaller than
	 * the minimum temperature then the minimum temperature is returned.
	 * @param 	n
	 * 			The amount to cool the temperature by
	 * @return	A temperature that is heated by the absolute value of the given
	 * 			number if the given number is negative
	 * 			A temperature set at the minimum temperature if the 
	 * 			result of the current temperature minus the given value
<<<<<<< HEAD
	 * 			is smaller then the negative maximum temperature.
=======
	 * 			is smaller then the minimum temperature.
>>>>>>> branch 'master' of https://MaartenCraeynest@bitbucket.org/MaartenCraeynest/ogp-examenproject.git
	 * 			If there is no overflow then the resulting temperature has a hotness equal to the
	 * 			maximum of the previous temperature minus the amount and 0. The coldness is equal to
	 * 			the negative minimum of the previous temperature minus the amount and 0.
	 * 			| if  (n<0)
	 * 			| then result == heat(-n)
	 * 			| else if (getTemperature() <= -getMaximumTemperature() + n)
	 * 			| 	   then result == new Temperature(getMaximumTemperature(), 0)
	 * 			|	   else result == new Temperature(-Math.min(getTemperature() - n, 0),
	 * 			|									   Math.max(getTemperature() - n, 0))	
	 */
	public Temperature cool(long n){
		if(n<0){
			return heat(-n);
		}
		if (getTemperature()<= -getMaximumTemperature() + n)
			// Overflow
			return new Temperature(getMaximumTemperature(), 0);
		else {
			long resultingTemp = getTemperature() - n;
			return new Temperature(Math.min(resultingTemp, 0), Math.max(resultingTemp, 0));
		}
	}
	/**
	 * Method to determine if a given value is a valid temperature
	 * @param 	temp
	 * 			The temperature to compare
	 * @return	True if the absolute value of the temperature of is
	 * 			smaller or equal than the maximumtemperature.
	 * 			| result == Math.abs(temp) <= getMaximumTemperature()
	 *
	 */
	public static boolean isValidTemperature(long temp){
		return (Math.abs(temp) <= getMaximumTemperature());
	} 
	
	/**
	 * A method that checks if this temperature is greater than
	 * another temperature
	 * @param	otherTemp
	 * 			The other temperature
	 * @return  True if this temperature is greater or equal then the 
	 * 			other temperature.
	 * 			| result == (this.getTemperature() >= otherTemp.getTemperature())
	 */
	public boolean isGreaterThan(Temperature otherTemp){
		return (this.getTemperature() > otherTemp.getTemperature());
	}
	
	/**
	 * A method that checks if an object is equal to this temperature
	 * @param	obj
	 * 			The object that will be compared for equality
	 * @return	True if the given argument is not null, is of the same class as this
	 * 			and the temperature is equal
	 * 			are equal
	 * 			| result == (otherTemp!= null) && (this.getClass() != obj.getClass()) 
	 * 			|				&& (getTemperature() == obj.getTemperature())
	 */
	@Override
	public boolean equals(Object obj){
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Temperature otherTemperature = (Temperature) obj;
		return (getTemperature() == otherTemperature.getTemperature());
	}
	
	/**
	 * Returns the hashCode of this temperature
	 * @return	The hashCode of the temperature of this object
	 * 			| result == Long.hashCode(getHotness() - getColdness())
	 */
	@Override
	public int hashCode(){
		return Long.hashCode(getHotness() - getColdness());
	}
	
	
}