package Alchemy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import be.kuleuven.cs.som.annotate.*;
/**
 * A container that can hold a quantity of a single ingredient up to a maximum 
 * capacity.
 * @invar	The ingredient in this container is valid
 * 			| hasProperIngredient(getIngredient())
 * @author 	Maarten Craeynest, Antony De Jonckheere
 * @version	1.0
 * 
 */
public class IngredientContainer {
	/**
	 * Creates a new empty container with a given capacity
	 * @param 	maxCapacity
	 * 			The max capacity of the container
	 * @param	allowedStates
	 * 			The allowed states of this container as a set.
	 * @post	The capacity is set to the given capacity or
	 * 			the maximumcapacity if the given capacity was null
	 * 			| if (maxCapacity == null)
	 * 			| then capacity = new Quantity(Quantity.MAXQUANTITY)
	 * 			| else new.getCapacity() == maxCapacity
	 * @post	Every state that is in allowed states	
	 * 			can be contained by this container. If the allowedstates
	 * 			is null then the container can contain any state
	 * 			| if (allowedStates != null)
	 * 			| then 	for each state in allowedStates
	 * 			|			new.canContain(state)
	 * 			| else	for each state in States.values()
	 * 						new.canContain(state)
	 * @post	The container has no ingredient in it
	 * 			| !hasIngredient()
	 */
	@Model	
	protected IngredientContainer(Quantity maxCapacity, Set<States> allowedStates){
		if (maxCapacity== null)
			capacity = new Quantity(Quantity.MAXQUANTITY);
		else
			capacity = maxCapacity;
		if (allowedStates == null)
			this.allowedStates = new HashSet<States>(Arrays.asList(States.values()));
		else
			this.allowedStates = new HashSet<States>(allowedStates);
	}
	

	/**
	 * Creates an empty container with maximum capacity that can hold any 
	 * ingredienttype.
	 * @effect	A new container is created with a maximumcapacity
	 * 			and all states allowed.
	 * 			| super(null, null)
	 */
	public IngredientContainer(){
		this(null, null);
	}
	
	/*****************************************************************
	 * containerType
	 ****************************************************************/
	/**
	 * Keeps track as for which states this container is built
	 * @note	This should be an immutable set but java does not support 
	 * 			it. We simply prevented any access to it to achieve somewhat
	 * 			the same effect. 
	 */
	final private Set<States> allowedStates;
	
	/**
	 * Check if this container can contain a given state.
	 * @param	state
	 * 			The state to check if this container can contain it
	 * @return	True if the container can contain the given state
	 * 			| allowedStates.contains(state) 
	 */
	public boolean canContain(States state){
		return allowedStates.contains(state);
	}
	
	/**
	 * Returns the allowed states of this container
	 */
	@Basic @Immutable
	public Set<States> getAllowedStates(){
		// return a copy
		return new HashSet<States>(allowedStates);
	}
	
	
	/********************************************************************
	 * capacity
	 *******************************************************************/
	/**
	 * The maximum quantity this container can hold
	 */
	final private Quantity capacity;
	
	/**
	 * Returns the maximum capacity
	 */
	@Immutable @Basic
	public Quantity getCapacity(){
		return capacity;
	}
	
	/*********************************************************************
	 * ingredient
	 ********************************************************************/
	/**
	 * The ingredient that is currently in this container or null if this
	 * container does not contain an ingredient.
	 */
	private AlchemicIngredient ingredient = null;
	
	/**
	 * Returns the ingredient the container currently holds, or null if no
	 * ingredient is currently in the container.
	 */
	@Basic
	public AlchemicIngredient getIngredient(){
		return this.ingredient;
	}
	
	/**
	 * Checks if this container currently holds an ingredient
	 * @return	True if the ingredient this container currently holds
	 * 			is not null
	 * 			|this.ingredient != null
	 */
	public boolean hasIngredient(){
		return this.ingredient != null;
	}
	
	/**
	 * Sets ingredient to the given ingredient
	 * @param	newIngredient
	 * 			The new ingredient of the container
	 * @pre		The container can have the ingredient as
	 * 			its ingredient.
	 * 			| canHaveAsIngredient(newIngredient)
	 */
	@Model
	private void setIngredient(AlchemicIngredient newIngredient){
		this.ingredient = newIngredient;
	}
	
	/**
	 * Checks if this container has a proper ingredient
	 * @return	true if this container has no ingredient or
	 * 			the ingredient is a valid ingredient for this
	 * 			container and the container of the ingredient 
	 * 			is set to this
	 * 			| result == !hasIngredient() || (getIngredient().getContainer() == this 
	 *			|									&& canHaveAsIngredient(getIngredient()))
	 */
	public boolean hasProperIngredient(){
		return !hasIngredient() || (getIngredient().getContainer() == this 
				&& canHaveAsIngredient(getIngredient()));
	}
	
	/**
	 * Checks if the container can have an ingredient as its content
	 * @param	ingredient
	 * 			The ingredient to check
	 * @return	False if the ingredient is not null and if the state of the ingredient 
	 * 			is not supported by the container or if the quantity of the ingredient
	 * 			is greater than the maximum capacity of the container and the container 
	 * 			is not terminated
	 * 			| if (ingredient != null && (!canContain(ingredient.getState()) 
	 * 			|				|| !getCapacity().isGreaterOrEqualTo(ingredient.getQuantity())))
	 * 			| then result == false	
	 * @note	Open specification				
	 */
	public boolean canHaveAsIngredient(AlchemicIngredient ingredient){
		return ingredient == null || (canContain(ingredient.getState())
				&& getCapacity().isGreaterOrEqualTo(ingredient.getQuantity()));
	}
	
	/**
	 * If this container has an ingredient, it is removed
	 * @effect	This container's ingredient is set to null and if the container
	 * 			has an ingredient its container is set to null.
	 * 			| setIngredient(null)
	 *			| if (getIngredient() != null)
	 * 			| then getIngredient().setContainer(null)
	 */
	public void empty(){
		AlchemicIngredient temp = getIngredient();
		setIngredient(null);
		if (temp != null)
			temp.setContainer(null);
	}
	
	/**
	 * Adds an ingredient to this container
	 * @effect 	The ingredient is added to this container and
	 * 			the ingredient's container is set to this.
	 * 			| setIngredient(newIngredient);
	 * 			| newIngredient.setContainer(this);
	 * @throws	IllegalStateException
	 * 			The ingredient already is in a container
	 * 			| newIngredient.hasContainer()
	 * @throws	IllegalStateException
	 * 			This container already contains an ingredient
	 * 			| hasIngredient()
	 * @throws	IllegalArgumentException
	 * 			The container cannot contain the ingredient
	 * 			| !canHaveAsIngredient(ingredient)
	 * @throws	IllegalArgumentException
	 * 			The newIngredient is null
	 * 			| newIngredient == null
	 */
	public void fill(@Raw AlchemicIngredient newIngredient) 
			throws IllegalStateException, IllegalArgumentException{
		if (newIngredient == null)
			throw new IllegalArgumentException("The container cannot be filled with nothing");
		if (newIngredient.hasContainer())
			throw new IllegalStateException("The given ingredient is already in a container");
		if (this.hasIngredient())
			throw new IllegalStateException("The container allready has an ingredient");
		if (!this.canHaveAsIngredient(newIngredient))
			throw new IllegalArgumentException("The cannot be contained in this container");
		setIngredient(newIngredient);
		newIngredient.setContainer(this);
	}
}
