package Alchemy;

import java.util.Set;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

/**
 * A enum for all the standard containers
 * that exists with their maximum capacity, allowed
 * states, and name (both singular and plural)
 * @author 	Maarten Craeynest, Antony de Jonckheere
 * @version	1.0
 */
public enum StdContainers {
	//Order from smallest to greatest (that can contain the same ingredienttypes) so that the
	// ordials follow the quantities. This allows getBestfit to be more easily implemented.
	SPOON(	new Quantity(Quantity.SPOONAMOUNT)	, new HashSet<States>(Arrays.asList(States.LIQUID, States.POWDER)), "Spoon", "Spoons"),
	
	VIAL(	new Quantity(Quantity.VIALAMOUNT)	, new HashSet<States>(Arrays.asList(States.LIQUID)),"Vial", "Vials"),
	BOTTLE(	new Quantity(Quantity.BOTTLEAMOUNT)	, new HashSet<States>(Arrays.asList(States.LIQUID)), "Bottle", "Bottles"),
	JUG(	new Quantity(Quantity.JUGAMOUNT)	, new HashSet<States>(Arrays.asList(States.LIQUID)), "Jug", "Jugs"),
	BARREL(	new Quantity(Quantity.BARRELAMOUNT)	, new HashSet<States>(Arrays.asList(States.LIQUID)), "Barrel", "Barrels"),
	
	SACHEL(	new Quantity(Quantity.SACHELAMOUNT)	, new HashSet<States>(Arrays.asList(States.POWDER)),"Sachel","Sachels"),
	BOX(	new Quantity(Quantity.BOXAMOUNT)  	, new HashSet<States>(Arrays.asList(States.POWDER)),"Box","Boxes"),
	SACK(	new Quantity(Quantity.SACKAMOUNT) 	, new HashSet<States>(Arrays.asList(States.POWDER)),"Sacks","Sacks"),
	CHEST(	new Quantity(Quantity.CHESTAMOUNT)	, new HashSet<States>(Arrays.asList(States.POWDER)),"Chest","Chests");
	
	
	
	/**
	 * Creates a containertype with a given capacity, a name,
	 *  the plural of the name and set of allowed states.
	 * @param 	capacity
	 * 			The capacity of the container
	 * @param 	allowedStates
	 * 			The allowed state of the container
	 * @param	name
	 * 			The name of the type
	 * @param	plural
	 * 			The plural of the name
	 * @post	The name is set to the given name
	 * 			| getName() == name
	 * @post	The plural is set to the given plural
	 * 			| getPlural() == plural
	 * @post	Every given type is supported
	 * 			| for (States state : allowedStates)
	 * 			|	getAllowedStates().contains(state)
	 * @post	The capacity is set to the given quantity
	 * 			| getCapacity() == capacity
	 */	
	private StdContainers(Quantity capacity, Set<States> allowedStates, String name, String plural){
		this.capacity = capacity;
		this.allowedStates = Collections.unmodifiableSet(allowedStates);
		this.name=name;
		this.plural=plural;
	}
	
	/**
	 * The capacity of this stdcontainer
	 */
	private final Quantity capacity;
	/**
	 * Returns the capacity of this standardcontainer
	 */
	@Basic @Immutable
	public Quantity getCapacity(){
		return capacity;
	}
	
	/**
	 * The states that are allowed in this stdContainer
	 */
	private final Set<States> allowedStates;
	/**
	 * Returns an unmodifiable set of the allowed states of this standardtype
	 */
	@Basic @Immutable
	public Set<States> getAllowedStates(){
		return allowedStates;		
	}
	/**
	 * Variable registering the name of the standard container
	 */
	private final String name;
	
	/**
	 * Returns the name of the standard container
	 */
	@Basic @Immutable
	public String getName() {
		return name;
	}
	/**
	 * Variable registering the plural of the name of this standard container
	 */
	private final String plural;
	/**
	 * Returns the plural of the name of the standard container
	 */
	@Basic @Immutable
	public String getPlural() {
		return plural;
	}
	
	/**
	 * Finds the smallest TransportContainer that can contain an ingredient
	 * @param	ingredient
	 * 			The ingredient for which a containertype is sought
	 * @return	The first standardcontainer that has enough capacity to store the ingredient
	 * 			and also can hold the state the ingredient currently is in.
	 * 			| for (StdContainers type : StdContainers.values())
	 *			|	if (new Container(type).canHaveAsIngredient(ingredient))
	 *			|	then	result == type;
	 * @throws	IllegalArgumentException
	 *			There are no containers that can hold the ingredient.
	 *			| for (StdContainers type : StdContainers.values())
	 *			|	!(new Container(type).canHaveAsIngredient(ingredient))
	 */
	public static StdContainers getBestFit(AlchemicIngredient ingredient)
			throws IllegalArgumentException {
		for (StdContainers type : StdContainers.values()){
			if ((new TransportContainer(type)).canHaveAsIngredient(ingredient))
				return type;
		}
		throw new IllegalArgumentException("No container can store the ingredient");
	}
	
	/**
	 * Finds the biggest capacicity of any standardcontainer  that can contain a given
	 * state. A zero quantity is returned if no container can hold this state. 
	 * @param	state
	 * 			Which state the container must be able to contain
	 * @return	The capacity of the biggest container that can contain the given 
	 * 			ingredientstate. An empty quantity if no containers can hold that state.
	 * 			| maxQuantity = new  Quantity(0L)
	 *			| for (StdContainers stdCont : StdContainers.values())
	 *			|	if (stdCont.getCapacity().isGreaterOrEqualTo(maxQuantity) 
	 *			|				&& getAllowedStates().contains(state))
	 *			|	then	maxQuantity = stdCont.getCapacity()
	 *			| result == maxQuantity;
	 */
	public static Quantity biggestContainerOf(States state){
		Quantity maxQuantity = new  Quantity(0L);
		for (StdContainers stdCont : StdContainers.values()){
			if (stdCont.getCapacity().isGreaterOrEqualTo(maxQuantity) && stdCont.getAllowedStates().contains(state))
				maxQuantity = stdCont.getCapacity();
		}
		return maxQuantity;
	}
}
