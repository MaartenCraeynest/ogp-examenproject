package Alchemy;

/**
 * A class for kettles that can mix two 
 * ingredients
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 */
public class Kettle extends Device {
	
	/**
	 * Creates a new empty kettle.
	 * @effect	The kettle is a device with two
	 * 			inputs and one output
	 * 			| super(2,1)
	 */
	public Kettle(){
		super(2,1);
	}
	
	/*********************************************************
	 * Overrides
	 ********************************************************/

	/**
	 * Empties the output, mixes the input ingredients to a single ingredient
	 * and moves the new ingredient to the output container.
	 * @effect	if this kettle is not in a laboratory nothing will happen
	 * 			|super.process();
	 * @effect	If the kettle can be processed then
	 * 			each argument that is in a output container is terminated
	 * 			| if (canBeProcessed())
	 * 			| then 	for each I in 1..getMAXOUTPUT():
	 * 			|			if getOutputAt(i).hasIngredient()
	 * 			|			then	getOutputAt(i).getIngredient().Terminate()	 
	 * @effect	If the kettle can be processed and
	 * 			if there is one ingredient total in the input containers
	 * 			then the input ingredient is moved to the output container.
	 * 			| if (getNbOfInputs() == 1 && canBeProcessed())
	 * 			|		for each I in 1..getMAXINPUT():
	 * 			|			if (getInputAt.hasIngredient())
	 * 			|				getInputAt(i).move(getOutputAt(1))
	 * @effect	If the kettle can be processed and there is more than one ingredient,
	 * 			 but all of the same type then an ingredient of unmixed type is
	 * 			 returned with the orgiginal type, equivalent type and equivalent temperature.
	 * 			| allSameType = true
	 * 			| for each I in 1..getMAXINPUT():
	 * 			|	for each J in 1..getMAXINPUT():
	 * 			|		if (getInputAt(I).hasIngredient() && getInputAt(J).hasIngredient() &&
	 * 			|				getInputAt(I).getIngredient().getType() != getInputAt(J).getIngredient().getType)
	 * 			|		then allSameType = false
	 * 			| if (allSame == true && getNbOfInputs()> 1 && canBeProcessed())
	 * 			| then 	toMix = new AchemicIngredient[getNbOfInputs()]
	 * 			|		index = 0
	 * 			|		for each I in 1..getMAXINPUT():
	 * 			|			if (getInputAt(i).hasIngredient())
	 * 			|			then	toMix[index] = getInputAt(i).getIngredient()
	 * 			|					index++
	 * 			|		newIngredient = new AlchemicIngredient(
	 * 			|							toMix[0].getType(),
	 * 			|							AlchemicTypeMixed.getEquivalentMixedState(toMix),
	 * 			|							AlchemicTypeMixed.getEquivalentMixedQuantity(toMix),
	 * 			|							AlchemicTypeMixed.getEquivalentMixedTemperature(toMix));	
	 * 			|		for each I in 0..getNbOfIngredients()-1:
	 * 			|			toMix[i].terminate()
	 * 			|		newIngredient.move(getOutputAt(1))
	 * @effect	If the kettle can be processed and there is more than one ingredient 
	 * 			in the kettle and at leastvtwo with a different type then
	 * 			an array is created of all the ingredients currently in 
	 * 			the kettle and a new ingredient is created with the mixed
	 * 			equivalent properties of the original ingredients. Every input
	 * 			ingredient is terminated and the new ingredient is added to 
	 * 			the first output container.
	 * 			| allSameType = true
	 * 			| for each I in 1..getMAXINPUT():
	 * 			|	for each J in 1..getMAXINPUT():
	 * 			|		if (getInputAt(I).hasIngredient() && getInputAt(J).hasIngredient() &&
	 * 			|				getInputAt(I).getIngredient().getType() != getInputAt(J).getIngredient().getType)
	 * 			|		then allSameType = false
	 * 			| if ( allSameType == false && canBeProcessed() && getNbOfInputs() > 1)
	 * 			| then  toMix = new AchemicIngredient[getNbOfInputs()]
	 * 			|		index = 0
	 * 			|		for each I in 1..getMAXINPUT():
	 * 			|			if (getInputAt(i).hasIngredient())
	 * 			|			then	toMix[index] = getInputAt(i).getIngredient()
	 * 			|					index++
	 * 			|		newIngredient = new AlchemicIngredient(
	 * 			|							AlchemicTypeMixed.getEquivalentMixedType(toMix),
	 * 			|							AlchemicTypeMixed.getEquivalentMixedState(toMix),
	 * 			|							AlchemicTypeMixed.getEquivalentMixedQuantity(toMix),
	 * 			|							AlchemicTypeMixed.getEquivalentMixedTemperature(toMix));	
	 * 			|		for each I in 0..getNbOfIngredients()-1:
	 * 			|			toMix[i].terminate()
	 * 			|		newIngredient.move(getOutputAt(1))
	 */
	@Override
	public void process() throws ArithmeticException {		
		if (!canBeProcessed())
			return;
		if (getNbOfInputs() == 1){
			for (int i = 1; i <= getMAXOUTPUT(); i++){
				if (getOutputAt(i).hasIngredient())
					getOutputAt(i).getIngredient().terminate();
			}
			for (int i = 1; i <= getMAXINPUT(); i++){
				if (getInputAt(i).hasIngredient())
					getInputAt(i).getIngredient().move(getOutputAt(1));
			}
			return;
		}
		if (getNbOfInputs() > 1){
			AlchemicTypeMixed newType=null;
			Quantity newQuantity=null;
			Temperature newTemperature=null;
			AlchemicIngredient newIngredient;
			
			AlchemicIngredient[] toMix = new AlchemicIngredient[getNbOfInputs()];
			for (int i = 1, arrayIndex = 0; i <= getMAXINPUT() && arrayIndex < getNbOfInputs(); i++){
				if (getInputAt(i).hasIngredient()){
					toMix[arrayIndex] = getInputAt(i).getIngredient();
					arrayIndex++;
				}
			}
			
			boolean allSameType = true;
			for (int i = 0; i <getNbOfInputs(); i++){
				for(int j = 0; j < getNbOfInputs(); j++){
					if (!toMix[i].getType().equals(toMix[j].getType()))
			 			 allSameType = false;
				}
			}
			
			try{
				newType=AlchemicTypeMixed.getEquivalentMixedType(toMix);
				newQuantity=AlchemicTypeMixed.getEquivalentMixedQuantity(toMix);
				newTemperature=AlchemicTypeMixed.getEquivalentMixedTemperature(toMix);
			}
			catch(ArithmeticException a){
				throw a;
			}
			
			 if (allSameType == true ){
				 newIngredient = new AlchemicIngredient(
			  					toMix[0].getType(),
								AlchemicTypeMixed.getEquivalentMixedState(toMix),
								newQuantity,
								newTemperature);	
			 }
			 else {

				 newIngredient = 
							new AlchemicIngredient(newType, AlchemicTypeMixed.getEquivalentMixedState(toMix), newQuantity, newTemperature);
			 }
			 
			for (int i = 1; i <= getMAXOUTPUT(); i++){
				if (getOutputAt(i).hasIngredient())
					getOutputAt(i).getIngredient().terminate();
			}
			
			// Creation is valid, can start making changes to the universe
			getOutputAt(1).empty();
			
			for (int i = 0; i < getNbOfInputs(); i++)
				toMix[i].terminate();
			
			newIngredient.move(getOutputAt(1));
		}
		
	}

	/**
	 * Adds this Kettle to a laboratory
	 * @param	lab
	 * 			The laboratory where this kettle will be added.
	 * @effect	The kettle's laboratory is set to the given lab and
	 * 			the lab's kettle is set to this.
	 * 			|	setLaboratory(lab)
	 * 			|	lab.setKettle(this)
	 * @throws	IllegalArgumentException
	 * 			The given lab is null
	 * 			| lab == null
	 * @throws	IllegalStateException
	 * 			The laboratory already has an kettle
	 * 			| lab.hasKettle()
	 * @throws 	IllegalStateException
	 * 			The kettle already has a laboratory
	 * 			| hasLaboratory()
	 */
	@Override
	public void addTo(Laboratory lab) throws IllegalStateException, IllegalArgumentException {
		if (lab == null)
			throw new IllegalArgumentException("The new lab can not be null");
		if (lab.hasKettle())
			throw new IllegalStateException("The laboratory already has a kettle");
		if (this.hasLaboratory())
			throw new IllegalStateException("This kettle already has a laboratory");
		setLaboratory(lab);
		lab.setKettle(this);
	}

	/**
	 * Removes this device from its lab
	 * @effect	The kettle's lab is set to null and
	 * 			that lab's kettle is set to null
	 * 			| Laboratory temp = this.getLaboratory()
	 * 			| this.setLaboratory(null)
	 * 			| temp.setKettle(null)
	 * @throws	IllegalStateException
	 * 			The kettle isn't in a laboratory
	 * 			| !hasLaboratory()
	 */
	@Override
	public void removeFromLab() throws IllegalStateException {
		if (!hasLaboratory())
			throw new IllegalStateException();
		Laboratory temp = this.getLaboratory();
		this.setLaboratory(null);
		temp.setKettle(null);
	}

}
