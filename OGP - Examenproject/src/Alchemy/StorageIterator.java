package Alchemy;

/**
 * The storage iterator is an iterator aimed at iterating
 * through the ingredients currently in storage in a laboratory.
 * 
 * A non-empty directory-iterator has a current ingredient,
 * with an ingredienttype which is returned by the inspector
 * getCurrente(). This inspector keeps on returning the same element
 * until the directory-iterator is advanced or reset.
 * 
 * Upon creation, a storage-iterator will be initialized with
 * all the elements in its storage. If the storage of that
 * laboratory is not empty, its first element will become the
 * current element for the iterator.
 * 
 * The Iterator is made so that no illegal actions can be done
 * onto the ingredients in storage. This means that the ingredient 
 * in storage will never be returned, so that it cannot be changed
 * or terminated. Only properties of the ingredient will be returned
 * 
 * @author  Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 * @see		DirectoryIterator  - Project 3 ModelOplossing
 * 			This Iterator is modeled after the DirectoryIterator
 * 			made by Tommy Messelis (2015)
 */
public interface StorageIterator {
	/**
	 * Return the number of remaining items to be
	 * returned by this storage iterator, including
	 * the current item.
	 * 
	 * @return	The resulting number cannot be negative.
	 * 			| result >= 0 
	 */
	int getNbRemainingIngredients();
	
	/**
	 * Return the type of the ingredient at the current index
	 *  of this directory-iterator.
	 * 
	 * @return	The type of the current ingredient
	 * @throws	IndexOutOfBoundsException
	 * 			This directory-iterator has no current item.
	 * 			| getNbRemainingItems() == 0
	 */
	AlchemicType getCurrent() throws IndexOutOfBoundsException;
	
	/**
	 * Return the quantity of the ingredient at the current index
	 *  of this directory-iterator.
	 * 
	 * @return	The quantity of the current ingredient
	 * @throws	IndexOutOfBoundsException
	 * 			This directory-iterator has no current item.
	 * 			| getNbRemainingItems() == 0
	 */
	Quantity getQuantity() throws IndexOutOfBoundsException;
	
	/**
	 * Return the volatilityDeviation of the ingredient 
	 * at the current index of this directory-iterator.
	 * 
	 * @return	The volatilityDeviation of the current ingredient
	 * @throws	IndexOutOfBoundsException
	 * 			This directory-iterator has no current item.
	 * 			| getNbRemainingItems() == 0
	 */
	double getVolatilityDeviation() throws IndexOutOfBoundsException;
	
	/**
	 * Advance the current ingredient of this iterator to the
	 * next ingredient. 
	 * 
	 * @pre		This storage-iterator must still have some remaining items.
	 * 			| getNbRemainingItems() > 0
	 * @post	The number of remaining disk items is decremented
	 * 			by 1.
	 * 			| new.getNbRemainingItems() == getNbRemainingItems() - 1
	 */
	void advance();
	
	/**
	 * Reset this storage-iterator to its first item.
	 */
	void reset();
	
	/**
	 * checks if the iterator has remaining items after the current item
	 * @return	true if the number of items is greater than 1
	 * 			| result == getNbRemainingItems()>1
	 */
	boolean hasNext();
}
