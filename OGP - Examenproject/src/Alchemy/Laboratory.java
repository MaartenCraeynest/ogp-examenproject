package Alchemy;


import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;
/**
 * A class that keeps track of a laboratory and its storage.
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 * @invar	The lab has proper ingredients.
 * 			| hasProperStorage()
 * @invar	The ingredient can have that amount of
 * 			current storage.
 * 			| canHaveAsCurrentStorage()
 * @invar	The lab has proper coolingbox
 * 			| hasProperCoolingBox()
 * @invar	The lab has proper oven
 * 			| hasProperOven()
 * @invar	The lab has proper kettle
 * 			| hasProperKettle()
 * @invar	The lab has proper transmogifier
 * 			| hasProperTransmogifier()
 */
public class Laboratory {
	/**
	 * Creates a new laboratory with the given amount of 
	 * storage rooms that indicates the maximum amount
	 * of storage.
	 * @param	nbOfStorerooms
	 * 			The maximum storage capacity of this laboratory
	 * 			given as the number of storerooms.
	 * @throws  IllegalArgumentException
	 * 			The number of storerooms is smaller or equal 
	 * 			to zero or the number of storerooms so big that
	 * 			the result would overflow a long.
	 * 			| nbOfStorerooms <= 0 || nbOfStorerooms >= 
	 * 			|			(Long.MAX_VALUE/Quantity.STOREROOMAMOUNT)
	 * @post	The laboratory has no ingredients in storage
	 * 			| getCurrentStorage().equals(new Quantity(0))
	 * @post	The laboratory has a maximum capacity of 
	 * 			the nbOfStorerooms times the standard
	 * 			storeroomQuantity
	 * 			| getMAXSTORAGE().equals(
	 * 			|		new Quantity(nbOfStorerooms *
	 * 			|						Quantity.STOREROOMQUANTITY))
	 * 			
	 */
	public Laboratory(int nbOfStorerooms)
			throws IllegalArgumentException {
		if (nbOfStorerooms <= 0 || nbOfStorerooms >= (Long.MAX_VALUE/Quantity.STOREROOMAMOUNT))
			throw new IllegalArgumentException(
					"Please provide a strictly positive number");
		MAXSTORAGE = new Quantity(nbOfStorerooms*Quantity.STOREROOMAMOUNT);
	}
	
	/*************************************************
	 * MAXSTORAGE
	 *************************************************/
	/**
	 * The maximum storage quantity allowed in this laboratory
	 */
	final private Quantity MAXSTORAGE;
	
	/**
	 * Returns the maximum storage quantity of this laboratory 
	 */
	public Quantity getMAXSTORAGE(){
		return MAXSTORAGE;
	}
	
	/**************************************************
	 * currentStorage
	 *************************************************/
	/**
	 * The current amount of ingredients in the lab
	 */
	private Quantity currentStorage = new Quantity(0);
	
	/**
	 * Returns the current amount of ingredients in the
	 * lab
	 */
	@Basic 
	public Quantity getCurrentStorage(){
		return currentStorage;
	}
	
	/**
	 * Sets the current amount of ingredients in the lab
	 */
	private void setCurrentStorage(Quantity newQuantity){
		currentStorage = newQuantity;
	}
	
	/**
	 * Checks if the current storage quantity
	 * is valid
	 * @return	True if the sum of the quantities of all the ingredients
	 * 			currently stored is equal to the current storage quantity and
	 * 			the sum is smaller then the maximum allowed quantity
	 * 			| totalQuantity = new Quantity(0)
	 * 			| for each I in 1..getNbOfStoredTypes()
	 * 			|	totalQuantity = totalQuantity.add(getIngredientAt(i).getQuantity()
	 * 			| result == totalQuantity.equals(getCurrentStorage()) &&
	 * 			|				&& getMAXSTORAGE().isGreaterOrEqualTo(totalQuantity)
	 */
	public boolean canHaveAsCurrentStorage(){
		Quantity totalQuantity = new Quantity(0);
		for (int i=1; i<=getNbOfStoredTypes(); i++){
				totalQuantity = totalQuantity.add(getIngredientAt(i).getQuantity());
		}
		return totalQuantity.equals(getCurrentStorage()) && getMAXSTORAGE().isGreaterOrEqualTo(totalQuantity);
	}
	
	/*************************************************
	 * storage
	 ************************************************/
	/**
	 * ArrayList that contains the ingredients currently in the 
	 * laboratory sorted by simple name
	 * @invar The storage list is not null
	 *        | storage != null
	 * @invar Each ingredient in the list references an effective AlchemicIngredient. 
	 *        | for each ingredient in storage:
	 *        |   ingredient != null
	 * @invar Each element in the list references a non-terminated item.
	 *        | for each ingredient in storage:
	 *        | !ingredient.isTerminated()
	 * @invar Each element in the list (except the first element)
	 *        references an item that has a name which (ignoring case)
	 *        comes after the name of the immediately preceding element,
	 *        in lexicographic order. 
	 *        | for each I in 0..storage.size() - 2:
	 *        |   storage.get(I).getType().getName().compareTo(
	 *        |					storage.get(I+1).getType().getName()) < 0
	 * @invar Each ingredient in the storage has a different type than any of the
	 * 		  other ingredients in the storage
	 * 		  | for each I in 0..storage.size()-1:
	 * 		  |		for each J in 0..storage.size()-1:
	 * 		  |			!getIngredientAt(I).getType().equals(getIngredientAt(J).getType())
	 * 		  |				|| I == J
	 * @invar None of the ingredients are in a container
	 * 		  | for each I in 0..storage.size()-1:
	 * 		  |      !storage.at(i).hasContainer()
	 */
	private ArrayList<AlchemicIngredient> storage = new ArrayList<AlchemicIngredient>();
	
	/**
	 * Returns the ingredient at the index position
	 * @param	index
	 * 			The index of the ingredient to get
	 * @throws	IndexOutOfBoudsException
	 * 			The index is not between 1 and the 
	 * 			maximum number of ingredients.
	 */
	@Raw @Model
	private AlchemicIngredient getIngredientAt(int index) 
			throws IndexOutOfBoundsException {
		if (index <= 0 || index > getNbOfStoredTypes())
			throw new IndexOutOfBoundsException();
		return storage.get(index-1);
	}
	
	/**
	 * Returns the type of the ingredient stored at the given
	 * index
	 * @return	The ingredient's type at the given index
	 * 			| getIngredientAt(index).getType()
	 */
	public AlchemicType getTypeAt(int index) 
			throws IndexOutOfBoundsException {
		return getIngredientAt(index).getType();
	}
	
	/**
	 * Returns the quantity of the ingredient stored at the given
	 * index
	 * @return	The ingredient's quantity at the given index
	 * 			| getIngredientAt(index).getQuantity()
	 */
	public Quantity getQuantityAt(int index) 
			throws IndexOutOfBoundsException {
		return getIngredientAt(index).getQuantity();
	}
	
	/**
	 * Returns the volatility deviation of the ingredient stored at the given
	 * index
	 * @return	The ingredient's volatility deviation at the given index
	 * 			| getIngredientAt(index).getQuantity()
	 */
	public double getDeviation(int index) 
			throws IndexOutOfBoundsException {
		return getIngredientAt(index).getVolatilityDeviation();
	}
	
	
	/**
	 * Changes the ingredient at the index positon to
	 * the given ingredient
	 * @param	index
	 * 			the index of the ingredient to change
	 * @param	ingredient
	 * 			the ingredient to change to
	 * @effect	The ingredient at the given position is terminated
	 * 			| getIngrientAt(index).terminate()
	 * @post	The ingredient at the index position is set 
	 * 			to the given ingredient
	 * 			| new.getIngredientAt(index) == ingredient
	 * @throws	IndexOutofBoundsException
	 * 			the index is not in 1 to the current number
	 * 			of types stored.
	 * 			| index <=0 || index > getNbOfStoredTypes
	 * @throws	IllegalArgumentException
	 * 			The ingredient at that position isn't of the same type as the given ingredient
	 * 			| !ingredient.getType().equals(getIngredientAt(index).getType())
	 * @throws	IllegalArgumentException
	 * 			The ingredient is not in the standard state
	 * 			| canBeStored(ingredient)
	 */
	@Raw @Model
	private void setIngredientAt(int index, AlchemicIngredient ingredient)
			throws IllegalArgumentException, IndexOutOfBoundsException{
		if (index <= 0 || index > getNbOfStoredTypes())
			throw new IndexOutOfBoundsException();
		if (!canBeStored(ingredient))
			throw new IllegalArgumentException("The ingredient cannot be added");
		if (!ingredient.getType().equals(getIngredientAt(index).getType()))
			throw new IllegalArgumentException("Can only change ingredients with the same type");
		AlchemicIngredient orignal = getIngredientAt(index);
		storage.set(index-1, ingredient);
		orignal.terminate();
	}
	
	/**
	 * Adds an ingredient at the given index
	 * @param	index
	 * 			The location where the ingredient will
	 * 			be added
	 * @param	newIngredient
	 * 			The ingredient that will be added to storage
	 * @post	The ingredient at the index position will be the given
	 * 			ingredient
	 * 			| new.getIngredientAt(I) == ingredient
	 * @post	Every existing ingredient that was at or after the given
	 * 			index has its index incremented by one.
	 * 			| for each I in index..getNbOfStoredTypes():
	 * 			|	new.getIngredientAt(I+1) = getIngredientAt(I)
	 * @post	The number of types stored is incremented by one	
	 * 			| new.getNbOfStoredTypes() = getNbOfStoredTypes() + 1
	 * @throws	IllegalArgumentException
	 * 			The ingredient is not in the correct state to be stored
	 * 			| !canBeStored(newIngredient)
	 * @throws	IllegalArgumentException
	 * 			There already exists an ingredient in storage with the same type
	 * 			| hasStored(ingredient.getType())
	 * @throws	IllegalArgumentException
	 * 			The index is not valid
	 * 			| !canHaveIngredinetAt(newIngredient.getType(), index)
	 */
	@Raw @Model
	private void addIngredientAt(int index, AlchemicIngredient newIngredient) 
			throws IllegalArgumentException {
		if (!canBeStored(newIngredient))
			throw new IllegalArgumentException("The ingredient cannot be added");
		if (hasStored(newIngredient.getType()))
			throw new IllegalArgumentException("There already exists an ingredient of that type");
		if (!canHaveTypeAt(newIngredient.getType(), index))
			throw new IllegalArgumentException("The location is not valid");
		storage.add(index-1, newIngredient);
	}
	
	/**
	 * Removes the ingredient at the index position
	 * @param	index
	 * 			The index of the ingredient to remove
	 * @post	The number of ingredients is reduced by one
	 * 			| new.getNbOfStoredTypes() == getNbOfStoredTypes() - 1
	 * @throws	IndexOutOfBoudsException
	 * 			The index is not in between 1 and the 
	 * 			number of ingredients.
	 * 			| index <= 0 || index > getNbOfStoredTypes()
	 */
	@Model
	private void removeIngredientAt(int index) 
			throws IndexOutOfBoundsException {
		if (index <= 0 || index > getNbOfStoredTypes())
			throw new IndexOutOfBoundsException();
		storage.remove(index-1);
	}
	
	/**
	 * Checks if there exists an ingredient with the given type in storage
	 * using binary search
	 * @param	type
	 * 			The IngredientType to search for
	 * @return	True if there exists an ingredient with the same type
	 * 			as the given type and the type is not null
	 * 			| result == (type !=null) && (find(type) != -1)
	 */
	public boolean hasStored(AlchemicType type){
		if (type == null)
			return false;
		else			
			return (find(type) != -1);
	}
	
	/**
	 * Finds the index of the ingredient with the same ingredienttype
	 * @param	type
	 * 			The type to find
	 * @return	True the index of the ingredient that has the same
	 * 			type or minus one if none are found.
	 * 			| for each I in 1..getNbOfStoredTypes():
	 * 			|	if (getIngredientAt(i).getType() == type)
	 * 			|	then result == i
	 * 			| result == -1
	 */
	@Raw
	public int find(AlchemicType type){
		int min = 1;
		int max = getNbOfStoredTypes();
		while (min <= max) {
			int middle = (min+max)/2;
			AlchemicIngredient middleItem = getIngredientAt(middle);
			if(middleItem.getType() == type)
				return middle;
			if(min == max )
				return -1;
			if (isStoredBefore(middle, type)) {
				max = middle;
			} else {
				min = middle+1;
			}
		}
		return -1;
	}
	
	/**
	 * Finds the index of the ingredient with the same simple name
	 * @param	name
	 * 			The name of the ingredient to find
	 * @return	The index of the ingredient that has the same
	 * 			name or minus one if none are found.
	 * 			| for each I in 1..getNbOfStoredTypes():
	 * 			|	if (getIngredientAt(i).getType().getSimpleName == name)
	 * 			|	then result == i
	 * 			| result == -1
	 */
	@Raw
	public int findSimple(String name){
		if (name == null)
			return -1;
		int min = 1;
		int max = getNbOfStoredTypes();
		while (min <= max) {
			int middle = (min+max)/2;
			AlchemicIngredient middleItem = getIngredientAt(middle);
			if(middleItem.getType().getSimpleName().equals(name))
				return middle;
			if(min == max)
				return -1;
			if (middleItem.getType().getSimpleName().compareTo(name) > 0) {
				max = middle;
			} else {
				min = middle+1;
			}
		}
		return -1;
	}
	
	/**
	 * Finds the index of the ingredient with the same special name
	 * @param	name
	 * 			The name of the ingredient to find
	 * @return	The index of the ingredient that has the same
	 * 			name or minus one if none are found.
	 * 			| for each I in 1..getNbOfStoredTypes():
	 * 			|	if getIngredientAt(i).getType() instanceof AlchemicTypeMixed && 
	 * 			|		(AlchemicTypeMixed)getIngredientAt(i).getType().getSpecialName().equals(name))
	 * 			|	then result == i
	 * 			| result == -1
	 */
	@Raw
	public int findSpecial(String name){
		if (name == null)
			return -1;
		for (int i = 1; i <= getNbOfStoredTypes(); i++){
			if (getIngredientAt(i).getType() instanceof AlchemicTypeMixed && 
					((AlchemicTypeMixed)getIngredientAt(i).getType()).getSpecialName().equals(name))
				return i;
		}
		return -1;
	}
	
	/**
	 * Returns the quantity that is in storage of 
	 * a type with a given name (simple or special)
	 * @param	name
	 * 			The name of the ingredient to look for
	 * @return	If there is a ingreident stored with the same
	 * 			simplename or specialname then the quantity of that ingredient
	 * 			is returned. Else a zero quantity is returned.
	 * 			| index = findSimple(name)
	 * 			| if (index == -1)
	 * 			| then	index = findSpecial(name)
	 * 			| if (index != -1 )
	 * 			| then result == getIngredientAt(index).getQuantity()
	 * 			| else result == new Quantity(0)
	 */
	public Quantity getStoredQuantityOf(String name){
		int index = findSimple(name);
		if (index == -1){
			index = findSpecial(name);
		}
		
		if (index == -1){
			return new Quantity(0);
		}
		else
			return getIngredientAt(index).getQuantity();
	}
	
	/**
	 * Store the ingredient in the container in the laboratory
	 * @param	transpContainer
	 * 			The container with the ingredient in it
	 * @throws 	IllegalArgumentException
	 * 			The given container is null
	 * 			| transpContainer == null
	 * @throws  IllegalArgumentException
	 * 			The container has no ingredient
	 * 			| !transpContainer.hasIngredient()
	 * @throws  IllegalStateException
	 * 			The laboratory doesn't have enough storage to add 
	 * 			this ingredient.
	 * 			| getMAXSTORAGE().subStract(ingr.getQuantity()).getQuantity() 
	 * 			|		< getCurrentStorage().getQuantity()
	 * @effect	The currentStorage is increased by the amount of the ingredient
	 * 			| setCurrentStorage(getCurrentStorage().add(ingr.getQuantity()))
	 * @effect	If there already exists an ingredient with the same type as the given
	 * 			ingredient then that ingredient is replaced with an ingredient with the
	 * 			same type, a quantity equals to the sum of the original ingredient and
	 * 			the given ingredient's quantity and the volatilityDeviation equal to the
	 * 			weighted average of the volatility deviation of the orginal ingredient and 
	 * 			the given ingredient with as weight the quantities of the ingredients. 
	 * 			Afterwards both the original as the given ingredient are terminated. The 
	 * 			inputcontainer is terminated as well.
	 * 			| if (hasStored(transpContainer.getIngredient().getType()))
	 * 			| then	
	 * 			|		ingr = ingredientContainer.getIngredientAt
	 * 			|		existingIngredient = getIngredientAt(find(ingr.getType()))
	 * 			|		ingrArray = [ existingIngredient, transpContainer.getIngredient()) ]
	 * 			|		newIngreident 
	 *			|				= new AlchemicIngredient(existingIngredient.getType(),
	 *			|							  existingIngredient.getQuantity().add(ingr.getQuantity()),
	 *			|							  AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(ingrArray))
	 *			| 		existingIngredient.terminate()
	 *			|		transportContainer.terminate()
	 *			|		setIngredientAt(find(ingr.getType()), newIngredient)
	 * @effect	If there are no existing ingredients with the same type then a new ingredient
	 * 			is created with the same quantity, type and volatilityDeviation. But with its
	 * 			temperature and state set to the type's standard. It is inserted in storage so
	 * 			that the simple name of the ingredient is ordered lexographic correctly 
	 * 			in respect to the ingredient before and after it. The inputingredient and
	 * 			container is terminated. 
	 * 			| if (!hasStored(transportContainer.getIngredient().getType())
	 * 			| then for each I in 2..getNbOfStoredTypes()
	 * 			|			if (getIngredientAt(i-1).getType().getSimpleName().compareTo(ingr.getType().getSimpleName()) < 0) &&
	 * 			|					getIngredientAt(i).getType().getSimpleName().compareTo(ingr.getType().getSimpleName()) >= 0)	
	 * 			|			then location = I
	 *			| 		if 	getIngredientAt(i).getType().getSimpleName().compareTo(ingr.getType().getSimpleName()) >= 0)	
	 * 			|			then location = 1
	 * 			|		if (getIngredientAt(getNbOfSavedTypes()).getType().getSimpleName().compareTo(ingr.getType().getSimpleName()) < 0)	
	 * 			|			then location = getNbOfSavedTypes()
	 * 			|		
	 * 			|		newIngredient = new AlchemicIngredient(existingIngredient.getType(),
	 * 			|							  existingIngredient.getQuantity().add(ingr.getQuantity()),
	 * 			|							  AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(ingrArray))
	 * 			|		addIngredientAt(location, newIngredient)
	 * 			|		existingIngredient.terminate()
	 * 			|		transportContainer.terminate()
	 */
	public void storeIngredient(TransportContainer transpContainer)
			throws IllegalStateException, IllegalArgumentException {
		if (transpContainer == null)
			throw new IllegalArgumentException("Cannot add null");
		if (!transpContainer.hasIngredient())
			throw new IllegalArgumentException("Cannot add nothing to the storage");
		
		AlchemicIngredient ingr = transpContainer.getIngredient();
		if (getMAXSTORAGE().subStract(ingr.getQuantity()).getQuantity() < getCurrentStorage().getQuantity())
			throw new IllegalStateException("The laboratory doesn't have enough space for the ingredient");
		
		
		//expand current storage total, start raw state
		setCurrentStorage(getCurrentStorage().add(ingr.getQuantity()));
		
		// There exist an ingredient with the same type:
		if (hasStored(ingr.getType())){
			int location = find(ingr.getType());
			AlchemicIngredient existingIngredient = getIngredientAt(location);
			AlchemicIngredient[] ingrArray = new AlchemicIngredient[2];
			ingrArray[0] = ingr;
			ingrArray[1] = existingIngredient;
			AlchemicIngredient newIngredient 
					= new AlchemicIngredient(existingIngredient.getType(),
											  existingIngredient.getQuantity().add(ingr.getQuantity()),
											  AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(ingrArray));
			setIngredientAt(location, newIngredient);
			
		}
		// No ingredient exists with the same type
		else {
			//find location where the new ingredient will fit
			int min = 1;
			int max = getNbOfStoredTypes()+1;
			while (min <= max) {
				int middle = (min+max)/2;
				if(min == max)
					break; // ends the loop
				if (isStoredBefore(middle, ingr.getType())) {
					max = middle;
				} else {
					min = middle+1;
				}
			}
			addIngredientAt(min, new AlchemicIngredient(ingr.getType(), 
														ingr.getQuantity(), 
														ingr.getVolatilityDeviation()));
		}
		ingr.terminate();
		transpContainer.terminate();
		// end raw state		
	}
	
	/**
	 * Checks if a type is stored before a given index
	 * @param	Index
	 * 			The index of the ingredient to compare the type to
	 * @param 	type
	 * 			The type to check
	 * @return	True if the name of the ingredient at the given index is 
	 * 			lexicographicly ordered after the name of the given type. The result is false if 
	 * 			the given type is null
	 * 			| result == (type =! null) && middleItem.getType().getSimpleName().compareTo(ingr.getType().getSimpleName()) < 0
	 * @throws  IndexOutOfBoundsException
	 * 			The index is not in bounds
	 * 			| index <=0 || index > getNbOfStoredTypes()
	 */
	private boolean isStoredBefore(int index, AlchemicType type) throws IndexOutOfBoundsException {
		if (index <=0 || index > getNbOfStoredTypes())
			throw new IndexOutOfBoundsException();
		if (type == null)
			return false;
		return type.getSimpleName().compareTo(getIngredientAt(index).getType().getSimpleName()) < 0;
	}
	
	/**
	 * Checks if an ingredient is stored after a given index
	 * @param	Index
	 * 			The index of the ingredient to compare the type to
	 * @param 	type
	 * 			The type to check
	 * @return	True if the name of the ingredient at the given index is 
	 * 			lexicographicly ordered before the name of the given type. If the given type is null
	 * 			then the result is false
	 * 			| result == (type != null) && middleItem.getType().getSimpleName().compareTo(ingr.getType().getSimpleName()) > 0
	 * @throws  IndexOutOfBoundsException
	 * 			The index is not in bounds
	 * 			| index <=0 || index > getNbOfStoredTypes()
	 */
	private boolean isStoredAfter(int index, AlchemicType type) throws IndexOutOfBoundsException {
		if (index <=0 || index > getNbOfStoredTypes())
			throw new IndexOutOfBoundsException();
		if (type == null)
			return false;
		return type.getSimpleName().compareTo(getIngredientAt(index).getType().getSimpleName()) > 0;
	}
	
	/**
	 * Extract ingredient from this laboratory and 
	 * puts it in the given container. If there is more 
	 * than the capacity of the biggest container then the remaining
	 * ingredient is removed.
	 * @param	name
	 * 			The simple name of the ingredient
	 * @post	The laboratory no longer contains any of the ingredient
	 * 			returned. (if there was one returned)	
	 * 			| !hasStored(result.getIngredient().getType())
	 * @return	A container containing the ingredient
	 * 			with a simplename equal to the given name
	 * 			or an empty spoon if there are no matches
	 * 			| index = findSimple(name)
	 * 			| if (index == -1)
	 * 			| then 	index = findSpecial(name)
	 * 			| if (index == -1)
	 * 			| then	result == new TransportContainer(StdContainers.SPOON)
	 * 			| else 
	 * 			|		amount = Math.min(getIngredientAt(index).getQuantity().getQuantity(),
	 *			|				StdContainers.biggestContainerOf(getIngredientAt(index).getState()).getQuantity())
	 *			|		returnIngre = 
	 *			|						new AlchemicIngredient( getIngredientAt(index).getType(),
	 *			|						new Quantity(amount),
	 *			|						getIngredientAt(index).getVolatilityDeviation())
	 *			|		setCurrentStorage(getCurrentStorage().subStract(getIngredientAt(index).getQuantity()))
	 *			|		removeIngredientAt(index)
	 *			|		result == new TransportContainer(returnIngre)
	 */
	public TransportContainer extract(String name){
		int index = findSimple(name);
		if (index ==-1)
			index = findSpecial(name);
		if (index == -1)
			return new TransportContainer(StdContainers.SPOON);
		
		long amount = Math.min(getIngredientAt(index).getQuantity().getQuantity(),
								StdContainers.biggestContainerOf(getIngredientAt(index).getState()).getQuantity());
		AlchemicIngredient returnIngre = 
				new AlchemicIngredient( getIngredientAt(index).getType(),
										new Quantity(amount),
										getIngredientAt(index).getVolatilityDeviation());
		setCurrentStorage(getCurrentStorage().subStract(getIngredientAt(index).getQuantity()));
		removeIngredientAt(index);
		return new TransportContainer(returnIngre);			
	}
	
	/**
	* Extracts an amount of ingredient from this container.
	* @param	name
	* 			The simple name of the ingredient to
	* 			extract
	* @param	quantity
	* 			The amount to extract
	* @return	A container containing a ingredient with the same simple or 
	* 			specialname (in case of mixed type)
	* 			as the given string. The container has no ingredient if 
	* 			there are no matches to the name or the requested quantity
	* 			is equal to zero or the requested quantity is zero.
 	*			The amount of ingredient is zero if there is
 	*			no ingredient in storage with the same name, 
 	*			Or the minimum of the given quantity, quantity
 	*			in storage, and the capacity of the biggest 
 	*			container that can container the ingredients state
	* 			| index = findSimple(name)
	* 			| if (index == -1) 
	* 			| then 	index = findSpecial(name)
	* 			| if (index == -1 || quantity == null || quantity.getQuantity() == 0)
	* 			| then	result == new TransportContainer(StdContainers.SPOON)
	* 			| else 
	* 			|		amount = Math.min(getIngredientAt(index).getQuantity().getQuantity(),
	*			|							StdContainers.biggestContainerOf(getIngredientAt(index).getState()).getQuantity(),
	*			|							quantity)
	*			|		returnIngre = 
	* 			|						new AlchemicIngredient( getIngredientAt(index).getType(),
	*			|						new Quantity(amount),
	*			|						getIngredientAt(index).getVolatilityDeviation())
    *           |		setCurrentStorage(getCurrentStorage().subStract(new Quantity(amount)));
	*			|		if (returnIngre.getQuantity().equals(getIngredientAt(index))
	*			|		then	removeIngredientAt(index)
	*			|		else 	setIngredientAt(index, new AlchemicIngredient(getIngredientAt(index).getType(),
	*			|								      			  getIngredientAt(index).getQuantity().subtract(returnIngr.getQuantity()),
	*			|												  getIngredientAt(index).getVolatility());
	*			|		result == new TransportContainer(returnIngre);
	*/
	public TransportContainer extract(String name, Quantity quantity){
		int index = findSimple(name);
		if (index ==-1)
			index = findSpecial(name);
		if (index == -1 || quantity == null || quantity.getQuantity() ==0)
			return new TransportContainer(StdContainers.SPOON);
		
		// take minimum of the requested quantity, available quantity and the quantity that fits in the biggest container
		long amount = Math.min(quantity.getQuantity(),
								StdContainers.biggestContainerOf(getIngredientAt(index).getState()).getQuantity());
		amount = Math.min(amount, getIngredientAt(index).getQuantity().getQuantity());
		AlchemicIngredient returnIngre = 
				new AlchemicIngredient( getIngredientAt(index).getType(),
										new Quantity(amount),
										getIngredientAt(index).getVolatilityDeviation());
		setCurrentStorage(getCurrentStorage().subStract(new Quantity(amount)));
		if (returnIngre.getQuantity().equals(getIngredientAt(index).getQuantity()))
			removeIngredientAt(index);
		else 	
			setIngredientAt(index, new AlchemicIngredient(getIngredientAt(index).getType(),
													getIngredientAt(index).getQuantity().subStract(returnIngre.getQuantity()),
													getIngredientAt(index).getVolatilityDeviation()));
		return new TransportContainer(returnIngre);		
	}
	/**
	 * Checks if an ingredient can be stored
	 * @param	ingredient
	 * 			The ingredient to be checked
	 * @return	True if the ingredient is not null, not terminated, has all 
	 * 			its parameters set to the standard parameters of its type
	 * 			(except volatility)
	 * 			| result == (ingredient != null) && !ingredient.hasContainer() &&
	 * 			|	ingredient.getType().getStandardTemperature() == ingredient.getTemperature()
	 * 			|		&& ingredient.getType().getStandardState() == ingredient.getState()
	 */
	private boolean canBeStored(AlchemicIngredient ingredient){
		return (ingredient != null) && !ingredient.hasContainer() &&
				ingredient.getType().getStandardTemperature() == ingredient.getTemperature()
				 && ingredient.getType().getStandardState() == ingredient.getState();
	}
	
	/**
	 * The number of different types in storage, this is also
	 * the number of ingredients because of the invariants
	 */
	public int getNbOfStoredTypes() {
		return storage.size();
	}
	
	/**
	 * Checks if the storage can have the ingredienttype at the 
	 * index position.
	 * @param	index
	 * 			The location to be checked.
	 * @return	If this index is not between 1 and the number of types in this laboratory
	 * 			then the result is false
	 * 			else
	 * 			If the ingredient at the given index has the same
	 * 			type as the given type 
	 * 			then true if the ingredient before it (if it exists) has a name
	 * 						that is ordered before the simplename of the type
	 * 					and the ingredient after it (if it exists) has a name 
	 * 						that is ordered after the simplename of the type
	 * 			If the ingredient at the given index doesn't have the 
	 * 			same type as the given type
	 * 			then true if the ingredient before the index (if it exists) has a name
	 * 						that is ordered before the simplename of the type
	 * 					and the ingredient at the index has a name 
	 * 						that is ordered after the simplename of the type
	 * 			|   if (index <= 0 || index > getNbOfStoredTypes())
	 * 			|		result == false
	 * 			|	else
	 * 			|	if (getIngredientAt(index).getType().equals(ingredientType))
	 * 			|	then result == ((i == 1 
	 * 			|				|| getIngredientAt(i-1).getType.getSimpleName().compareTo(
	 * 			|					type.getSimpleName()) >= 0) &&
	 * 			|			   (i == getNbOfStoredTypes()
	 * 			|				|| getIngredientAt(i+1).getType.getSimpleName().compareTo(
	 * 			|					type.getSimpleName()) <= 0))
	 * 			|	if (!getIngredientAt(index).getType().equals(ingredientType))
	 * 			|	then result == ((i == 1 
	 * 			|				|| getIngredientAt(i-1).getType.getSimpleName().compareTo(
	 * 			|					type.getSimpleName()) >= 0) &&
	 * 			|			   (i == getNbOfStoredTypes() + 1
	 * 			|				|| getIngredientAt(i).getType.getSimpleName().compareTo(
	 * 			|					type.getSimpleName()) <= 0))	
	 * @note	This method does not check if there already is one of this type in the lab	
	 */
	public boolean canHaveTypeAt(AlchemicType type, int index){
		if (index <= 0 || index > getNbOfStoredTypes()+1)
			return false;
		if (index != getNbOfStoredTypes()+1 && getIngredientAt(index).getType().equals(type))
			return ((index == 1 
						|| isStoredAfter(index-1, type))
					 &&(index == getNbOfStoredTypes()
							|| isStoredBefore(index+1, type)));
		else{
			return ((index == 1 
							|| isStoredAfter(index-1, type))
						&&(index == getNbOfStoredTypes() +1
							|| isStoredBefore(index, type)));
		}
	}
	
	/**
	 * Checks if this laboratory has proper ingredients
	 * stored
	 * @return  true if for every ingredient in storage
	 * 			its type is unique in storage. If its index is correct,
	 *          and in the right state
	 *          | for each I in 1..getNbOfStoredTypes()
	 *          |		for each J in 1..getNbOfStoredTypes()
	 *          |			I == J ||
	 *          |			 getIngredientAt(I).getType() != getIngredientAt(J).getType()
	 *          |		canBeStored(getIngredientAt(I))
	 *          |		canHaveTypeAt(getIngredientAt(I).getType(), I)
	 */
	public boolean hasProperStorage(){
		for (int i = 1; i <=getNbOfStoredTypes(); i++){
			for (int j = 1; j <=getNbOfStoredTypes(); j++){
				if (i!=j &&
				 getIngredientAt(i).getType() == getIngredientAt(j).getType())
					return false;
			}
			if (!canBeStored(getIngredientAt(i)))
				return false;
			if (!canHaveTypeAt(getIngredientAt(i).getType(), i))
				return false;
		}
		return true;
	}
	
	/**
	 * Returns a StorageIterator with all the ingredients 
	 * currently stored in this laboratory
	 * @return  An iterator with all the elements of the laboratory. 
	 * 			Every item of the Laboratory is present in the iterator object, the
	 * 			order of the Ingredients are the same as in the initial Laboratory. The 
	 * 			number of items left in the Iterator is the same as the number
	 * 			of Ingredients in Storage. 
	 */
	public StorageIterator getIterator(){
		return new StorageIterator(){
			/**
			 * Copy of the storage list
			 */
			ArrayList<AlchemicIngredient> storedIngredients = new ArrayList<AlchemicIngredient>(storage);
			/**
			 * Where the iterator is currently at
			 */
			private int index = 0;
			
			/**
			 * How many Ingredients are remaining in the iterator, including
			 * the current one.
			 */
			public int getNbRemainingIngredients() {
				return storedIngredients.size() - index;
			}
			
			/**
			 * Returns the type of the ingredient at the current index
			 * @return	The current item
			 * @throws	IndexOutOfBoundsException
			 * 			This StorageIterator has no current ingredient.
			 * 			| getNbRemainingItems() == 0
			 */
			public AlchemicType getCurrent() throws IndexOutOfBoundsException {
				if(index>=storedIngredients.size()|| index<0){
					throw new IndexOutOfBoundsException();
				}
				return storedIngredients.get(index).getType();
			}
			
			/**
			 * Returns the quantity of the ingredient at the current index
			 * @return	The current item
			 * @throws	IndexOutOfBoundsException
			 * 			This StorageIterator has no current ingredient.
			 * 			| getNbRemainingItems() == 0
			 */
			public Quantity getQuantity() throws IndexOutOfBoundsException {
				if(index>=storedIngredients.size()|| index<0){
					throw new IndexOutOfBoundsException();
				}
				return storedIngredients.get(index).getQuantity();
			}
			/**
			 * Returns the volatilityDeviation of the stored ingredient at the
			 * current index.
			 * @return	The current item
			 * @throws	IndexOutOfBoundsException
			 * 			This StorageIterator has no current ingredient.
			 * 			| getNbRemainingItems() == 0
			 */
			public double getVolatilityDeviation() throws IndexOutOfBoundsException {
				if(index>=storedIngredients.size()|| index<0){
					throw new IndexOutOfBoundsException();
				}
				return storedIngredients.get(index).getVolatilityDeviation();
			}
			
			/**
			 * Advance the index of this laboratory.
			 * @pre		The storageIterator must have some remaining ingredients
			 * 			| getNbOfRemainingIngredients() > 1
			 * @post	The number of remaining items is decreased
			 * 			| new.getNbOfRemainingIngredients() ==  
			 * 			|			new.getNbOfRemainingIngredients() - 1
			 */
			public void advance() {
				index++;
			}

			/**
			 * Reset the StorageIterator
			 */
			public void reset() {
				index = 0;
			}
			
			/**
			 * Checks if the iterator has a remaining ingredient after 
			 * this one
			 * @return 	true if the number of ingredients remaining is greater
			 * 			than one.
			 * 			| getNbOfRemainingIngredients() > 1
			 */
			public boolean hasNext() {
				return getNbRemainingIngredients() > 1;
			}
			
		};
	}
	
	
	/*************************************************
	 * coolingBox
	 *************************************************/
	private CoolingBox coolingBox = null;
	
	/**
	 * Returns the cooling box of this laboratory
	 */
	@Basic
	public CoolingBox getCoolingBox(){
		return coolingBox;
	}
	
	/**
	 * Set the coolingBox of this laboratory
	 * @param	coolingbox
	 * 			The new cooling box
	 * @throws	IllegalStateException
	 * 			The laboratory has a cooling box and its laboratory is 
	 * 			set to this.
	 * 			| hasCoolingBox() && getCoolingBox().getLaboraty() == this
	 * @throws	IllegalArgumentException
	 * 			The given coolingbox is not null and does not have it's laboratory 
	 * 			set to this.
	 * 			| coolingbox != null && coolingbox.getLaboratory() != this
	 */ 
	@Raw @Model
	protected void setCoolingBox(CoolingBox coolingbox) throws IllegalStateException{
		if (hasCoolingBox() && getCoolingBox().getLaboratory() == this)
			throw new IllegalStateException();
		if (coolingbox != null && coolingbox.getLaboratory() != this)
			throw new IllegalArgumentException();
		this.coolingBox = coolingbox;
	}
	
	/**
	 * Checks if this laboratory has a coolingbox
	 * @return  True if the laboratory has a coolingbox
	 * 			| result == (getCoolingBox() != null)
	 */
	public boolean hasCoolingBox(){
		return getCoolingBox() != null;
	}
	
	/**
	 * Checks if this laboratory has proper coolingBox
	 * @return True if the laborary has no coolingBox or 
	 * 			the cooling box has this as its laboratory
	 * 			| result == (!hasCoolingBox() || getCoolingBox().getLaboratory() == this)
	 */
	public boolean hasProperCoolingBox(){
		return (!hasCoolingBox() || getCoolingBox().getLaboratory() == this);
	}
	
	/*************************************************
	 * Oven
	 *************************************************/
	private Oven oven = null;
	
	/**
	 * Returns the oven of this laboratory
	 */
	@Basic
	public Oven getOven(){
		return oven;
	}
	
	/**
	 * Set the oven of this laboratory
	 * @param	oven
	 * 			The new oven
	 * @throws	IllegalStateException
	 * 			The laboratory has an oven and
	 * 			the oven still has this laboratory as its laboratory
	 * 			| hasOven() && getOven().getLaboratory() == this
	 * @throws	IllegalArgumentException
	 * 			The given oven is not null and does not have it's laboratory 
	 * 			set to this.
	 * 			| oven != null && oven.getLaboratory() != this
	 */
	@Raw @Model
	protected void setOven(Oven oven) throws IllegalStateException{
		if (hasOven() && getOven().getLaboratory() == this)
			throw new IllegalStateException();
		if (oven != null && oven.getLaboratory() != this)
			throw new IllegalArgumentException();
		this.oven = oven;
	}
	
	/**
	 * Checks if this laboratory has a oven
	 * @return  true if the laboratory has a oven
	 * 			| result == (getOven() != null)
	 */
	public boolean hasOven(){
		return getOven() != null;
	}
	
	/**
	 * Checks if this laboratory has proper oven
	 * @return True if the laborary has no oven or 
	 * 			the oven has this as its laboratory
	 * 			| result == (!hasOven() || getOven().getLaboratory() == this)
	 */
	public boolean hasProperOven(){
		return (!hasOven() || getOven().getLaboratory() == this);
	}
	
	/*************************************************
	 * Kettle
	 *************************************************/
	private Kettle kettle = null;
	
	/**
	 * Returns the kettle of this laboratory
	 */
	@Basic
	public Kettle getKettle(){
		return kettle;
	}
	
	/**
	 * Set the kettle of this laboratory
	 * @param	kettle
	 * 			The new kettle
	 * @throws	IllegalStateException
	 * 			The laboratory already has a kettle and it has its
	 * 			laboratory set to this.
	 * 			| hasKettle() && getKettle().getLaboratory() == this
	 * @throws	IllegalArgumentException
	 * 			The given kettle is not null and does not have it's laboratory 
	 * 			set to this.
	 * 			| kettle != null && kettle.getLaboratory() != this
	 */
	@Raw @Model
	protected void setKettle(Kettle kettle) 
			throws IllegalStateException, IllegalArgumentException{
		if (hasKettle() && getKettle().getLaboratory() == this)
			throw new IllegalStateException();
		if (kettle != null && kettle.getLaboratory() != this)
			throw new IllegalArgumentException();
		this.kettle = kettle;
	}
	
	/**
	 * Checks if this laboratory has a kettle
	 * @return  true if the laboratory has a kettle
	 * 			| result == (getKettle() != null)
	 */
	public boolean hasKettle(){
		return getKettle() != null;
	}
	
	/**
	 * Checks if this laboratory has proper kettle
	 * @return true if the laboratory has no kettle or 
	 * 			the kettle has this as its laboratory
	 * 			| (!hasKettle() || getKettle().getLaboratory() == this)
	 */
	public boolean hasProperKettle(){
		return (!hasKettle() || getKettle().getLaboratory() == this);
	}
	
	/*************************************************
	 * Transmogifier
	 *************************************************/
	private Transmogifier transmogifier = null;
	
	/**
	 * Returns the transmogifier of this laboratory
	 */
	@Basic
	public Transmogifier getTransmogifier(){
		return transmogifier;
	}
	
	/**
	 * Set the coolingBox of this laboratory
	 * @param	transmogifier
	 * 			The new transmogifier
	 * @throws	IllegalStateException
	 * 			The laboratory allready has a transmogifier and
	 * 			it has its laboratory set to this.
	 * 			| hasTransmogifier() &&  getTransmogifier.getLaboratory() != this
	 * @throws	IllegalArgumentException
	 * 			The given transmogifier is not null and does not have its laboratory 
	 * 			set to this.
	 * 			| transmogifier != null && transmogifier.getLaboratory() != this
	 */
	@Raw @Model
	protected void setTransmogifier(Transmogifier transmogifier) 
			throws IllegalStateException{
		if (hasTransmogifier() &&  getTransmogifier().getLaboratory() == this)
			throw new IllegalStateException();     
		if (transmogifier != null && transmogifier.getLaboratory() != this)
			throw new IllegalArgumentException();
		this.transmogifier = transmogifier;
	}
	
	/**
	 * Checks if this laboratory has a transmogifier
	 * @return  true if the laboratory has a transmogifier
	 * 			| result == (getTransmogifier() != null)
	 */
	public boolean hasTransmogifier(){
		return getTransmogifier() != null;
	}
	
	/**
	 * Checks if this laboratory has proper transmogifier
	 * @return true if the laboratory has no transmogifier or 
	 * 			the transmogifier has this as its laboratory
	 * 			| result == (!hasTransmogifier() || getTransmogifier().getLaboratory() == this)
	 */
	public boolean hasProperTransmogifier(){
		return (!hasTransmogifier() || getTransmogifier().getLaboratory() == this);
	}
	
	/**
	 * Executes a given recipe a given amount of time
	 */
	public void excecuteRecipe(Recipe recipe, int amount){
		for (int i =0; i < amount && excecuteRecipe(recipe); i++);
	}
	
	/**
	 * Executes a given recipe once
	 * @return 	True if the entire recipe has 
	 * 			been successfully excecuted.
	 */
	public boolean excecuteRecipe(Recipe recipe){
		Quantity totalQuantity = new Quantity(0);
		// Check if the sum of all the ingredients will not overflow the quantity max value
		for (int i = 1; i <= recipe.getNbOfIngredients(); i++){
			if (Long.MAX_VALUE - totalQuantity.getQuantity() <  Recipe.extractQuantity(recipe.getIngredientAt(i)).getQuantity())
				return false;
			else 
				totalQuantity = totalQuantity.add(Recipe.extractQuantity(recipe.getIngredientAt(i)));
		}
		
		boolean canContinue = true;
		TransportContainer extractContainer;
		int atIngredient = 1;
		ArrayList<AlchemicIngredient> intermediates = new ArrayList<AlchemicIngredient>();
		for (int i = 1; i <= recipe.getNbOfSteps() && canContinue; i++){
			switch (recipe.getRecipeCommandAt(i)){
				case ADD:
					// extract the ingredient from storage
					extractContainer = this.extract(Recipe.extractName(recipe.getIngredientAt(atIngredient)),
													Recipe.extractQuantity(recipe.getIngredientAt(atIngredient)));
					// if the ingredient is not found end the recipe
					if (!extractContainer.hasIngredient()){
						canContinue = false;
						break;
					}
					// add the ingredient to the intermediates at last position
					intermediates.add(extractContainer.getIngredient());
					extractContainer.terminate();
					// increment the count of which ingredient to add
					atIngredient++;
					break;
				case MIX:
					// Check if the lab has a kettle to mix with
					if (!hasKettle()){
						canContinue = false;
						break;
					}
					//As long as there is more then one ingredient in the intermediates
					while(intermediates.size() > 1){
						// emtpy the inputcontainers
						getKettle().getInputAt(1).empty();
						getKettle().getInputAt(2).empty();
						// move the 0th and 1th intermediate to the input of the kettle
						intermediates.get(0).move(getKettle().getInputAt(1));
						intermediates.get(1).move(getKettle().getInputAt(2));
						// remove the 0th and 1th intermediate from the intermediates
						intermediates.remove(0);
						intermediates.remove(0);
						// mix the two
						getKettle().process();
						// add the result to the intermediates
						intermediates.add(getKettle().getOutputAt(1).getIngredient());
						// empty kettle output (or it would get terminated on the next mix)
						getKettle().getOutputAt(1).empty();
					}
					break;
				case COOL:
					// Checks if the laboratory has a cooling box
					if (!this.hasCoolingBox()){
						canContinue = false;
						break;
					}
					// if there is a last intermediate 
					if (!intermediates.isEmpty()){
						// empty inputslot
						getCoolingBox().getInputAt(1).empty();
						// set the cooling box's temperature
						getCoolingBox().setTemperature(intermediates.get(intermediates.size()-1).getTemperature().cool(50));
						// put the last intermediate in the inputslot
						intermediates.get(intermediates.size()-1).move(getCoolingBox().getInputAt(1));
						// cool
						getCoolingBox().process();
						// set the changed ingredient
						intermediates.set(intermediates.size()-1, getCoolingBox().getOutputAt(1).getIngredient());
						// empty outputcontainer
						getCoolingBox().getOutputAt(1).empty();
					}
					break;
				case HEAT:
					// Checks if the laboratory has an oven
					if (!this.hasOven()){
						canContinue = false;
						break;
					}
					// check if there is an ingredient in the laboratory
					if (!intermediates.isEmpty()){
						// empty the input of the oven
						getOven().getInputAt(1).empty();
						// set the temperature of the oven
						getOven().setTemperature(intermediates.get(intermediates.size()-1).getTemperature().heat(50));
						// add the ingredient to the input of the oven
						intermediates.get(intermediates.size()-1).move(getOven().getInputAt(1));
						// process the oven
						getOven().process();
						// get the output
						intermediates.set(intermediates.size()-1, getOven().getOutputAt(1).getIngredient());
						// emtpy the ouput
						getOven().getOutputAt(1).empty();
					}
					break;
				default:
					canContinue = false;
					break;
			}
		}
		//mix result if the last step was not mix and the previous steps was successfull
		if (canContinue && intermediates.size()>1){
			// Check if the lab has a kettle to mix with
			if (!hasKettle()){
				canContinue = false;
			}
			else {
				//As long as there is more then one ingredient in the intermediates
				while(intermediates.size() > 1){
					// empty the inputcontainers
					getKettle().getInputAt(1).empty();
					getKettle().getInputAt(2).empty();
					// move the 0th and 1th intermediate to the input of the kettle
					intermediates.get(0).move(getKettle().getInputAt(1));
					intermediates.get(1).move(getKettle().getInputAt(2));
					// remove the 0th and 1th intermediate from the intermediates
					intermediates.remove(0);
					intermediates.remove(0);
					// mix the two
					getKettle().process();
					// add the result to the intermediates
					intermediates.add(getKettle().getOutputAt(1).getIngredient());
					// empty kettle output (or it would get terminated on the next mix)
					getKettle().getOutputAt(1).empty();
				}
			}
		}
		
		// put all the intermediate products in storage. 
		for (int i = 0; i < intermediates.size(); i++){
			long remainingQuantity = intermediates.get(i).getQuantity().getQuantity();
			AlchemicIngredient original = intermediates.get(i);
			// have to iterate in case the amount is greater then the maximum capacity of the
			// biggest container
			while (remainingQuantity > 0){
				long amount = Math.min(remainingQuantity,
									   StdContainers.biggestContainerOf(original.getState()).getQuantity());
				AlchemicIngredient ingr = new AlchemicIngredient(original.getType(), 
																 original.getState(), 
																 new Quantity(amount), 
																 original.getTemperature());
				this.storeIngredient(new TransportContainer(ingr));
				remainingQuantity = remainingQuantity - amount;
			}
			original.terminate();
		}
		return canContinue;
	}
}
