package Alchemy;

import be.kuleuven.cs.som.annotate.*;

/**
 * An class that creates immutable quantity objects
 * and compares quantities of ingredients
 * @author 	Maarten Craeynest, Antony De Jonckhhere
 * @version 1.0
 * @invar	The quantity will always be greater than or equal 
 * 			to zero
 * 			| getQuantity() >= 0
 *
 */
@Value
public class Quantity {
	/**
	 * Creates a immutable quantity object with a given quantity
	 * @param 	quantity
	 * 			The quantity
	 * @pre		The quantity is greater or equal to zero
	 * 			| quantity >= 0
	 */
	public Quantity(long quantity) {
		this.quantity = quantity;
	}
	
	/************************************************************
	 * quantity
	 ***********************************************************/
	final private long quantity;
	
	/**
	 * Returns the quantity of this object
	 */
	@Basic @Immutable
	public long getQuantity(){
		return quantity;
	}
	
	/**
	 * Checks if this quantity object is greater or equal to another quantity
	 * object
	 * @param	otherQuantity
	 * 			The quantity that this one will be compared to.
	 * @return	The result is true if this quantity is greater than the 
	 * 			other one
	 * 			| result == (this.getQuantity() >= otherQuantity.getQuantity())
	 */
	public boolean isGreaterOrEqualTo(Quantity otherQuantity){
		return (this.getQuantity() >= otherQuantity.getQuantity());
	}
	
	/**
	 * Creates a new Quantity object with a quantity equal to the sum of
	 * this quantity and the secondQuantity
	 * @param	secondQuantity
	 * 			The second argument in the sum
	 * @return	The result is a new quantity object where the quantity is equal to
	 * 			is equal to the sum of this quantity and the otherQuantity.
	 * 			|result == (new Quantity(this.getQuantity() + other.getQuantity()));
	 * @pre		The sum of the quantities is smaller then long
	 * 			| MAXQUANTITY - this.getQuantity()) <= secondQuantity.getQuantity()
	 */
	public Quantity add(Quantity secondQuantity) throws ArithmeticException{
		return new Quantity(this.getQuantity() + secondQuantity.getQuantity());
	}
	
	/**
	 * Creates a new Quantity object with a quantity equal to this quantity minus
	 *  the second Quantity
	 * @param	secondQuantity
	 * 			The second argument in the substraction
	 * @return	The result is a new quantity object where the quantity is equal to
	 * 			is equal to the sum of this quantity and the otherQuantity.
	 * 			|result == (new Quantity(this.getQuantity() - other.getQuantity()));
	 * @pre		This quantity is greater or equal to the other quantity
	 * 			| this.isGreaterOrEqualTo(otherQuantity)
	 */
	public Quantity subStract(Quantity secondQuantity){
		return new Quantity(this.getQuantity() - secondQuantity.getQuantity());
	}
	
	/**
	 * Returns a Quantity object that is rounded to the given amount
	 * @param	amount
	 * 			To what amount the result will be rounded
	 * @pre		The amount is greater then zero
	 * 			| amount > 0
	 * @return	The result is a new Quantity object with a quantity equal
	 * 			to this quantity minus this quantity modulo the input amount
	 * 			| result == (new Quantity(this.getQuantity() - this.getQuantity() % amount)
	 */
	public Quantity roundTo(long amount){
		return new Quantity(this.getQuantity() - this.getQuantity() % amount);
	}
	
	/**
	 * Checks if an object is equal to this quantity
	 * @param	obj
	 * 			The object to compare to
	 * @return	The result is true if the other object is not null, and 
	 * 			the class of this object equals the class of the other object
	 * 			and the quantity if this object equals the quantity of the other
	 * 			object.
	 * 			| result == (obj!=null) && (this.getClass() == obj.getClass()) &&
	 * 			|				(this.getQuantity() == obj.getQuantity())
	 */
	@Override
	public boolean equals(Object obj){
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Quantity otherQuantity = (Quantity) obj;
		return (this.getQuantity() == otherQuantity.getQuantity());
	}
	
	/**
	 * Returns the hashCode of this object
	 * @return	The hashCode of the quantity of this object
	 * 			| result == Long.hashCode(getQuantity())
	 */
	@Override
	public int hashCode(){
		return Long.hashCode(getQuantity());
	}

	
	/**********************************************************************
	 * Constants for sizes
	 **********************************************************************/
	public final static long MAXQUANTITY = Long.MAX_VALUE;
	
	public final static long DROPAMOUNT = 8;
	public final static long PINCHAMOUNT = 6;
	
	public final static long SPOONAMOUNT = 24; // LCM of dropamount and pinchamount
	
	public final static long VIALAMOUNT 	= SPOONAMOUNT	*5;
	public final static long BOTTLEAMOUNT 	= VIALAMOUNT	*3;
	public final static long JUGAMOUNT 		= BOTTLEAMOUNT	*7;
	public final static long BARRELAMOUNT 	= JUGAMOUNT		*12;
	
	public final static long SACHELAMOUNT	= SPOONAMOUNT	*7;
	public final static long BOXAMOUNT		= SACHELAMOUNT	*6;
	public final static long SACKAMOUNT		= BOXAMOUNT		*3;
	public final static long CHESTAMOUNT	= SACKAMOUNT	*10;
	
	public final static long STOREROOMAMOUNT= CHESTAMOUNT	*5;
	
	// Start methods that can be used for liquids
	/**
	 * Returns the amount of drops that do not fill a spoon
	 * @return	The amount of drops in this quantity that are leftover
	 * 			after filling as many spoons as possible. If the number is not
	 * 			whole the result is rounded down.
	 * 			| result ==  (this.getQuantity() % SPOONAMOUNT - this.getQuantity() % DROPAMOUNT)/DROPAMOUNT
	 */
	@Immutable
	public long getExactNbOfDrops(){
		return (this.getQuantity() % SPOONAMOUNT - this.getQuantity() % DROPAMOUNT)/DROPAMOUNT ;
	}
	
	/**
	 * Returns the amount of spoons that do not fill a vial
	 * @return	The amount of spoons in this quantity that are leftover
	 * 			after filling as many vials as possible. If the number is not
	 * 			whole the result is rounded down.
	 * 			| result ==  (this.getQuantity() % VIALAMOUNT - this.getQuantity() % SPOONAMOUNT)/SPOONAMOUNT 
	 */
	@Immutable
	public long getExactNbOfSpoonsLiquid(){
		return (this.getQuantity() % VIALAMOUNT - this.getQuantity() % SPOONAMOUNT)/SPOONAMOUNT ;
	}
	
	/**
	 * Returns the amount of vials that do not fill a bottle
	 * @return	The amount of vials in this quantity that are leftover
	 * 			after filling as many bottles as possible. If the number is not
	 * 			whole the result is rounded down.
	 * 			| result ==  (this.getQuantity() % BOTTLEAMOUNT - this.getQuantity() % VIALAMOUNT)/VIALAMOUNT ;
	 */
	@Immutable
	public long getExactNbOfVials(){
		return (this.getQuantity() % BOTTLEAMOUNT - this.getQuantity() % VIALAMOUNT)/VIALAMOUNT ;
	}
	
	/**
	 * Returns the amount of bottles that do not fill a jug
	 * @return	The amount of bottles in this quantity that are leftover
	 * 			after filling as many jugs as possible. If the number is not
	 * 			whole the result is rounded down.
	 * 			| result ==  (this.getQuantity() % JUGAMOUNT - this.getQuantity() % BOTTLEAMOUNT)/BOTTLEAMOUNT 
	 */
	@Immutable
	public long getExactNbOfBottles(){
		return (this.getQuantity() % JUGAMOUNT - this.getQuantity() % BOTTLEAMOUNT)/BOTTLEAMOUNT ;
	}
	
	/**
	 * Returns the amount of jugs that do not fill a barrels
	 * @return	The amount of jugs in this quantity that are leftover
	 * 			after filling as many barrels as possible. If the number is not
	 * 			whole the result is rounded down.
	 * 			| result ==  (this.getQuantity() % BARRELAMOUNT - this.getQuantity() % JUGAMOUNT)/JUGAMOUNT 
	 */
	@Immutable
	public long getExactNbOfJugs(){
		return (this.getQuantity() % BARRELAMOUNT - this.getQuantity() % JUGAMOUNT)/JUGAMOUNT ;
	}
	
	/**
	 * Returns the amount of barrels that do not fill a storeroom
	 * @return	The amount of barrels in this quantity that are leftover
	 * 			after filling as many storerooms as possible. If the number is not
	 * 			whole the result is rounded down.
	 * 			| result ==  (this.getQuantity() % STOREROOMAMOUNT - this.getQuantity() % BARRELAMOUNT)/BARRELAMOUNT 
	 */
	@Immutable
	public long getExactNbOfBarrels(){
		return (this.getQuantity() % STOREROOMAMOUNT - this.getQuantity() % BARRELAMOUNT)/BARRELAMOUNT ;
	}
	
	
	/**
	 * Returns The amount of storerooms
	 * @return	The amount of storerooms in this quantity. If the number is not
	 * 			whole the result is rounded down.
	 * 			| result ==  (this.getQuantity() - this.getQuantity() % STOREROOMAMOUNT)/STOREROOMAMOUNT ;
	 */
	@Immutable
	public long getExactNbOfStorerooms(){
		return (this.getQuantity() - this.getQuantity() % STOREROOMAMOUNT)/STOREROOMAMOUNT ;
	}
	
	// Start methods that can be used for solids
	/**
	 * Returns the amount of pinches that do not fill a spoon
	 * @return	The amount of pinches in this quantity that are leftover
	 * 			after filling as many spoons as possible. If the number is not
	 * 			whole it is rounded down.
	 * 			| result == (this.getQuantity() % SPOONAMOUNT  - this.getQuantity() % PINCHAMOUNT)/PINCHAMOUNT 
	 */
	@Immutable
	public long getExactNbOfPinches(){
		return (this.getQuantity() % SPOONAMOUNT  - this.getQuantity() % PINCHAMOUNT)/PINCHAMOUNT ;
	}
	
	/**
	 * Returns the amount of spoon that do not fill a sachel
	 * @return	The amount of spoon in this quantity that are leftover
	 * 			after filling as many sachels as possible. If the number is not
	 * 			whole it is rounded down.
	 * 			| result == (this.getQuantity() % SACHELAMOUNT  - this.getQuantity() % SPOONAMOUNT)/SPOONAMOUNT 
	 */
	@Immutable
	public long getExactNbOfSpoonsSolid(){
		return (this.getQuantity() % SACHELAMOUNT  - this.getQuantity() % SPOONAMOUNT)/SPOONAMOUNT ;
	}
	
	/**
	 * Returns the amount of sachels that do not fill a box
	 * @return	The amount of sachels in this quantity that are leftover
	 * 			after filling as many boxes as possible. If the number is not
	 * 			whole it is rounded down.
	 * 			| result == this.getQuantity() % BOXAMOUNT  - this.getQuantity() % SACHELAMOUNT)/SACHELAMOUNT ;
	 */
	@Immutable
	public long getExactNbOfSachels(){
		return (this.getQuantity() % BOXAMOUNT  - this.getQuantity() % SACHELAMOUNT)/SACHELAMOUNT ;
	}
	
	/**
	 * Returns the amount of boxes that do not fill a sack
	 * @return	The amount of boxes in this quantity that are leftover
	 * 			after filling as many sacks as possible. If the number is not
	 * 			whole it is rounded down.
	 * 			| result == (this.getQuantity() % SACKAMOUNT  - this.getQuantity() % BOXAMOUNT)/BOXAMOUNT ;
	 */
	@Immutable
	public long getExactNbOfBoxes(){
		return (this.getQuantity() % SACKAMOUNT  - this.getQuantity() % BOXAMOUNT)/BOXAMOUNT ;
	}
	
	/**
	 * Returns the amount of sacks that do not fill a chest
	 * @return	The amount of sacks in this quantity that are leftover
	 * 			after filling as many chests as possible. If the number is not
	 * 			whole it is rounded down.
	 * 			| result == (this.getQuantity() % CHESTAMOUNT  - this.getQuantity() % SACKAMOUNT)/SACKAMOUNT 
	 */
	@Immutable
	public long getExactNbOfSacks(){
		return (this.getQuantity() % CHESTAMOUNT  - this.getQuantity() % SACKAMOUNT)/SACKAMOUNT ;
	}
	
	/**
	 * Returns the amount of chests that do not fill a storeroom
	 * @return	The amount of chests in this quantity that are leftover
	 * 			after filling as many storerooms as possible. If the number is not
	 * 			whole it is rounded down.
	 * 			| result == (this.getQuantity() % STOREROOMAMOUNT  - this.getQuantity() % CHESTAMOUNT)/CHESTAMOUNT
	 */
	@Immutable
	public long getExactNbOfChests(){
		return (this.getQuantity() % STOREROOMAMOUNT  - this.getQuantity() % CHESTAMOUNT)/CHESTAMOUNT ;
	}
}
