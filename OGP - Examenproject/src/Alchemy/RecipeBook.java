package Alchemy;
import java.util.ArrayList;
import be.kuleuven.cs.som.annotate.*;
/**
 * A class that collects recipes in one collection
 * 
 * @author 	Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 * 
 * @invar	Every recipe in this book must have its 
 * 			recipeBook set to this
 * 			| for each recipe in this
 * 			|	recipe.getRecipeBook() == this
 * 
 */
public class RecipeBook {
	/**
	 * Creates a new Recipebook that contains no recipes.
	 * 
	 * @post	The recipe book contains no recipes
	 * 			| getNbOfPages() == null
	 */
	public RecipeBook(){
		
	}
	
	
	/************************************************
	 * recipeList
	 ************************************************/
	/**
	 * Variable to keep track of the recipes added to the book
	 * When a recipe is removed the reference is set to null.
	 */
	private ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
	
	/**
	 * Returns the number of pages in the book
	 */
	public int getNbOfPages(){
		return recipeList.size();
	}
	
	/**
	 * Returns the recipe on the index page
	 * @param	page
	 * 			the page of the recipe
	 * @throws	IndexOutOfBoundsException
	 * 			The page is not in bounds
	 * 			| page <=0 || page > getNbOfPages()
	 */
	@Basic	
	public Recipe getRecipeAt(int page)
		throws IndexOutOfBoundsException{
		if (page <=0 || page > getNbOfPages())
			throw new IndexOutOfBoundsException();
		return recipeList.get(page-1);
	}
	
	/**
	 * Sets the reference at the given page to null 
	 * @param	page
	 * 			The page of the recipe that will be removed 
	 * 			from this book
	 * @post	The page is set to null
	 * 			| getRecipeAt(page) == null
	 * @throws	IndexOutOfBoundsException
	 * 			The page is not in range
	 * 			| page<=0 || page > getNbOfPages()
	 * @throws	IllegalArgumentException
	 * 			The recipe at the given index still has
	 * 			this book as its book
	 * 			| getRecipeAt(page).getRecipeBook()  == this
	 */
	public void removeRecipeAt(int page) throws IllegalArgumentException, IndexOutOfBoundsException{
		if (getRecipeAt(page).getRecipeBook()== this)
			throw new IllegalArgumentException();
		if (page<=0 || page > getNbOfPages())
			throw new IndexOutOfBoundsException();
		recipeList.set(page-1, null);
	}
	
	/**
	 * Finds the page of a recipe in a book, if the 
	 * recipe is not in this book then -1 is returned
	 * 
	 * @return	The page of this book where this recipe is written
	 * 			if the recipe is not in the book the result is -1.
	 * 			| for (int i=1;i<=getNbOfPages(); i++){
	 *			|	if (getRecipeAt(i) == recipe)
	 *			|		result ==  i;
	 *			|	}
	 *			| result ==  -1
	 */
	public int getPageOf(Recipe recipe){
		for (int i=1;i<=getNbOfPages(); i++){
			if (getRecipeAt(i) == recipe)
				return i;
		}
		return -1;
	}
	
	/**
	 * Removes a recipe from this book
	 * @param	recipe
	 * 			The recipe to be removed
	 * 
	 * @effect	The page of the recipe is removed
	 * 			removeRecipeAt(getPageOf(recipe))
	 * 	
	 * @throws	IllegalArgumentException
	 * 			The recipe still has this book as its recipeBook
	 * 			| recipe.getRecipeBook() == this
	 * @throws	IllegalArgumentException
	 * 			The recipe is not in this book
	 * 			| getPageOf(recipe) == -1
	 */
	public void removeRecipe(Recipe recipe) throws IllegalArgumentException{
		if (recipe.getRecipeBook()== this)
			throw new IllegalArgumentException();
		if (getPageOf(recipe) == -1)
			throw new IllegalArgumentException();
		removeRecipeAt(getPageOf(recipe));
	}
	
	/**
	 * Adds a recipe to the book
	 * @param	recipe
	 * 			the recipe to be added
	 * @post	The recipe on the last page is equal
	 * 			to recipe
	 * 			| getRecipeAt(new.getNbOfPages()) == recipe
	 * @post	The number of pages is increased
	 * 			| new.getNbOfPages = getNbOfPages + 1
	 * @throws	IllegalArgumentException
	 * 			The given recipe does not have its recipebook set to this
	 * 			| recipe.getRecipeBook() != this
	 */
	public void addRecipe(Recipe recipe) throws IllegalArgumentException{
		if (recipe.getRecipeBook() != this){
			throw new IllegalArgumentException();
		}
		recipeList.add(recipe);
	}
	
	/**
	 * Checks if a recipe is in this book
	 * @param	recipe
	 * 			the recipe to check
	 * return	true if the result of getpageof is a valid page.
	 * 			result == (getPageOf(recipe) != -1)
	 */
	public boolean hasAsRecipe(Recipe recipe){
		return (getPageOf(recipe) != -1);
	}
	
}
