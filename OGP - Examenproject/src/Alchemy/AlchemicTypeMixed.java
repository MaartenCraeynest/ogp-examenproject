package Alchemy;
import be.kuleuven.cs.som.annotate.*;
import java.util.*;
/**
 * A class that represents types that originate from mixing two different 
 * alchemictypes. A special name can be given to the ingredient 
 * @invar	The special name should be a valid name
 * 			| canHaveAsSpecialName(getSpecialName())
 * @author 	Maarten Craeynest, Antony De Jonckheere
 * @version	1.0
 */
public class AlchemicTypeMixed extends AlchemicType {
	/**
	 * Initializes a mixed alchemic type with a given simple name, standard state,
	 * standard temperature and theoretic volatility
	 * @param 	standardstate
	 * 			The state to set the standard state of this mixed 
	 * 			alchemic type to
	 * @param 	standardtemp
	 * 			The temperature to set the standard temperature of this
	 * 			mixed alchemic type to
	 * @param 	theoreticVolatility
	 * 			The volatility to set the theoretic volatility of this 
	 * 			mixed alchemic type to
	 * @param 	name
	 * 			The name to set the simple name of this mixed 
	 * 			alchemic type to
	 * @effect	The new mixed type is set as an alchemic type with given name,
	 * 			standardstate,standardtemp and theoreticVolatility
	 * 			|super(name,standardstate,standardtemp,theoreticVolatility)
	 */
	public AlchemicTypeMixed(String name,States standardstate,Temperature standardtemp,double theoreticVolatility)
			throws IllegalArgumentException{
		super(name,standardstate,standardtemp,theoreticVolatility);
	}
	
	/**
	 * Initializes a mixed alchemic type with a given special name, standard state,
	 * standard temperature and theoretic volatility
	 * @param	simplename
	 * 			The simple name to give to this mixed alchemic type
	 * @param 	standardstate
	 * 			The state to set the standard state of this mixed 
	 * 			alchemic type to
	 * @param 	standardtemp
	 * 			The temperature to set the standard temperature of this
	 * 			mixed alchemic type to
	 * @param 	theoreticVolatility
	 * 			The volatility to set the theoretic volatility of this 
	 * 			mixed alchemic type to
	 * @param 	specialname
	 * 			The name to set the special name of this mixed 
	 * 			alchemic type to
	 * @effect	The new mixed type is set as an alchemic type with given
	 * 			simplename,standardstate,standardtemp and theoreticVolatility
	 * 			|this(simplename,standardstate,standardtemp,theoreticVolatility)
	 * @effect	The new mixed type has the given special name
	 * 			|setSpecialName(specialname) 
	 */
	public AlchemicTypeMixed(String simplename,States standardstate,Temperature standardtemp,double theoreticVolatility,String specialname)
			throws IllegalArgumentException{
		this(simplename,standardstate,standardtemp,theoreticVolatility);
		setSpecialName(specialname);
	}
	/*******************************************************************
	 * specialName
	 *******************************************************************/
	/**
	 * Variable registering the special name of this mixed alchemic type
	 */
	private String specialName="";
	
	/**
	 * Returns the special name of this mixed type if it is defined
	 * otherwise returns an empty String
	 */
	@Basic
	public String getSpecialName() {
		return specialName;
	}
	/**
	 * Sets the special name of this mixed type to the given name
	 * @param 	specialName 
	 * 			the special name to set
	 * @post	The special name of this mixed type is the given name
	 * 			| new.getSimpleName()== specialName
	 * @throws	IllegalArgumentException
	 * 			if the given special name is not a valid name for an unmixed type
	 * 			| !AlchemicTypeUnmixed.canHaveAsSpecialName(specialName)
	 */
	@Raw
	public void setSpecialName(String specialName) throws IllegalArgumentException{
		if(!canHaveAsSpecialName(specialName)){
			throw new IllegalArgumentException();
		}
		this.specialName = specialName;
	}
	
	/**
	 * Checks if this object has a special name
	 * @return	True if the special name of this object is not an
	 * 			empty string
	 * 			| result == (getSpecialName() != "")
	 */
	public boolean hasSpecialName(){
		return (getSpecialName() != "");
	}
	
	/**
	 * Checks if this a specialname is valid
	 * @param	name
	 * 			The name to check
	 * @return	True if the name is not null, and the name equals an empty
	 * 			string or is a valid simplename for the superklass
	 * 			| result == (name != null) && (name == "" || super.canHaveAsSimpleName(name)) 
	 */
	@Raw
	public boolean canHaveAsSpecialName(String name){
		return (name != null) && (name == "" || super.canHaveAsSimpleName(name)) ;
	}
	
	/**
	 * Method to determine if the given name is a valid simple name
	 * for an alchemic type
	 * @param 	name
	 * 			The simple name to give to this alchemic type
	 * @return	True if the name is not null and every name 
	 * 			between ", ", "mixed with" and "and"
	 * 			is a valid name for an unmixed alchemic type and "mixed with"
	 * 			and "and" only appear once each. "mixed with" after the 
	 * 			first unmixed type name and "and" only before the last 
	 * 			unmixed type name. if the different unmixed type names are 
	 * 			in alphabetical order and no unmixed type name is repeated 
	 * 			| if (name == null)
	 * 			| then result == false
	 * 			| if(name.matches(".* mixed with .* mixed with .*")||name.matches(".* and .* and .*")||name.matches(".*, .* mixed with .*")||name.matches(".* and .*, .*"))
	 *			| then result == false;
	 *			| if(name.matches(".*, .*")&&!name.contains(" and "))
	 *			|	result == false;
	 *			| name=name.replaceAll(" mixed with ", ", ");
	 *			| name=name.replaceAll(" and ",", ");
	 *			| String[] individualNames=name.split(", ");
	 *			| String[] alphanames=name.split(", ");
	 *			| Arrays.sort(alphanames);
	 *			| for i in 0..individualNames.length
	 *			|	 if(!individualNames[i].equals(alphanames[i]))
	 *			| 		then result == false
	 *			| foreach(String a in individualNames){
	 *			|	if(!(AlchemicTypeUnmixed.canHaveAsSimpleName(a)))
	 *			|	then result == false;
	 */
	@Override
	public boolean canHaveAsSimpleName(String name){
		if (name == null)
			return false;
		if(name.matches(".* mixed with .* mixed with .*")||name.matches(".* and .* and .*")){
			return false;
		}
		if(name.matches(".*(, | and ).* mixed with .*")||name.matches(".* and .*(, | mixed with ).*")){
			return false;
		}
		if(name.matches(".*, .*")&&!name.contains(" and ")){
			return false;
		}
		name=name.replaceFirst(" mixed with ", ", ");
		name=name.replaceFirst(" and ",", ");
		if(name.matches("(.+, )*(.*), (.*, )*\\2.*")){	//checks whether the mixed name contains any unmixed name more than once
			return false;
		}
		String[] individualNames=name.split(", ");	
		String[] alphanames=name.split(", ");
		Arrays.sort(alphanames);
		for(int i=0;i<individualNames.length;i++){
			if(!individualNames[i].equals(alphanames[i])){
				 return false;
			}
		}
		for(String a:individualNames){
			if(!(super.canHaveAsSimpleName(a))){
				return false;
			}
		}
		return true;
	}

	
	/**
	 * Represents this object as its simple name followed by a comma and the
	 * special name of this ingredient if it has one.
	 * @return	This object's simple name followed by a comma and the special name
	 * 			of this object if it has one.
	 * 			| if (hasSpecialName())
	 * 			| then	result ==  getSimpleName() + ", " + getSpecialName()
	 * 			| else	result == getSimpleName()
	 */
	@Override
	public String toString(){
		 if (hasSpecialName())
			return getSimpleName() + ", " + getSpecialName();
		 else	
			 return getSimpleName();
	}

	/***********************************************************************
	 * Methods for finding equivalent variables after mixing two ingredients
	 ***********************************************************************/
	
	/**
	 * Returns the equivalent name of the mixed type of the given list of ingredients
	 * @param 	ingredients
	 * 			The list of alchemic ingredients to mix
	 * @return	The simple names of the unmixed types of the ingredients separated by
	 * 			comma's. if any of the ingredients is of a mixed type, the special
	 * 			name is used if one exists, if it doesn't, the simple names of the 
	 * 			different unmixed types in the mixed name are used. All the different 
	 * 			names are alphabetically ordered. the first two names are separated by 
	 * 			" mixed with " instead of comma's and if there are more than three
	 * 			different names, the last two are separated by " and " instead of
	 * 			comma's. If any name appears more than once, only one will be used
	 *			in the new mixed name.
	 *			|result=""
	 * 			|for each AlchemicIngredient a in ingredients
	 *			|	result=result+", "+a.toString()
	 *			|result=result.replaceAll("\\b( mixed with| and)\\b",",")
	 *			|String[] names=simplename.split(", ")
	 *			|Arrays.sort(names)
	 *			|result =""
	 *			|for each(String s in names)
	 *			|	result=result+", "+s
	 *			|result=result.replaceAll(", (\\w*)(, \\1)*", ", $1");
	 *			|result=result.replaceFirst(", ", "")
	 *			|result=result.replaceFirst("(.*?), ", "$1 mixed with ")
	 *			|result=result.replaceFirst("\\A(, ){1}", "")
	 *			|if(result.matches(".*, .*"))
	 *			|	then
	 *			|	result=result.replaceFirst("(?s)(.*), ", "$1 and ")
	 *				
	 */
	public static String getEquivalentMixedName(AlchemicIngredient[] ingredients){
		String simplename="";
		for(AlchemicIngredient a:ingredients){
			simplename=simplename+", "+a.toString();
		}
		simplename=simplename.replaceAll("\\b( mixed with| and)\\b",",");
		String[] names=simplename.split(", ");
		Arrays.sort(names);
		simplename="";
		for(String s: names){
			simplename=simplename+", "+s;
		}
		simplename=simplename.replaceFirst(", ", "");
		simplename=simplename.replaceAll(", (\\w*)(, \\1)+", ", $1"); //replaces recurring names
		simplename=simplename.replaceFirst(", (.*?), ", "$1 mixed with ");
		simplename=simplename.replaceFirst("\\A(, ){1}", "");
		if(simplename.matches(".*, .*")){
			simplename=simplename.replaceFirst(" (.*), ", " $1 and ");
		}
		return simplename;		
	}

	/**
	 * Returns the standard state of the mixed alchemic ingredients
	 * @param 	ingredients
	 * 			List of alchemic ingredients to mix
	 * @return	The state of the ingredient with the standard temperature
	 * 			closest to [0,20] if different ingredients are equally close
	 * 			preference is given to liquid
	 * 			|temp	=new Temperature(0,20)
	 * 			|sttemp	=new Temperature(Temperature.getMaximumTemperature(),0)
	 * 			|for each AlchemicIngredient a in ingredients
	 * 			|	if(Math.abs(sttemp.compare(temp))<Math.abs(temp.compare(a.getType().getStandardTemperature())))
	 * 			|	then	result == a.getType.getStandardState()
	 * 			|	else if(Math.abs(sttemp.compare(temp))==Math.abs(temp.compare(a.getType().getStandardTemperature())&&a.getType.getStandardState().equals(States.LIQUID))
	 * 			|		then result == States.LIQUID
	 */
	public static States getEquivalentMixedStandardState(AlchemicIngredient[] ingredients){
		States state=States.POWDER;
		Temperature temp=new Temperature(0,20);
		Temperature sttemp=new Temperature(Temperature.getMaximumTemperature(),0);
		for(AlchemicIngredient a:ingredients){
			if(Math.abs(sttemp.compare(temp))>=Math.abs(temp.compare(a.getType().getStandardTemperature()))){
				if(Math.abs(sttemp.compare(temp))==Math.abs(temp.compare(a.getType().getStandardTemperature()))){
					if(a.getType().getStandardState().equals(States.LIQUID)){
						state=States.LIQUID;
					}
				}
				else {
					state=a.getType().getStandardState();
				}
				sttemp=a.getType().getStandardTemperature();
			}
		}
		return state;
	}
	/**
	 * Returns the equivalent standard temperature of the mixed ingredients
	 * @param 	ingredients
	 * 			The ingredients to mix
	 * @return	The standard temperature of the ingredients closest to [0,20]
	 * 			if both are equally close, the hottest temperature is chosen
	 * 			|temp=new Temperature(0,20)
	 * 			|sttemp=new Temperature(Temperature.getMaximumTemperature(),0)
	 * 			|for each AlchemicIngredient a in ingredients
	 * 			|	if(Math.abs(sttemp.compare(temp))<Math.abs(temp.compare(a.getType().getStandardTemperature())))
	 * 			|		then
	 * 			|		result=a.getType.getStandardTemperature()
	 * 			|	else if(Math.abs(sttemp.compare(temp))==Math.abs(temp.compare(a.getType().getStandardTemperature()))
	 * 			|		then
	 * 			|			if(a.getType().getStandardTemperature().getHotness()>sttemp.getHotness())
	 *			|				then
	 *			|				sttemp=a.getType().getStandardTemperature()
	 */
	public static Temperature getEquivalentMixedStandardTemperature(AlchemicIngredient[] ingredients){
		Temperature temp=new Temperature(0,20);
		Temperature sttemp=new Temperature(Temperature.getMaximumTemperature(),0);
		for(AlchemicIngredient a:ingredients){
			if(Math.abs(temp.compare(sttemp))>=Math.abs(temp.compare(a.getType().getStandardTemperature()))){
				if(Math.abs(temp.compare(sttemp))==Math.abs(temp.compare(a.getType().getStandardTemperature()))){
					if(a.getType().getStandardTemperature().getHotness()>=sttemp.getHotness()){
						sttemp=a.getType().getStandardTemperature();
					}
				}
				else{
					sttemp=a.getType().getStandardTemperature();
				}
			}
		}
		return sttemp;
	}
	

	/**
	 * Returns the equivalent theoretic volatility of the mixed type
	 * @param 	ingredients
	 * 			The ingredients to mix
	 * @return	The average theoretic volatility weighted by the quantity
	 * 			|totalquantity=0;
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	totalquantity=totalquantity+a.getQuantity().getQuantity()
	 * 			|	result=result+a.getType().getTheoreticVolatility()*a.getQuantity().getQuantity();
	 * 			|result=result/totalquantity
	 * @throws	ArithmeticException
	 * 			if the total quantity overflows a long
	 * 			|totalquantity=0;
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	if(totalquantity>Long.MAX_VALUE - a.getQuantity().getQuantity())
	 * 			|		then throw new ArithmeticException()
	 * 			|	else
	 * 			|		totalquantity=totalquantity+a.getQuantity().getQuantity()>Long.MAX_VALUE
	 */
	public static double getEquivalentMixedTheoreticVolatility(AlchemicIngredient[] ingredients)throws ArithmeticException{
		long totalquantity=0;
		double theoreticvolatility=0;
		for(AlchemicIngredient a:ingredients){
			try{
				totalquantity=Math.addExact(totalquantity,a.getQuantity().getQuantity());
				theoreticvolatility=theoreticvolatility+a.getType().getTheoreticVolatility()*a.getQuantity().getQuantity();
			}
			catch(ArithmeticException arith){
				throw arith;
			}
		}
		return theoreticvolatility/totalquantity;
	}

	/**
	 * Returns the equivalent state of the mixed ingredients
	 * @param 	ingredients
	 * 			The list of ingredients to mix
	 * @return	The state of the ingredient with the standard temperature
	 * 			closest to [0,20], if different ingredients are equally close
	 * 			preference is given to liquid
	 * 			|temp=new Temperature(0,20)
	 * 			|sttemp=new Temperature(Temperature.getMaximumTemperature(),0)
	 * 			|for each AlchemicIngredient a in ingredients
	 * 			|	if(Math.abs(sttemp.compare(temp))<Math.abs(temp.compare(a.getType().getStandardTemperature())))
	 * 			|		then result == a.getType.getStandardState()
	 * 			|	else if(Math.abs(sttemp.compare(temp))==Math.abs(temp.compare(a.getType().getStandardTemperature())&&a.getType.getStandardState().equals(States.LIQUID))
	 * 			|		then result==States.LIQUID
	 * 
	 */
	public static States getEquivalentMixedState(AlchemicIngredient[] ingredients){
		States state=States.POWDER;
		Temperature temp=new Temperature(0,20);
		Temperature sttemp=new Temperature(Temperature.getMaximumTemperature(),0);
		for(AlchemicIngredient a:ingredients){
			if(Math.abs(sttemp.compare(temp))>=Math.abs(temp.compare(a.getType().getStandardTemperature()))){
				if(Math.abs(sttemp.compare(temp))==Math.abs(temp.compare(a.getType().getStandardTemperature()))){
					if(a.getState().equals(States.LIQUID)){
						state=States.LIQUID;
					}
				}
				else {
					state=a.getState();
				}
				sttemp=a.getType().getStandardTemperature();
			}
		}
		return state;
	}
	
	/**
	 * Returns the equivalent temperature of the mixed ingredients
	 * @param 	ingredients
	 * 			The ingredients to mix
	 * @return	The average temperature weighted by the quantity.
	 * 			|totalquantity=0;
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	totalquantity=totalquantity+a.getQuantity().getQuantity()
	 * 			|	result=result+a.getType().getTemperature()*a.getQuantity().getQuantity()
	 * 			|result=result/totalquantity
	 * 			|return new Temperature(-Math.min(result, 0),Math.max(result, 0))
	 * @throws	ArithmeticException
	 * 			If the total quantity overflows a long
	 * 			|totalquantity=0;
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	if(totalquantity > Long.MAX_VALUE - a.getQuantity().getQuantity())
	 * 			|		then throw new ArithmeticException()
	 * 			|	else
	 * 			|		totalquantity = totalquantity+a.getQuantity().getQuantity()
	 */
	public static Temperature getEquivalentMixedTemperature(AlchemicIngredient[] ingredients)throws ArithmeticException{
		long totalquantity=0;
		long temperature=0;
		for(AlchemicIngredient a:ingredients){
			try{
				totalquantity=Math.addExact(totalquantity,a.getQuantity().getQuantity());
				temperature=temperature+(a.getTemperature().getHotness()-a.getTemperature().getColdness())*a.getQuantity().getQuantity();
			}
			catch(ArithmeticException arith){
				throw arith;
			}
		}
		temperature= Math.round(temperature/totalquantity);
		return new Temperature(Math.abs(Math.min(temperature, 0)),Math.max(temperature, 0));
	}
	
	/**
	 * Returns the total mixed quantity
	 * @param 	ingredients
	 * 			the ingredients to mix
	 * @return  The sum of the quantities of each ingredient, if 
	 * 			an ingredient has pinches or drops and the state 
	 * 			of that ingredient is not equal to the equivalent
	 * 			mixed state of the given ingredients, the quantity
	 * 			of that ingredient is rounded to the nearest spoon
	 * 			amount
	 * 			|result=0
	 * 			|differentstatequantity=0
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	if(a.getState().equals(getEquivalentMixedState(ingredients)))
	 *			|		then
	 *	 		|		result=result+a.getQuantity().getQuantity()
	 *			|	else
	 *			|		differentstatequantity=differentstatequantity+a.getQuantity().getQuantity()
	 *			|return result+differentstatequantity-differentstatequantity%Quantity.SPOONAMOUNT
	 * @throws	ArithmeticException
	 * 			if the total quantity or any of the intermerdiate results overflows a long
	 * 			|totalquantity=0
	 * 			|differentstatequantity=0
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	if(a.getState().equals(getEquivalentMixedState(ingredients)))
	 *			|		then
	 *			|			if(totalquantity > Long.MAXVALUE - a.getQuantity().getQuantity())
	 *			|				then throw new ArithmeticException
	 *	 		|			totalquantity=totalquantity+a.getQuantity().getQuantity()
	 *			|	else
	 *			|			if(totalquantity - (a.getQuantity().getQuantity() % Quantity.SPOONAMOUNT) > Long.MAXVALUE - a.getQuantity().getQuantity())
	 *			|				then throw new ArithmeticException
	 *	 		|			totalquantity = totalquantity  + a.getQuantity().getQuantity()  - (a.getQuantity().getQuantity() % Quantity.SPOONAMOUNT)
	 *			|		
	 */
	public static Quantity getEquivalentMixedQuantity(AlchemicIngredient[] ingredients)throws ArithmeticException{
		long totalquantity=0;
		States state=getEquivalentMixedState(ingredients);
		long differentstatequantity=0;
		try{
			for(AlchemicIngredient a:ingredients){
				if(a.getState().equals(state)){
					totalquantity=Math.addExact(totalquantity,a.getQuantity().getQuantity());
				}
				else{
					differentstatequantity=Math.addExact(differentstatequantity,a.getQuantity().getQuantity());
				}
			}
			differentstatequantity=differentstatequantity-differentstatequantity%Quantity.SPOONAMOUNT;
			totalquantity=Math.addExact(totalquantity,differentstatequantity);
		}
		catch(ArithmeticException arith){
			throw arith;
		}
		return new Quantity(totalquantity);			
	}
	/**
	 * Returns the total mixed volatility deviation
	 * @param 	ingredients
	 * 			The ingredients to mix
	 * @return	The average effective volatility deviation weighted by the quantity
	 * 			|totalquantity=0;
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	totalquantity=totalquantity+a.getQuantity().getQuantity()
	 * 			|	result=result+a.getEffectiveVolatilityDeviation()*a.getQuantity().getQuantity();
	 * 			|result=result/totalquantity
	 * @throws	ArithmeticException
	 * 			if the total quantity overflows a long
	 * 			|totalquantity=0;
	 * 			|for each alchemicIngredient a in ingredients
	 * 			|	if(totalquantity >Long.MAX_VALUE - a.getQuantity().getQuantity())
	 * 			|		then
	 * 			|		throw new ArithmeticException()
	 * 			|	else
	 * 			|		totalquantity=totalquantity+a.getQuantity().getQuantity()
	 */
	public static double getEquivalentMixedVolatilityDeviation(AlchemicIngredient[] ingredients)throws ArithmeticException{
		double deviation=0;
		long totalquantity=0;
		for(AlchemicIngredient a:ingredients){
			try{
				totalquantity=Math.addExact(totalquantity,a.getQuantity().getQuantity());
				deviation=deviation+a.getEffectiveVolatilityDeviation()*a.getQuantity().getQuantity();
			}
			catch(ArithmeticException arith){
				throw arith;
			}
		}
		return deviation/totalquantity;
	}
	/**
	 * Returns the equivalent mixed type of the given ingredients
	 * @param 	ingredient
	 * 			The ingredients to mix
	 * @return	An AlchemicTypeMixed with the equivalent mixed name,
	 * 			standard state, standard temperature and theoretic
	 * 			volatility
	 * 			|result ==  new AlchemicTypeMixed(getEquivalentMixedName(ingredient),getEquivalentMixedStandardState(ingredient),
	 *			|				getEquivalentMixedStandardTemperature(ingredient),getEquivalentMixedTheoreticVolatility(ingredient))
	 */
	public static AlchemicTypeMixed getEquivalentMixedType(AlchemicIngredient[] ingredient)
			throws ArithmeticException{
		return new AlchemicTypeMixed(getEquivalentMixedName(ingredient),getEquivalentMixedStandardState(ingredient),
								getEquivalentMixedStandardTemperature(ingredient),getEquivalentMixedTheoreticVolatility(ingredient));
	}
	
	/**
	 * Returns the equivalent mixed type of the given ingredients with 
	 * the given special name
	 * @param 	ingredients
	 * 			The ingredients to mix
	 * @param 	specialname
	 * 			The special name to give to the new alchemic type mixed
	 * @return	An AlchemicTypeMixed with the equivalent mixed name,
	 * 			standard state, standard temperature, theoretic
	 * 			volatility and given special name
	 * 			|result== new getEquivalentMixedType(ingredients).setSpecialName(specialName)
	 */
	public static AlchemicTypeMixed getEquivalentMixedTypeWithSpecialName(AlchemicIngredient[] ingredients,String specialname) 
			throws IllegalArgumentException{
		AlchemicTypeMixed alch=getEquivalentMixedType(ingredients);
		alch.setSpecialName(specialname);
		return alch;
	}
	
}
