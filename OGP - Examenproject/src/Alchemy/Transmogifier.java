package Alchemy;

/**
 * A device that can change state of an ingredient
 * 
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 */
public class Transmogifier extends Device {
	/**
	 * Create a new transmogifier
	 * @effect	The transmogifier is a device with one
	 * 			input and one output
	 * 			| super(1,1)
	 */
	public Transmogifier() {
		super(1,1);
		
	}
	
	/*********************************************************
	 * Overrides
	 **********************************************************/
	/**
	 * Changes the state of the inputingredient to the opposite state
	 * @effect	If the transmogifier can be processed then
	 * 			every output slot is emptied and each ingredient in 
	 * 			an input slot has it state changed from liquid to powder
	 * 			or from powder to liquid and is moved to the output.
	 * 			| if (canBeProcessed())
	 * 			| then
	 * 			| 	for each I in 1..getMAXOUTPUT():
	 * 			|		if getOutputAt(i).hasIngredient()
	 * 			|			getOutputAt(i).getIngredient().terminate()
	 * 			| 	for each I in 1..getMAXINPUT():
	 * 			|		getOutputAt(i).empty()
	 * 			|		if getOutputAt(i).hasIngredient()
	 * 			|		then 	getInputAt(i).getIngredient().changeState()
	 *			|				getInputAt(i).getIngredient().move(getOutputAt(i))
	 */
	@Override
	public void process() {
		if (!canBeProcessed())
			return;
		for (int i = 1; i <= getMAXOUTPUT(); i++){
			if (getOutputAt(i).hasIngredient())
				getOutputAt(i).getIngredient().terminate();
		}
		for (int i = 1; i <= getMAXINPUT(); i++){
			// Empty the previous output(s)
			getOutputAt(i).empty();
			if (getInputAt(i).hasIngredient()){
				// Changes the state
				getInputAt(i).getIngredient().changeState();
				// moves it to the output
				getInputAt(i).getIngredient().move(getOutputAt(i));
			}
		}
	}

	/**
	 * Adds this transmogifier to a laboratory
	 * @param	lab
	 * 			The laboratory where this transmogifier will be added.
	 * @effect	The transmogifier's laboratory is set to the given lab and
	 * 			the lab's kettle is set to this.
	 * 			|	setLaboratory(lab)
	 * 			|	lab.setTransmogifier(this)
	 * @throws	IllegalArgumentException
	 * 			The given lab is null
	 * 			| lab == null
	 * @throws	IllegalStateException
	 * 			The laboratory already has an transmogifier
	 * 			| lab.hasTransmogifier()
	 * @throws  IllegalStateException
	 * 			This transmogifier is already in a laboratory
	 * 			| hasLaboratory()
	 */
	@Override
	public void addTo(Laboratory lab) throws IllegalStateException, IllegalArgumentException {
		if (lab == null)
			throw new IllegalArgumentException("The new lab can not be null");
		if (lab.hasTransmogifier())
			throw new IllegalStateException("The given lab already has a transmogifier");
		if (hasLaboratory())
			throw new IllegalStateException("This transmogifier is already in a laboratory");
		setLaboratory(lab);
		lab.setTransmogifier(this);
	}

	/**
	 * Removes this transmogifier from its lab
	 * @effect	The transmogifier's lab is set to null and
	 * 			that lab's transmogifier is set to null
	 * 			| Laboratory temp = this.getLaboratory()
	 * 			| this.setLaboratory(null)
	 * 			| temp.setTransmogifier(null)
	 * @throws	IllegalStateException
	 * 			The transmogifier isn't in a laboratory
	 * 			| !hasLaboratory()
	 */
	@Override
	public void removeFromLab() throws IllegalStateException {
		if (!hasLaboratory())
			throw new IllegalStateException("This transmogifier is not in a lab.");
		Laboratory temp = this.getLaboratory();
		this.setLaboratory(null);
		temp.setTransmogifier(null);
	}
	
	/**
	 * Adds an ingredient to this device
	 * @param	container
	 * 			The a container with the ingredient to
	 * 			be added.
	 * @effect  Removes the ingredient from its container and adds it to the
	 * 			first input container, if there already is a ingredient there
	 * 			then the ingredient is terminated, the container the ingredient
	 * 			came in is terminated.
	 * 			|if (getInputAt(1).hasIngredient())
	 * 			|	getInputAt(1).getIngredient().terminate()
	 * 			|container.getIngredient().move(getInputAt(1))
	 * 			|container.terminate()
	 * @throws	IllegalArgumentException
	 * 			The container has no ingredient
	 * 			| !container.hasIngredient()
	 */
	@Override
	public void addIngredient(TransportContainer container)
			throws IllegalArgumentException {
		if (!container.hasIngredient()){
			throw new IllegalArgumentException("Cannot add an empty container to a device.");
		}
		if (getInputAt(1).hasIngredient()){
			getInputAt(1).getIngredient().terminate();
		}
		container.getIngredient().move(getInputAt(1));
		container.terminate();
	}

}
