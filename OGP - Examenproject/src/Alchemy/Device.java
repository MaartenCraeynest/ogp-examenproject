package Alchemy;

import be.kuleuven.cs.som.annotate.*;

/**
 * This class represents an abstract device that can take
 * one or more input ingredients and process them to another
 * ingredient.
 * 
 * @author 	Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 * 
 * @invar	The device's amount of input or outputcontainers is greater
 * 			or equal to zero.
 * 			| isValidAmountOfInAndOutPuts(getMAXINPUT(),getMAXOUTPUT())
 */
public abstract class Device {
	/**
	 * Creates an alchemical device with a given amount of input
	 * and output slots
	 * @param	MAXINPUT	
	 * 			The number of inputcontainers of this device
	 * @param	MAXOUTPUT
	 * 			The number of outputcontainers of this device
	 * @post	The number of inputdevices is equal to the given MAXOUTPUT
	 * 			| new.getMAXINPUT() == MAXINPUT
	 * @post	The number of outputdevices is equal to the given MAXOUTPUT	
	 * 			| new.getMAXOUTPUT()  == MAXINPUT
	 * @post	The device input is completely empty
	 * 			| new.getNbOfInputs() == 0
	 * @post	The device output is completely empty
	 * 			| new.getNbOfOutputs() == 0
	 * @throws	IllegalArgumentException
	 * 			The number of inputs/outputs is not valid
	 * 			| !isValidAmountOfInAndOutPuts(getMAXINPUT(),getMAXOUTPUT())
	 * @throws	IllegalArgumentException [can]
	 * 			| ? true
	 */
	@Model
	protected Device(int MAXINPUT, int MAXOUTPUT) throws IllegalArgumentException{
		if (!isValidAmountOfInAndOutPuts(MAXINPUT,MAXOUTPUT))
			throw new IllegalArgumentException("Please take a amount of input/outputcontainers"
												+ "that is not negative");
		this.MAXINPUT  = MAXINPUT;
		this.MAXOUTPUT = MAXOUTPUT;
		inputContainers = new IngredientContainer[getMAXINPUT()];
		
		for (int i = 0; i < getMAXINPUT();i++){
			inputContainers[i] = new IngredientContainer();
		}
		
		outputContainers = new IngredientContainer[getMAXOUTPUT()];
		for (int i = 0; i < getMAXOUTPUT(); i++){
			outputContainers[i] = new IngredientContainer();
		}
	}
	
	/**********************************************************
	 * number of input and output containers
	 *********************************************************/
	
	/**
	 * The maximum amount of input ingredients for this device
	 */
	private final int MAXINPUT;
	
	/**
	 * Returns the maximum number of inputs of this device
	 */
	@Basic @Immutable @Raw
	public int getMAXINPUT(){
		return MAXINPUT;
	}
	
	/**
	 * The maximum amount of output ingredients for this device
	 */
	private final int MAXOUTPUT;
	
	/**
	 * Returns the maximum number of outputs of this device
	 */
	@Basic @Immutable @Raw
	public int getMAXOUTPUT(){
		return MAXOUTPUT;
	}
	
	/**
	 * Checks if the number of input and output containers are
	 * valid for a device
	 * @param 	input
	 * 			The number of inputcontainers
	 * @param	output
	 * 			The number of outputcontainers
	 * @return	True if both numbers are greater or equal 
	 * 			to zero
	 * 			| result == (input >= 0) && (output >= 0)
	 */
	public static boolean isValidAmountOfInAndOutPuts(int input, int output){
		return (input >= 0) && (output >= 0);
	}
	
	/*****************************************************
	 * inputContainers
	 ****************************************************/
	/**
	 * An array containing universal containers (containers
	 * can be empty) where input ingredients will be stored
	 */
	private final IngredientContainer[] inputContainers;
	
	/**
	 * Returns the input container at the index position
	 * @param	index
	 * 			The index of the input container
	 * @throws	IndexOutOfBoundsException
	 * 			The index is not in [1,MAXINPUT]
	 * 			| index <= 0 || index > getMAXINPUT()
	 */
	@Basic
	public IngredientContainer getInputAt(int index) 
			throws IndexOutOfBoundsException{
		if (index <= 0 || index > getMAXINPUT())
			throw new IndexOutOfBoundsException();
		return inputContainers[index-1];
	}
	
	/**
	 * Returns the amount of input ingredients currently in this device
	 * @return	The amount of containers in this device that 
	 * 			contain an ingredient.
	 * 			| count = 0;
	 *			| for each i : 1..getMAXINPUT():
	 *			|	if (getInputAt(i).hasIngredient())
	 *			| 		count++
	 *			| result ==  count;
	 */
	public int getNbOfInputs(){
		int count = 0;
		for (int i = 1; i <= getMAXINPUT(); i++){
			if (getInputAt(i).hasIngredient()){
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Adds an ingredient to this device
	 * @param	container
	 * 			The a container with the ingredient to
	 * 			be added.
	 * @effect  Removes the ingredient from its container and adds it to the
	 * 			first input container that is free, the original container gets teminated.
	 * 			|container.getIngredient().move(getInputAt(findFirstEmpty()))
	 * 			|container.terminate()
	 * @throws	IllegalArgumentException
	 * 			The container has no ingredient
	 * 			| !container.hasIngredient()
	 * @throws	IllegalStateException
	 * 			There are no free inputcontainers
	 * 			| getNbOfInputs() == getMAXINPUT()
	 *
	 */
	public void addIngredient(TransportContainer container)
			throws IllegalArgumentException, IllegalStateException {
		if (!container.hasIngredient()){
			throw new IllegalArgumentException("Cannot add an empty container to a device.");
		}
		container.getIngredient().move(getInputAt(findFirstEmpty()));
		container.terminate();
	}
	
	/**
	 * Finds the index of the first container in this device that 
	 * has no ingredient. 
	 * @return	The index of the first container that has no ingredient
	 * 			| for i in 1..getMAXINPUT():
	 *			|	if (!getInputAt(i).hasIngredient()){
	 *			|		return i;
	 * @throws	IllegalStateException
	 *			The container has no empty containers
	 *			| for i in 1..getMAXINPUT():
	 *			|	getInputAt(i).hasIngredient()
	 */
	public int findFirstEmpty()
			throws IllegalArgumentException{
		for (int i = 1; i <= getMAXINPUT(); i++){
			if (!getInputAt(i).hasIngredient()){
				return i;
			}
		}
		throw new IllegalStateException("There are no empty inputcontainers.");
	}
	
	/*****************************************************
	 * outputContainers
	 ****************************************************/
	/**
	 * An array containing universal containers for the output
	 * (containers can be empty)
	 */
	private final IngredientContainer[] outputContainers;

	/**
	 * Returns the output container at the index position
	 * @param	index
	 * 			The index of the container
	 * @throws	IndexOutOfBoundsException
	 * 			The index is not in [1,MAXOUTPUT]
	 * 			| index <= 0 || index > getMAXOUTPUT()
	 */
	@Basic
	public IngredientContainer getOutputAt(int index) 
			throws IndexOutOfBoundsException{
		if (index <= 0 || index > getMAXOUTPUT())
			throw new IndexOutOfBoundsException();
		return outputContainers[index-1];
	}
	
	/**
	 * Returns the amount of output ingredients currently in this device
	 * @return	The amount of output containers in this device that
	 * 			contain an ingredient.
	 * 			| count = 0;
	 *			| for each i : 1..getMAXOUTPUT():
	 *			|	if (getOutputAt(i).hasIngredient())
	 *			| 		count++
	 *			| result ==  count;
	 */
	public int getNbOfOutputs(){
		int count = 0;
		for (int i = 1; i <= getMAXOUTPUT(); i++){
			if (getOutputAt(i).hasIngredient()){
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Extracts an ingredient from the device at a given index
	 * @param	container
	 * 			The container where the output will be stored
	 * @param	index
	 * 			The index of the output container
	 * @effect	The ingredient of the output container at the index
	 * 			is moved to the given container.
	 * 			|getOutputAt(index).getIngredient().move(container)
	 * @throws	IndexOutOfBoundsException
	 * 			The container at the index holds no ingredient
	 * 			|getOutPutAt(index).getIngredient() == null
	 */
	public void extractIngredient(TransportContainer container, int index)
			throws IllegalArgumentException, IndexOutOfBoundsException, IllegalStateException {
		if (getOutputAt(index).getIngredient() == null)
			throw new IllegalArgumentException("The index refers to an empty outputcontainer");
		getOutputAt(index).getIngredient().move(container);
	}
	
	/**
	 * Takes all of the input ingredients, changes them in some way and
	 * moves them to the output ingredients.
	 * @throws	ArithmeticException [can]
	 * 			| ? true
	 */
	public abstract void process() throws ArithmeticException;
	
	/**
	 * Checks if the device can be used.
	 * @return	True if the device is in a laboratory.
	 * 			| result == hasLaboratory()
	 */
	public boolean canBeProcessed(){
		return hasLaboratory();
	}
	
	/***************************************************************************
	 * Laboratory
	****************************************************************************/
	/**
	 * Where this device is currently located (can be null)
	 */
	private Laboratory lab = null;
	
	/**
	 * Returns the laboratory where this device is stored
	 */
	@Basic
	public Laboratory getLaboratory(){
		return lab;
	}
	
	/**
	 * Sets the location where this device is stored
	 * @param	lab
	 * 			The laboratory where this device is stored
	 * @post	The laboratory of this device is the given lab
	 * 			| new.getLaboratory() == lab
	 */
	@Model
	protected void setLaboratory(Laboratory lab){
		this.lab = lab;
	}
	
	/**
	 * Checks if this device has a laboratory
	 * @return	True if this device has a laboratory
	 * 			| result == (getLaboratory() != null)
	 */
	public boolean hasLaboratory(){
		return (getLaboratory() != null);
	}
	
	/**
	 * Adds this device to a laboratory
	 * @param 	lab
	 * 			The laboratory this device is added to.
	 * @throws	IllegalStateException(this)
	 * 			This device already is in a laboratory
	 * 			| hasLaboratory()
	 * @throws	IllegalStateException(laboratory) [can]
	 * 			? true
	 */
	public abstract void addTo(Laboratory lab) throws IllegalStateException;
	/**
	 * Removes this device from its laboratory
	 * @throws	IllegalStateException
	 * 			This device has no laboratory
	 * 			| !hasLaboratory()
	 */
	public abstract void removeFromLab() throws IllegalStateException;
}
