package Alchemy;

/**
 * A cooling box that can cool an ingredient to its
 * temperature.
 * 
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 */
public class CoolingBox extends TempAlteringDevice {
	/**
	 * Creates a new coolingbox with a given temperature
	 * @param   temperature
	 * 			The temperature this device will be set at
	 * @effect	The cooling box is a temperature altering device with one input
	 * 			and one output
	 * 			|super(1,1,temperature)
	 */
	public CoolingBox(Temperature temperature) throws IllegalArgumentException {
		super(1,1,temperature);
	}

	/*********************************************************
	 * Overrides
	 ********************************************************/
	/**
	 * Sets the temperature of the ingredient currently in input 
	 * to the temperature of this cooling box and moves the ingredient to
	 * the output. If there are pre-existing output ingredients then they
	 * are terminated.
	 * @effect	If the cooling box can be processed then
	 * 			every ingredient in an output slot is terminated and if the corresponding
	 * 			input slot has an ingredient that ingredient's temperature is set
	 * 			to the temperature of this cooling box and is moved to the output container	
	 * 			| if (canBeProcessed())
	 * 			| then
	 * 			| 	for each I in 1..getMAXOUTPUT():
	 * 			|		if (getOutPutAt(i).hasIngredient())
	 * 			|		then   getOutputAt(i).getIngredient().terminate()  	
	 * 			|	 for each I in 1..getMAXINPUT():
	 * 			|		if getOutputAt(i).hasIngredient()
	 * 			|		then 	if 		getInputAt(i).getIngredient().getTemperature().isGreaterThan(this.getTemperature()) 
	 * 			|				then	getInputAt(i).getIngredient().setTemperature(this.getTemperature()
	 *			|				getInputAt(i).getIngredient().move(getOutputAt(i))
	 */
	@Override
	public void process() {
		if (!canBeProcessed())
			return;
		for (int i = 1; i <= getMAXOUTPUT(); i++){
			// Empty the last output(s)
			if (getOutputAt(i).hasIngredient())
				getOutputAt(i).getIngredient().terminate();
		}
		for (int i = 1; i <= getMAXINPUT(); i++){
			if (getInputAt(i).hasIngredient()){
				if (getInputAt(i).getIngredient().getTemperature().isGreaterThan(getTemperature()) ){
					// Cools the ingredient(s)
					getInputAt(i).getIngredient().setTemperature(this.getTemperature());
				}
				// moves it to the output
				getInputAt(i).getIngredient().move(getOutputAt(i));
			}
		}
	}

	/**
	 * Adds this cooling box to a laboratory
	 * @param	lab
	 * 			The laboratory where this cooling box will be added.
	 * @effect	The cooling box's laboratory is set to the given lab and
	 * 			the lab's cooling box is set to this.
	 * 			|	setLaboratory(lab)
	 * 			|	lab.setCoolingBox(this)
	 * @throws	IllegalArgumentException
	 * 			The given lab is null
	 * 			| lab == null
	 * @throws	IllegalStateException
	 * 			The laboratory already has an cooling box
	 * 			| lab.hasCoolingBox()
	 * @throws	IllegalStateException
	 * 			The cooling box already has a laboratory
	 * 			| hasLaboratory()
	 */
	@Override
	public void addTo(Laboratory lab) throws IllegalStateException, IllegalArgumentException {
		if (lab == null)
			throw new IllegalArgumentException("The new lab can not be null");
		if (lab.hasCoolingBox())
			throw new IllegalStateException("The lab already has a cooling box");
		if (this.hasLaboratory())
			throw new IllegalStateException("This device already has a laboratory");
		setLaboratory(lab);
		lab.setCoolingBox(this);
	}

	/**
	 * Removes this cooling box from its laboratory
	 * @effect	The cooling box's laboratory is set to null and
	 * 			that laboboratory's cooling box is set to null
	 * 			| Laboratory temp = this.getLaboratory()
	 * 			| this.setLaboratory(null)
	 * 			| temp.setCoolingBox(null)
	 * @throws	IllegalStateException
	 * 			The cooling box isn't in a laboratory
	 * 			| !hasLaboratory()
	 */
	@Override
	public void removeFromLab() throws IllegalStateException {
		if (!hasLaboratory())
			throw new IllegalStateException();
		Laboratory temp = this.getLaboratory();
		this.setLaboratory(null);
		temp.setCoolingBox(null);
	}


}
