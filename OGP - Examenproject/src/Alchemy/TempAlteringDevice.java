package Alchemy;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class that bundles all devices whose purpose it is to alter
 * the temperature of given ingredient
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version	1.0
 * @invar  	This has a valid temperature
 * 			| isValidTemperature(getTemperature())
 */
public abstract class TempAlteringDevice extends Device {
	/**
	 * Creates a new device with emtpy input and output containers
	 * @param 	MAXINPUT
	 * 			The number of input containers of this device
	 * @param	MAXOUTPUT
	 * 			The number of output containers of this device
	 * @effect	The tempalteringdevice is a device
	 * 			| super(MAXINPUT, MAXOUTPUT)
	 * @effect	The temperature is set to the given argument
	 * 			| setTemperature(temperature)
	 */
	@Model
	protected TempAlteringDevice(int MAXINPUT,int MAXOUTPUT,Temperature temperature)
				throws IllegalArgumentException {
		super(MAXINPUT, MAXOUTPUT);
		setTemperature(temperature);
	}
	
	/****************************************************************
	 * temperature
	 ***************************************************************/
	
	/**
	 * The temperature to which this device is set (default [0,20])
	 */
	private Temperature temperature = new Temperature(0,20);
	
	/**
	 * Returns the temperature this tempAlteringDevice set at.
	 */
	@Basic
	public Temperature getTemperature(){
		return this.temperature;
	}
	
	/**
	 * Sets the temperature of this device
	 * @param	temperature
	 * 			The new temperature of this device
	 * @post	The temperature is set to the given
	 * 			temperature.
	 * 			| getTemperature() == temperature
	 * @throws	IllegalArgumentException
	 * 			The given temperature is not valid
	 * 			| !isValidTemperature(temperature)
	 */
	@Raw
	public void setTemperature(Temperature temperature)
			throws IllegalArgumentException{
		if (!CoolingBox.isValidTemperature(temperature))
			throw new IllegalArgumentException();
		this.temperature = temperature;
	}
	
	/**
	 * Checks if a temperature is valid
	 * @param	temperature
	 * 			The temperature to be checked
	 * @return	True if the temperature is not null
	 * 			| result == (temperature != null)
	 */
	public static boolean isValidTemperature(Temperature temperature){
		return (temperature != null);
	}
	
	/********************************************************
	 * Overrides
	 ********************************************************/
	/**
	 * Adds an ingredient to this device
	 * @param	container
	 * 			The container with the ingredient to
	 * 			be added.
	 * @effect  Removes the ingredient from its container and adds it to the
	 * 			first input container, if there already is a ingredient there
	 * 			then the ingredient is terminated, the original container gets teminated.
	 * 			|if (getInputAt(1).hasIngredient())
	 * 			|	getInputAt(1).getIngredient().terminate()
	 * 			|container.getIngredient().move(getInputAt(1))
	 * 			|container.terminate()
	 * @throws	IllegalArgumentException
	 * 			The container has no ingredient
	 * 			| !container.hasIngredient()
	 */
	@Override
	public void addIngredient(TransportContainer container)
			throws IllegalArgumentException, IllegalStateException {
		if (!container.hasIngredient()){
			throw new IllegalArgumentException("Cannot add an empty container to a device.");
		}
		if (getInputAt(1).hasIngredient()){
			getInputAt(1).getIngredient().terminate();
		}
		container.getIngredient().move(getInputAt(1));
		container.terminate();
	}
}
