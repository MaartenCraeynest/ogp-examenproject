 package Alchemy;

import be.kuleuven.cs.som.annotate.*;
/**
 * A class that represents an ingredient with a type,
 * temperature, volatility, state and container.
 * 
 * @invar	The ingredient has a proper container
 * 			| hasProperContainer()
 * @invar	The type is valid
 * 			| isValidType(getType())
 * @invar	The quantity is valid 
 * 			| canHaveAsQuantity(getQuantity())
 * @invar	The volatility deviation is valid
 * 			| isValidVolatilityDeviation(getVolatilityDeviation())
 * @invar	The temperature of the ingredient is valid
 * 			| canHaveAsTemperature(getTemperature())
 * @invar	The ingredient has a valid state
 * 			| canHaveAsState(getState())
 * 
 * @author  Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 */
public class AlchemicIngredient {
	/*********************************************************
	 * constructors
	 *********************************************************/
	/**
	 * Intitializes a new alchemic ingredient with the given type,state,quantity
	 * and temperature
	 * @param 	type
	 * 			The type to set the type of this alchemic ingredient to
	 * @param 	state
	 * 			The state to set the state of this alchemic ingredient to
	 * @param 	quantity
	 * 			The quantity to set the quantity of this alchemic 
	 * 			ingredient to
	 * @param 	temperature
	 * 			The temperature to set the temperature of this alchemic
	 * 			ingredient to
	 * @post	The type is set to the given type
	 * 			| new.getType()==type
	 * @post	The quantity is set to the given quantity
	 * 			| new.getQuantity()==quantity
	 * @post	The ingredient has a volatility deviation between -0.1 and 0.1 at random
	 * 			| new.getVolatilityDeviation() <= 0.1 && new.getVolatilityDeviation() >= -0.1
	 * @effect	Sets the temperature of this ingredient to the given temperature
	 * 			| setQuantity(quantity)
	 * @effect	Sets the state of this ingredient to the given state
	 * 			| setState(state)
	 * @throws 	IllegalArgumentException
	 * 			if the given quantity is zero, the given type is null
	 * 			or the given state is null
	 * 			| !isValidType(type)||state==null||!isValidTemperature(temperature)|| !isValidQuantity(quantity)
	 */
	public AlchemicIngredient(AlchemicType type,States state,Quantity quantity,Temperature temperature)throws IllegalArgumentException{
		if(!isValidType(type)||!canHaveAsState(state)||!canHaveAsTemperature(temperature)||!canHaveAsQuantity(quantity)){
			throw new IllegalArgumentException();
		}
		setState(state);
		setTemperature(temperature);
		this.type=type;
		this.volatilityDeviation=(Math.random()-.5)/5;
		this.quantity=quantity;
	}
	
	/**
	 * Initializes a new alchemic ingredient with the given type quantity and
	 * volatility deviation.
	 * @param 	type
	 * 			The type to set the type of this alchemic ingredient to
	 * @param 	quantity
	 * 			The quantity to set the quantity of this alchemic ingredient
	 * 			to
	 * @param 	volatilityDeviation
	 * 			The the deviation of the volatility of this ingredient
	 * 			when compared to the theoretic volatility of the type
	 * @effect	Sets the state of this ingredient to the standard state
	 * 			| setState(type.getStandardState())
	 * @effect	Sets the temperature of this ingredient to the standardTemperature
	 * 			| setTemperature(type.getStandardTemperature())
	 * @post	The quantity of this ingredient is set to the given quantity
	 * 			| new.getQuantity()==quantity
	 * @post	The volatilityDeviation of the ingredient is set to the given volatility
	 * 			| new.getVolatilityDeviation() == volatilityDeviation
	 * @post	The type of this ingredient is set to the given type
	 * 			| new.getType() == type
	 * @throws 	IllegalArgumentException
	 * 			if the given quantity is zero, the given type is null
	 * 			or the given state is null
	 * 			| !isValidType(type)|| !isValidQuantity(quantity)
	 * @throws 	IllegalArgumentException
	 * 			The volatilityDeviation is not within -0.1 to 0.1
	 * 			| !isValidVolatility(volatility)
	 */
	public AlchemicIngredient(AlchemicType type, Quantity quantity, double volatilityDeviation)throws IllegalArgumentException{
		if(!isValidType(type))
			throw new IllegalArgumentException();
		if (!canHaveAsQuantity(quantity))
			throw new IllegalArgumentException();
		if (!AlchemicIngredient.isValidVolatilityDeviation(volatilityDeviation))
			throw new IllegalArgumentException();
		
		this.type=type;
		this.quantity = quantity;
		this.volatilityDeviation = volatilityDeviation;
		setTemperature(getType().getStandardTemperature());
		setState(type.getStandardState());
	}
	
	/**
	 * Initializes an ingredient with a type with given name, standard state, 
	 * standard temperature and theoretic volatility. The state and temperature 
	 * of the ingredient are set to the standardstate 
	 * and standard temperature, the quantity is set to 1 spoon
	 * 
	 * @param 	name
	 * 			The simple name to give to the type of this ingredient
	 * @param 	standardstate
	 * 			The standard state to give to the type of this ingredient
	 * @param 	standardtemp
	 * 			The standard temperature to give to the type of this ingredient
	 * @param 	theoreticVolatility
	 * 			The theoretic volatility to give to the type of this ingredient
	 * @effect	The type of the new ingredient is a new unmixed type
	 * 			with given name,standard state,standard temperature
	 * 			and theoreticVolatility. The new ingredient is set with this
	 * 			new type, the standard temperature of the type,
	 * 			the standard state of the type and a quantity equal to 
	 * 			one spoon.
	 * 			| type=AlchemicTypeUnmixed(name,standardstate,standardtemp,theoreticVolatility)
	 * 			| this(type,standardstate,new Quantity(24) ,standardtemp);
	 */
	public AlchemicIngredient(String name,States standardstate,Temperature standardtemp,double theoreticVolatility) 
			throws IllegalArgumentException{
		this(new AlchemicTypeUnmixed(name,standardstate,standardtemp,theoreticVolatility),standardstate,new Quantity(24) ,standardtemp);
	}
	
	/**
	 * Initializes a new alchemic ingredient with the given type,state,quantity,
	 * temperature and container
	 * @param 	type
	 * 			The type to set the type of this alchemic ingredient to
	 * @param 	state
	 * 			The state to set the state of this alchemic ingredient to
	 * @param 	quantity
	 * 			The quantity to set the quantity of this alchemic 
	 * 			ingredient to
	 * @param 	temperature
	 * 			The temperature to set the temperature of this alchemic
	 * 			ingredient to
	 * @param 	container
	 * 			If this ingredient can have the given container as a container
	 * 			sets the container of this ingredient to the given container
	 * @effect	Initializes a new alchemic ingredient with the given type,state,quantity
	 * 			and temperature
	 * 			| this(type,state,quantity,temperature)
	 * @effect	The ingredient is in the container
	 * 			| container.fill(this)
	 * 
	 */
	public AlchemicIngredient(AlchemicType type,States state,Quantity quantity,Temperature temperature,IngredientContainer container)
			throws IllegalArgumentException, IllegalStateException {
		this(type,state,quantity,temperature);
		container.fill(this);
	}
	
	/**
	 * Initialize a quantity of water
	 * @param 	quantity
	 * 			The quantity of water to initialize
	 * @effect	Initializes water with given quantity and container
	 * 			|this(new AlchemicTypeUnmixed(),States.LIQUID,quantity,new Temperature(0,20),container)
	 */
	public AlchemicIngredient(Quantity quantity){
		this(new AlchemicTypeUnmixed(),States.LIQUID,quantity,new Temperature(0,20));
	}
	/*********************************************************
	 * container
	 *********************************************************/
	/**
	 * The container where this ingredient is stored, can be null if
	 * this ingredient is not in a container
	 */
	private IngredientContainer container = null;
	
	/**
	 * Checks if this ingredient has a container
	 * @return	True if this ingredient has no container
	 * 			| result == (getContainer != null)
	 */
	public boolean hasContainer(){
		return (getContainer() != null);
	}
	
	/**
	 * Returns the container of this ingredient
	 */
	@Basic
	public IngredientContainer getContainer(){
		return container;
	}
	
	/**
	 * Sets the ingredient's container to a new container
	 * @param	newContainer
	 * 			The new container of the ingredient
	 * @post	The ingredient's container is equal to the given container
	 * 			| getContainer() == newContainer
	 * @throws 	IllegalStateException
	 * 			This ingredient has a container and the current container still has this ingredient
	 * 			as its content 
	 * 			| hasContainer() && getContainer().getIngredient() == this
	 * @throws	IllegalArgumentException
	 * 			The new container is not null and doesn't have this as its ingredient
	 * 			| newContainer != null && newContainer.getIngredient != this
	 * @throws	IllegalArgumentException
	 * 			This ingredient is terminated
	 * 			| this.isTerminated()
	 */
	@Model @Raw
	protected void setContainer(IngredientContainer newContainer) throws IllegalStateException {
		if (isTerminated() && newContainer != null)
			throw new IllegalArgumentException("A terminated ingredient cannot have a container");
		if (hasContainer() && getContainer().getIngredient() == this){
			throw new IllegalStateException("This container still has a valid container");
		}
		else if (newContainer != null && newContainer.getIngredient() != this){
			throw new IllegalArgumentException("The container must have this set as it's ingredient");
		}
		container = newContainer;
	}
	
	/**
	 * Checks if this ingredient can have a container as its container
	 * @return 	True if this ingredient is not terminated and the state of this
	 * 			ingredient is supported by the container and the quantity of this ingredient
	 * 			is smaller or equal to the maximum capacity of the container.
	 * 			| result == (!isTerminated() && container.canContainer(this.getState()) &&
	 * 			|				container.getCapacity().isGreaterOrEqualTo(this.getCapacity())
	 */
	@Raw
	public boolean canHaveAsContainer(IngredientContainer container){
		return !isTerminated() && container.canContain(this.getState()) && 
					container.getCapacity().isGreaterOrEqualTo(this.getQuantity());
	}
	
	/**
	 * Checks if this ingredient has a proper container
	 * @return	True if this has no container or this's container has this as its ingredient
	 * 			and is a valid container
	 * 			| result == (hasContainer() || (getContainer().getIngredient() == this 
	 * 											&& canHaveAsContainer(getContainer())))
	 */
	@Raw
	public boolean hasPropperContainer(){
		return (!hasContainer() || (getContainer().getIngredient() == this 
				 											&& canHaveAsContainer(getContainer())));
	}
	
	/**
	 * Moves this ingredient to the given container
	 * @param 	newContainer
	 * 			The container to move this ingredient to
	 * @effect	If the current ingredient has a container,
	 * 			the old container is emptied and the given container is
	 * 			set to the container of this ingredient
	 * 			|if (hasContainer())
	 * 			|	then
	 * 			|		getContainer().empty()
	 * 			|		newcontainer.fill(this)
	 * @throws	IllegalArgumentException()
	 * 			The newContainer is null
	 * 			|	newContainer == null
	 * @throws	IllegalStateException
	 * 			The given container already holds an ingredient or
	 * 			cannot hold this.
	 * 			| newContainer.hasIngredient() || !newContainer.canHaveAsIngredient(this)
	 * 			
	 */
	public void move(IngredientContainer newContainer)
			throws IllegalStateException, IllegalStateException{
		if (newContainer == null)
			throw new IllegalArgumentException("Cannot move ingredient to null");
		if (newContainer.hasIngredient() || !newContainer.canHaveAsIngredient(this))
			throw new IllegalStateException("The container cannot be used for this ingredient");
		
		if (hasContainer())
			getContainer().empty();
		newContainer.fill(this);
		
	}

	/*********************************************************
	 * Type
	 *********************************************************/
	/**
	 * Variable registering the type of this ingredient
	 */
	private final AlchemicType type;
	
	/**
	 * Returns the type of this ingredient
	 */
	@Basic
	public AlchemicType getType() {
		return type;
	}
	
	/**
	 * Checks if a given type can be used as a type of this
	 * ingredient
	 * @param	type
	 * 			The type to be checked
	 * @return	True if the type is not null
	 * 			| result == (type != null)
	 */
	public static boolean isValidType(AlchemicType type){
		return  type != null;
	}

	/*********************************************************
	 * state
	 *********************************************************/
	/**
	 * variable registering the state this ingredient is in
	 */
	private States state;
	/**
	 * Returns the state of this ingredient
	 */
	@Basic
	public States getState() {
		return state;
	}
	/**
	 * Sets the state of this container to the given state
	 * @param 	state 
	 * 			The state to set
	 * @post	The state is set to the given state
	 * 			| new.getState()==state
	 * @throws	IllegalArgumentException
	 * 			If the given state is null
	 * 			| !isValidState(state)
	 */
	@Model @Raw
	private void setState(States state)throws IllegalArgumentException {
		if(!canHaveAsState(state)){
			throw new IllegalArgumentException();
		}
		this.state = state;
	}
	
	/**
	 * Checks if an ingredient can have the given state as its state
	 * @param	state
	 * 			The state to check
	 * @return	True if the ingredient is terminated and the state is null or if 
	 * 			the ingredient is not terminated and the state is not null
	 * 			| result == (isTerminated() && state == null) || (state != null)
	 */
	@Raw
	public boolean canHaveAsState(States state){
		return (isTerminated() && state == null) || (!isTerminated() && state != null);
	}
	
	/**
	 * Changes the state of this ingredient to the state it is currently not in
	 * @effect	sets the state to the other state
	 * 			|if(getState().equals(States.LIQUID))
	 * 			|then setState(States.POWDER)
	 * 			|else setState(States.LIQUID)
	 */
	public void changeState(){
		if(getState().equals(States.LIQUID)){
			setState(States.POWDER);
		}
		else if(getState().equals(States.POWDER)){
			setState(States.LIQUID);
		}
	}
	/*********************************************************
	 * quantity
	 *********************************************************/

	/**
	 * Variable registering the quantity of this ingredient
	 */
	private Quantity quantity;
	
	/**
	 * Returns the quantity
	 */
	@Basic
	public Quantity getQuantity() {
		return quantity;
	}
	/**
	 * Creates a clone of this object with a quantity
	 * equal to the given quantity
	 * @param 	quantity 
	 * 			The quantity of the clone
	 * @return	Creates a new ingredient with the same type, state, temperature,
	 * 			volatilityDeviation and the given quantity 
	 * 			| result == new AlchemicIngredient(getType(),getState(),quantity,getTemperature(),container)
	 * @pre 	If this ingredient has a container, requires a quantity that is
	 * 			smaller than or equal to the size of the container
	 * 			|if(hasContainer())
	 * 			|	then
	 * 			|	getContainer().getCapacity()>=quantity 
	 * @pre 	The quantity of the given quantity must be greater than 0
	 * 			|getQuantity().getQuantity()>0 
	 */
	public AlchemicIngredient setQuantity(Quantity quantity)throws IllegalArgumentException {
		if(hasContainer()){
			IngredientContainer container=getContainer();
			getContainer().empty();
			return new AlchemicIngredient(getType(),getState(),quantity,getTemperature(),container);
		}
		else{
			return new AlchemicIngredient(getType(),getState(),quantity,getTemperature());
		}
	}
	
	/**
	 * Check if an ingredient can have a quantitiy as its quantity
	 * @param	quantity
	 * 			The quantity to check
	 * @return	True if the container is terminated and the quantity is null
	 * 			or if the container is not terminated and the quantity is not null,
	 * 			and the quantity is greater then the 0 quantity
	 * 			| result  == (isTerminated() && quantity == null)||
	 * 			|			(!isTerminated() && (quantity != null) 
	 * 			|			&& !(new Quantity(0)).isGreaterOrEqualTo(quantity))
	 */
	@Raw
	public boolean canHaveAsQuantity(Quantity quantity){
		return (isTerminated() && quantity == null) ||(!isTerminated() && (quantity != null) && (!(new Quantity(0)).isGreaterOrEqualTo(quantity)));
	}
	
	/*********************************************************
	 * temperature
	 *********************************************************/
	/**
	 * variable registering the temperature of this ingredient
	 */
	private Temperature temperature = new Temperature(0,0);
	/**
	 * Returns the temperature
	 */
	@Basic
	public Temperature getTemperature() {
		return temperature;
	}

	/**
	 * Sets the temperature of this ingredient
	 * @param 	temperature 
	 * 			The temperature to set
	 * @throws	IllegalArgumentException
	 * 			if the given temperature is null
	 * 			| !canHaveAsTemperature(temperature)
	 */
	@Raw
	public void setTemperature(Temperature temperature)throws IllegalArgumentException {
		if(!canHaveAsTemperature(temperature)){
			throw new IllegalArgumentException();
		}
		this.temperature = temperature;
	}
	
	/**
	 * Checks if the temperature is valid for this ingredient
	 * @param	temperature
	 * 			The temperature to check
	 * @return	True if the ingredient is terminated and the temperature is null or
	 * 			the ingredient is not terminated and the temperature is not null
	 * 			| result == (isTerminated() && temperature == null) || (temperature != null)
	 */
	public Boolean canHaveAsTemperature(Temperature temperature){
		return (isTerminated() && temperature == null) || (!isTerminated() && temperature != null);
	}
	
	/*********************************************************
	 * volatility
	 *********************************************************/
	/**
	 * variable registering the deviation of the theoretic 
	 * volatility of this ingredient
	 */
	private final double volatilityDeviation;
	/**
	 * Return the volatilityDeviation
	 */
	@Basic @Immutable
	public double getVolatilityDeviation() {
		return volatilityDeviation;
	}
	
	/**
	 * Checks if a volatility deviation is valid
	 * @param	deviation
	 * 			The deviation to be checked
	 * @return	True if the deviation is between -0.1 and 0.1
	 * 			| result == (deviation >= -0.1 && deviation >= 0.1)
	 */
	public static boolean isValidVolatilityDeviation(double deviation){
		return (deviation >= -0.1 && deviation <= 0.1);
	}
	/**
	 * Returns the effective amount the intrinsic volatility
	 * differs from the theoretic volatility of this type
	 * @return	The effective difference between intrinsic and
	 * 			theoretic volatility
	 * 			| result == getIntrinsicVolatility()-getType().getTheoreticVolatility()
	 */
	@Immutable
	public double getEffectiveVolatilityDeviation(){
		return getIntrinsicVolatility()-getType().getTheoreticVolatility();
	}

	/**
	 * Returns the intrinsic volatility
	 * @return 	The theoretic volatility + the volatility deviation
	 * 			if it is in the interval [0,1], otherwise the value
	 * 			is rounded to the nearest integer 
	 * 			|result=type.getTheoreticVolatility()+getVolatilityDeviation()*type.getTheoreticVolatility();
	 *			|if(intr>1||intr<0)
	 *			|	then
	 *			|	result=Math.round(result);
	 */
	@Immutable
	public double getIntrinsicVolatility(){
		double intr=type.getTheoreticVolatility()+getVolatilityDeviation()*type.getTheoreticVolatility();
		if(intr>1||intr<0){
			intr=Math.round(intr);
			
		}
		return intr;
	}
	/**
	 * Returns the effective volatility if the temperature equals 
	 * the standard temperature
	 * @return	The effective volatility if the temperature equals 
	 * 			the standard temperature 
	 * 			|result == Math.max(10*getIntrinsicVolatility()*(getType().getStandardTemperature().getHotness()-getType().getStandardTemperature().getColdness()),
	 * 			|			   0)
	 */
	@Immutable
	public double getVolatilityAtStandardTemperature(){
		double eff=10*getIntrinsicVolatility()*(getType().getStandardTemperature().getHotness()-getType().getStandardTemperature().getColdness());
		if(eff<0){
			eff=0;
		}
		return eff;
	}
	/**
	 * Returns the effective volatility at the 
	 * current tmperature
	 * @return	The effective volatility
	 * 			| if(getTemperature().compare(getType().getStandardTemperature())>0)
	 * 			| then
	 * 			| result == getEffectiveVolatility()+300*getIntrinsicVolatility()*
	 * 			|				(getTemperature().compare(getType().getStandardTemperature())/
	 * 			|				(getType().getStandardTemperature().getHotness()-getType().getStandardTemperature().getColdness()));
	 * 			| else  if(getTemperature().compare(standard)!=0)
	 * 			|	    then
	 * 			|		result == getEffectiveVolatility()*(getTemperature().compare(getType().getStandardTemperature())/
	 * 			|					(getType().getStandardTemperature().getHotness()-getType().getStandardTemperature().getColdness()));
	 * @return	If the standard temperature of the type of this ingredient
	 * 			is zero and the temperature is nonzero, the effective volatility is 
	 * 			set to the maximum effective volatility
	 * 			|if(getTemperature().compare(getType().getStandardTemperature)!=0 && getType().getStandardTemperature()==0)
	 * 			|	then
	 * 			|	result == Double.MAX_VALUE
	 */
	public double getEffectiveVolatility(){
		double eff=getVolatilityAtStandardTemperature();
		Temperature standard=getType().getStandardTemperature();
		long totalstandard=Math.abs(standard.getHotness()-standard.getColdness());
		long compare=getTemperature().compare(standard);
		if(compare>0){
			if(standard.equals(new Temperature(0,0))){
				return Double.MAX_VALUE;
			}
			eff=eff+300*getIntrinsicVolatility()*(compare/(totalstandard));
		}
		else if(compare<0){
			if(standard.equals(new Temperature(0,0))){
				return Double.MAX_VALUE;
			}
			eff=eff*getIntrinsicVolatility()*(-compare/totalstandard);
		}
		return eff;
	}
	/**
	 * Returns the complete name of this alchemic ingredient
	 * @return	The complete name of this alchemic ingredient
	 * 			|name=type.toString();
	 *			|compare = type.getStandardTemperature().compare(getTemperature());
	 *			|if(compare<0){
	 *			|then name="Cooled "+name;
	 *			|else if(compare>0){
	 *			|	  then name="Heated "+name;
	 *			|
	 *			|if(this.getVolatilityAtStandardTemperature()>1000)
	 *			|then name=name+" Danger";
	 *			|
	 *			|if(this.getEffectiveVolatility()>1000)
	 *			|then name="Volatile "+name;
	 *			|else if(this.getEffectiveVolatility()<.01*this.getVolatilityAtStandardTemperature()&&this.getEffectiveVolatility()<100)
	 *			|	  then name="Inert "+name;
	 *			|result == name;
	 */
	public String getCompleteName(){
		String name=type.toString();
		long compare=type.getStandardTemperature().compare(getTemperature());
		if(compare>0){
			name="Cooled "+name;
		}
		else if(compare<0){
			name="Heated "+name;
		}
		if(this.getVolatilityAtStandardTemperature()>1000){
			name=name+" Danger";
		}
		if(this.getEffectiveVolatility()>1000){
			name="Volatile "+name;
		}
		else if(this.getEffectiveVolatility()<.01*this.getVolatilityAtStandardTemperature()&&this.getEffectiveVolatility()<100){
			name="Inert "+name;
		}
		return name;
		
	}
	/*********************************************************
	 * Termination
	 *********************************************************/
	/**
	 * Variable registering whether an ingredient has been terminated
	 */
	private boolean isTerminated = false;
	
	/**
	 * Returns whether or not this ingredient is terminated
	 */
	@Basic
	public boolean isTerminated(){
		return isTerminated;
	}
	/**
	 * sets the termination to the given boolean
	 * @param 	isTerminated
	 * 			The boolean to set isTerminated to
	 */
	@Model
	private void setTerminated(boolean isTerminated){
		this.isTerminated=isTerminated;
	}
	/**
	 * Terminates this ingredient
	 * @post	Removes the temperature of this ingredient
	 * 			|new.getTemperature()==null
	 * @post	Removes the quantity of this ingredient
	 * 			|new.getQuantity()==null
	 * @post	Removes the state of this ingredient
	 * 			| new.getState()==null
	 * @effect	Empties the container of this ingredient
	 * 			if it has one.
	 * 			| if (hasContainer())
	 * 			|	getContainer().empty()
	 * @effect	Sets this ingredient as terminated
	 * 			| setTerminated(true)
	 */
	public void terminate(){
		this.temperature=null;
		this.quantity=null;
		this.state=null;
		if(hasContainer()){
			getContainer().empty();
		}
		setTerminated(true);
	}
	/**
	 * Returns the name of this alchemic type
	 * @return	The name of the alchemic type of this ingredient
	 * 			| getType().toString()
	 */
	@Override
	public String toString(){
		return getType().toString();
	}
}
