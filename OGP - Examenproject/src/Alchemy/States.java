package Alchemy;
import be.kuleuven.cs.som.annotate.*;
/**
 * Enum containing the possible states of alchemic ingredients
 * @author Maarten Craeynest, Antony De Jonckheere
 * @version 1.0
 *
 */
public enum States {
	LIQUID("Liquid"),POWDER("Powder");
	
	/**
	 * String that contains the name of the state 
	 */
	private final String stateName;
	
	/**
	 * Initialize a new State with the given state
	 * @param 	name
	 * 			The name of this state
	 * @post	The name of this is state to the given name
	 * 			| new.getState().equals(state)
	 */
	private States(String state){
		this.stateName=state;
	}
	/**
	 * Returns the name of this state
	 */
	@Basic @Immutable
	public String getState(){
		return stateName;
	}
}