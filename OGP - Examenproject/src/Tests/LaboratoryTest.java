package Tests;

import static org.junit.Assert.*;

import org.junit.*;

import Alchemy.*;

public class LaboratoryTest {
	Laboratory labTest, emptyLab, fullLab; 
	Kettle kettleFullLab;
	Oven ovenFullLab;
	Transmogifier transmogifierFullLab;
	CoolingBox coolingBoxFullLab;
	
	AlchemicTypeUnmixed water, sugar, bread;
	AlchemicIngredient waterBarrel, iceSpoon, sugarCane, frenchToast;
	TransportContainer waterBarrelTransp,iceSpoonTransp,sugarCaneTransp,frenchToastTransp;
	

	@Before
	public void setUp() throws Exception {
		emptyLab = new Laboratory(5);
		fullLab = new Laboratory(6);
		kettleFullLab 			= new Kettle();
		kettleFullLab.addTo(fullLab);
		ovenFullLab 			= new Oven(new Temperature(0,0));
		ovenFullLab.addTo(fullLab);
		transmogifierFullLab 	= new Transmogifier();
		transmogifierFullLab.addTo(fullLab);
		coolingBoxFullLab 		= new CoolingBox(new Temperature(0,0));
		coolingBoxFullLab.addTo(fullLab);
		
		// Add some ingredients to the full lab
		water = new AlchemicTypeUnmixed();
		sugar = new AlchemicTypeUnmixed("Sugar", States.POWDER, new Temperature(0,20), 0.5);
		bread = new AlchemicTypeUnmixed("Bread", States.POWDER, new Temperature(0,20), 0.5);
		
		
		waterBarrel = new AlchemicIngredient(water, 
											  States.LIQUID,
											  new Quantity(Quantity.BARRELAMOUNT), 
											  new Temperature(0,20));
		iceSpoon = new AlchemicIngredient(water, 
												States.POWDER,
												new Quantity(Quantity.SPOONAMOUNT), 
												new Temperature(0,20));
		sugarCane = new AlchemicIngredient(sugar, 
												States.POWDER,
												new Quantity(Quantity.SPOONAMOUNT), 
												new Temperature(0,20));
		frenchToast = new AlchemicIngredient(bread, 
												States.POWDER,
												new Quantity(Quantity.SACHELAMOUNT), 
												new Temperature(0,20));
		
		waterBarrelTransp = new TransportContainer(waterBarrel);
		iceSpoonTransp = new TransportContainer(iceSpoon);
		sugarCaneTransp = new TransportContainer(sugarCane);
		frenchToastTransp = new TransportContainer(frenchToast);
		
		fullLab.storeIngredient(waterBarrelTransp);
		fullLab.storeIngredient(iceSpoonTransp);
		fullLab.storeIngredient(sugarCaneTransp);
		fullLab.storeIngredient(frenchToastTransp);
	}
	
	@After
	public void invarCheck() throws Exception {
		assertTrue(emptyLab.hasProperStorage());
		assertTrue(emptyLab.hasProperCoolingBox());
		assertTrue(emptyLab.hasProperOven());
		assertTrue(emptyLab.hasProperTransmogifier());
		assertTrue(emptyLab.hasProperKettle());
		assertTrue(emptyLab.canHaveAsCurrentStorage());
		
		
		assertTrue(fullLab.hasProperStorage());
		assertTrue(fullLab.hasProperCoolingBox());
		assertTrue(fullLab.hasProperOven());
		assertTrue(fullLab.hasProperTransmogifier());
		assertTrue(fullLab.hasProperKettle());
		assertTrue(fullLab.canHaveAsCurrentStorage());
	}

	@Test
	public void testLaboratory() {
		labTest = new Laboratory(5);
		assertEquals(new Quantity(5*Quantity.STOREROOMAMOUNT),
					 labTest.getMAXSTORAGE());
		assertEquals(new Quantity(0),
					 labTest.getCurrentStorage());
		assertFalse(labTest.hasCoolingBox());
		assertFalse(labTest.hasKettle());
		assertFalse(labTest.hasOven());
		assertFalse(labTest.hasTransmogifier());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testLaboratoryZero() {
		new Laboratory(0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testLaboratoryNegative() {
		new Laboratory(-1);
	}
	
	// Test case that will only do something if the storeroomquantity gets 
	// changed to a much bigger value 
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void testLaboratoryMAXINT(){
		if (Quantity.STOREROOMAMOUNT > (Long.MAX_VALUE/Integer.MAX_VALUE)){
			new Laboratory(Integer.MAX_VALUE);
		}
		else throw new IllegalArgumentException();
	}
	
	@Test
	public void testHasStored() {
		assertTrue(fullLab.hasStored(water));
		assertFalse(emptyLab.hasStored(water));
		assertFalse(fullLab.hasStored(null));
	}

	@Test
	public void testFindAlchemicType() {
		AlchemicTypeUnmixed nulltype = null;
		assertEquals(3,fullLab.find(water));
		assertEquals(-1,fullLab.find(nulltype));
		assertEquals(-1,emptyLab.find(water));
		
	}

	@Test
	public void testFindSimpleString() {
		String nullString = null;
		assertEquals(3,fullLab.findSimple("Water"));
		assertEquals(-1,fullLab.findSimple(nullString));
		assertEquals(-1,emptyLab.findSimple("Water"));
	}
	
	@Test
	public void testFindSpecialString(){
		AlchemicTypeMixed waterBread = new AlchemicTypeMixed("Bread mixed with Water", States.POWDER, new Temperature(0,20), 0);
		waterBread.setSpecialName("Bad Meatloaf");
		AlchemicIngredient waterBreadLoaf = new AlchemicIngredient(waterBread, States.LIQUID, new Quantity(Quantity.SACHELAMOUNT), new Temperature(0,20));
		TransportContainer loafSack = new TransportContainer(waterBreadLoaf);
		assertEquals(-1,fullLab.findSpecial("Bad Meatloaf"));
		fullLab.storeIngredient(loafSack);
		assertEquals(2,fullLab.findSpecial("Bad Meatloaf"));
		assertEquals(-1, fullLab.findSpecial(null));
	}
	
	@Test
	public void testGetStoredQuantityOf(){
		assertEquals(new Quantity(Quantity.SPOONAMOUNT + Quantity.BARRELAMOUNT),
								fullLab.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0),
								emptyLab.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0),
								fullLab.getStoredQuantityOf(null));
	}

	@Test
	public void testStoreIngredientExists() {
		// Store ingredient that already existed:
		AlchemicIngredient freshWater = new AlchemicIngredient(water, States.LIQUID, new Quantity(Quantity.BOTTLEAMOUNT), new Temperature(0,0));
		TransportContainer freshWaterBottle = new TransportContainer(freshWater);
		
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity expectedTotalQuantity = previousTotalQuantity.add(freshWater.getQuantity());
		
		Quantity previousWaterQuantity = fullLab.getStoredQuantityOf("Water");
		Quantity expectedWaterQuantity = previousWaterQuantity.add(freshWater.getQuantity());
		fullLab.storeIngredient(freshWaterBottle);
		assertTrue(freshWater.isTerminated());
		assertTrue(freshWaterBottle.isTerminated());
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		assertEquals(expectedWaterQuantity, fullLab.getStoredQuantityOf("Water"));
	}
	
	@Test
	public void testStoreIngredientDoesntExists() {
		// Store ingredient that does not exist
		AlchemicIngredient freshWater = new AlchemicIngredient(water, States.LIQUID, new Quantity(Quantity.BOTTLEAMOUNT), new Temperature(0,0));
		TransportContainer freshWaterBottle = new TransportContainer(freshWater);
			
		Quantity previousTotalQuantity = emptyLab.getCurrentStorage();
		Quantity expectedTotalQuantity = previousTotalQuantity.add(freshWater.getQuantity());
				
		Quantity previousWaterQuantity = emptyLab.getStoredQuantityOf("Water");
		Quantity expectedWaterQuantity = previousWaterQuantity.add(freshWater.getQuantity());
		emptyLab.storeIngredient(freshWaterBottle);
		assertTrue(freshWater.isTerminated());
		assertTrue(freshWaterBottle.isTerminated());
		assertEquals(expectedTotalQuantity, emptyLab.getCurrentStorage());
		assertEquals(expectedWaterQuantity, emptyLab.getStoredQuantityOf("Water"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testStoreIngredientNull(){
		emptyLab.storeIngredient(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testStoreIngredientEmptyContainer(){
		TransportContainer emptyContainer = new TransportContainer(StdContainers.BARREL);
		emptyLab.storeIngredient(emptyContainer);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testStoreIngredientOverFlow(){
		Laboratory smallLab = new Laboratory(1);
		while(true){
			AlchemicIngredient freshWater = new AlchemicIngredient(water, States.LIQUID, new Quantity(Quantity.BOTTLEAMOUNT), new Temperature(0,0));
			TransportContainer freshWaterBottle = new TransportContainer(freshWater);
			smallLab.storeIngredient(freshWaterBottle);
		}
	}
	
	@Test
	public void testExtractString() {
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity sugarQuantityStored = fullLab.getStoredQuantityOf("Sugar");
		
		TransportContainer transpCont = fullLab.extract("Sugar");
		
		assertTrue(transpCont.hasIngredient());
		assertFalse(fullLab.hasStored(transpCont.getIngredient().getType()));
		
		assertEquals(sugarQuantityStored, transpCont.getIngredient().getQuantity());
		Quantity expectedTotalQuantity = previousTotalQuantity.subStract(transpCont.getIngredient().getQuantity());
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		// Check if the properties are the same as the expected storage properties
		assertEquals("Sugar",transpCont.getIngredient().getType().getSimpleName());
		assertEquals(transpCont.getIngredient().getType().getStandardState(), transpCont.getIngredient().getState());
		assertEquals(transpCont.getIngredient().getType().getStandardTemperature(), transpCont.getIngredient().getTemperature());
	}
	
	@Test
	public void testExtractStringSpecialName() {
		AlchemicTypeMixed waterBread = new AlchemicTypeMixed("Bread mixed with Water", States.POWDER, new Temperature(0,20), 0);
		waterBread.setSpecialName("Bad Meatloaf");
		AlchemicIngredient waterBreadLoaf = new AlchemicIngredient(waterBread, States.LIQUID, new Quantity(Quantity.SACHELAMOUNT), new Temperature(0,20));
		TransportContainer loafSack = new TransportContainer(waterBreadLoaf);
		fullLab.storeIngredient(loafSack);
		
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity sugarQuantityStored = fullLab.getStoredQuantityOf("Bad Meatloaf");
		
		TransportContainer transpCont = fullLab.extract("Bad Meatloaf");
		
		assertTrue(transpCont.hasIngredient());
		assertFalse(fullLab.hasStored(transpCont.getIngredient().getType()));
		
		assertEquals(sugarQuantityStored, transpCont.getIngredient().getQuantity());
		Quantity expectedTotalQuantity = previousTotalQuantity.subStract(transpCont.getIngredient().getQuantity());
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		// Check if the properties are the same as the expected storage properties
		assertEquals("Bread mixed with Water",transpCont.getIngredient().getType().getSimpleName());
		assertEquals(transpCont.getIngredient().getType().getStandardState(), transpCont.getIngredient().getState());
		assertEquals(transpCont.getIngredient().getType().getStandardTemperature(), transpCont.getIngredient().getTemperature());
	}
	
	@Test
	public void testExtractStringNoExists() {
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		int previousNbOfIngredients = fullLab.getNbOfStoredTypes();
		
		TransportContainer transpCont = fullLab.extract("RocketFuel");
		
		assertFalse(transpCont.hasIngredient());
		assertEquals(previousTotalQuantity, fullLab.getCurrentStorage());
		assertEquals(previousNbOfIngredients, fullLab.getNbOfStoredTypes());
	}
	
	@Test
	public void testExtractStringNull() {
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		int previousNbOfIngredients = fullLab.getNbOfStoredTypes();
		
		TransportContainer transpCont = fullLab.extract(null);
		
		assertFalse(transpCont.hasIngredient());
		assertEquals(previousTotalQuantity, fullLab.getCurrentStorage());
		assertEquals(previousNbOfIngredients, fullLab.getNbOfStoredTypes());
	}
	
	@Test
	public void testExtractStringNoContainerExists() {
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity waterQuantityStored = fullLab.getStoredQuantityOf("Water");
		
		TransportContainer transpCont = fullLab.extract("Water");
		
		assertTrue(transpCont.hasIngredient());
		assertEquals(StdContainers.biggestContainerOf(transpCont.getIngredient().getState()),
					transpCont.getCapacity());
		assertFalse(fullLab.hasStored(transpCont.getIngredient().getType()));
		
		assertFalse(waterQuantityStored.equals(transpCont.getIngredient().getQuantity()));
		Quantity expectedTotalQuantity = previousTotalQuantity.subStract(waterQuantityStored);
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		// Check if the properties are the same as the expected storage properties
		assertEquals("Water",transpCont.getIngredient().getType().getSimpleName());
		assertEquals(transpCont.getIngredient().getType().getStandardState(), transpCont.getIngredient().getState());
		assertEquals(transpCont.getIngredient().getType().getStandardTemperature(), transpCont.getIngredient().getTemperature());
	}

	@Test
	public void testExtractStringQuantityValidAmount() {
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity waterQuantityStored = fullLab.getStoredQuantityOf("Water");
		Quantity extractedQuantity = new Quantity(Quantity.SPOONAMOUNT);
		
		TransportContainer transpCont = fullLab.extract("Water", extractedQuantity);
		
		assertTrue(transpCont.hasIngredient());
		assertTrue(fullLab.hasStored(transpCont.getIngredient().getType()));
		
		assertEquals(waterQuantityStored.subStract(extractedQuantity),fullLab.getStoredQuantityOf("Water"));
		assertEquals(extractedQuantity, transpCont.getIngredient().getQuantity());
		
		Quantity expectedTotalQuantity = previousTotalQuantity.subStract(extractedQuantity);
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		// Check if the properties are the same as the expected storage properties
		assertEquals("Water",transpCont.getIngredient().getType().getSimpleName());
		assertEquals(transpCont.getIngredient().getType().getStandardState(), transpCont.getIngredient().getState());
		assertEquals(transpCont.getIngredient().getType().getStandardTemperature(), transpCont.getIngredient().getTemperature());
	}
	
	@Test
	public void testExtractStringQuantitySpecialName() {
		AlchemicTypeMixed waterBread = new AlchemicTypeMixed("Bread mixed with Water", States.POWDER, new Temperature(0,20), 0);
		waterBread.setSpecialName("Bad Meatloaf");
		AlchemicIngredient waterBreadLoaf = new AlchemicIngredient(waterBread, States.LIQUID, new Quantity(Quantity.SACHELAMOUNT), new Temperature(0,20));
		TransportContainer loafSack = new TransportContainer(waterBreadLoaf);
		fullLab.storeIngredient(loafSack);
		
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity waterQuantityStored = fullLab.getStoredQuantityOf("Bread mixed with Water");
		Quantity extractedQuantity = new Quantity(Quantity.SPOONAMOUNT);
		
		TransportContainer transpCont = fullLab.extract("Bad Meatloaf", extractedQuantity);
		
		assertTrue(transpCont.hasIngredient());
		assertTrue(fullLab.hasStored(transpCont.getIngredient().getType()));
		
		assertEquals(waterQuantityStored.subStract(extractedQuantity),fullLab.getStoredQuantityOf("Bad Meatloaf"));
		assertEquals(extractedQuantity, transpCont.getIngredient().getQuantity());
		
		Quantity expectedTotalQuantity = previousTotalQuantity.subStract(extractedQuantity);
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		// Check if the properties are the same as the expected storage properties
		assertEquals("Bread mixed with Water",transpCont.getIngredient().getType().getSimpleName());
		assertEquals(transpCont.getIngredient().getType().getStandardState(), transpCont.getIngredient().getState());
		assertEquals(transpCont.getIngredient().getType().getStandardTemperature(), transpCont.getIngredient().getTemperature());
	}
	
	@Test
	public void testExtractStringQuantityZeroAmount() {
		Quantity extractedQuantity = new Quantity(0);
		TransportContainer transpCont = fullLab.extract("Water", extractedQuantity);
		assertFalse(transpCont.hasIngredient());
	}
	
	@Test
	public void testExtractStringQuantityNullName() {
		Quantity extractedQuantity = new Quantity(15);
		TransportContainer transpCont = fullLab.extract(null, extractedQuantity);
		assertFalse(transpCont.hasIngredient());
	}
	
	@Test
	public void testExtractStringQuantityNullQuantity() {
		TransportContainer transpCont = fullLab.extract("Water", null);
		assertFalse(transpCont.hasIngredient());
	}
	
	@Test
	public void testExtractStringQuantityNoContainerExists() {
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity waterQuantityStored = fullLab.getStoredQuantityOf("Water");
		Quantity wantedQuantity = new Quantity(5*Quantity.BARRELAMOUNT);
		Quantity epectedQuantity = StdContainers.biggestContainerOf(States.LIQUID);
		TransportContainer transpCont = fullLab.extract("Water", wantedQuantity);
		
		assertTrue(transpCont.hasIngredient());
		assertTrue(fullLab.hasStored(transpCont.getIngredient().getType()));
		
		assertEquals(waterQuantityStored.subStract(epectedQuantity),fullLab.getStoredQuantityOf("Water"));
		assertEquals(epectedQuantity, transpCont.getIngredient().getQuantity());
		
		Quantity expectedTotalQuantity = previousTotalQuantity.subStract(epectedQuantity);
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		// Check if the properties are the same as the expected storage properties
		assertEquals("Water",transpCont.getIngredient().getType().getSimpleName());
		assertEquals(transpCont.getIngredient().getType().getStandardState(), transpCont.getIngredient().getState());
		assertEquals(transpCont.getIngredient().getType().getStandardTemperature(), transpCont.getIngredient().getTemperature());
	}
	
	@Test
	public void testExtractStringQuantityNotEnoughIngredient() {
		Quantity previousTotalQuantity = fullLab.getCurrentStorage();
		Quantity sugarQuantityStored = fullLab.getStoredQuantityOf("Sugar");
		Quantity wantedQuantity = new Quantity(sugarQuantityStored.getQuantity() + 1);
		TransportContainer transpCont = fullLab.extract("Sugar", wantedQuantity);
		
		assertTrue(transpCont.hasIngredient());
		assertFalse(fullLab.hasStored(transpCont.getIngredient().getType()));
		
		assertEquals(sugarQuantityStored.subStract(sugarQuantityStored),fullLab.getStoredQuantityOf("Sugar"));
		assertEquals(sugarQuantityStored, transpCont.getIngredient().getQuantity());
		
		Quantity expectedTotalQuantity = previousTotalQuantity.subStract(sugarQuantityStored);
		assertEquals(expectedTotalQuantity, fullLab.getCurrentStorage());
		// Check if the properties are the same as the expected storage properties
		assertEquals("Sugar",transpCont.getIngredient().getType().getSimpleName());
		assertEquals(transpCont.getIngredient().getType().getStandardState(), transpCont.getIngredient().getState());
		assertEquals(transpCont.getIngredient().getType().getStandardTemperature(), transpCont.getIngredient().getTemperature());
	}

	@Test
	public void testCanHaveTypeAt() {
		assertTrue(fullLab.canHaveTypeAt(bread, 1));
		assertTrue(emptyLab.canHaveTypeAt(bread, 1));
		assertFalse(fullLab.canHaveTypeAt(bread, 3));
		assertTrue(fullLab.canHaveTypeAt(new AlchemicTypeUnmixed("Xavier Steak",States.POWDER, new Temperature(20,0), 0.5), 4));
	}

	@Test
	public void testGetIterator() {
		StorageIterator it = fullLab.getIterator();
		assertEquals(fullLab.getNbOfStoredTypes(), it.getNbRemainingIngredients());
		for (int i = 1; i<fullLab.getNbOfStoredTypes();i++){
			assertEquals(fullLab.getDeviation(i), it.getVolatilityDeviation(), 0.01);
			assertEquals(fullLab.getQuantityAt(i), it.getQuantity());
			assertEquals(fullLab.getTypeAt(i), it.getCurrent());
			it.advance();
		}
	}

	@Test
	public void testHasCoolingBox() {
		assertTrue(fullLab.hasCoolingBox());
		assertFalse(emptyLab.hasCoolingBox());
	}
	
	@Test
	public void testHasOven() {
		assertTrue(fullLab.hasOven());
		assertFalse(emptyLab.hasOven());
	}

	@Test
	public void testHasKettle() {
		assertTrue(fullLab.hasKettle());
		assertFalse(emptyLab.hasKettle());
	}
	@Test
	public void testHasTransmogifier() {
		assertTrue(fullLab.hasTransmogifier());
		assertFalse(emptyLab.hasTransmogifier());
	}
}
