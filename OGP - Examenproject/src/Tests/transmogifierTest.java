package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Alchemy.*;

public class transmogifierTest {
	Laboratory emptyLab, fullLab;
	Transmogifier transmogifierNoLab, transmogifierTest, transmogifierInLab;
	AlchemicIngredient waterVial, waterBarrel;
	TransportContainer waterContainer, extractContainer;

	@Before
	public void setUp() throws Exception {
		emptyLab = new Laboratory(1);
		
		fullLab = new Laboratory(1);
		transmogifierInLab = new Transmogifier();
		transmogifierInLab.addTo(fullLab);
		
		transmogifierNoLab = new Transmogifier();
		
		waterVial = new AlchemicIngredient(new Quantity(Quantity.VIALAMOUNT));
		waterContainer = new TransportContainer(waterVial);
		
		transmogifierInLab.addIngredient(waterContainer);
		
		waterBarrel = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
		waterContainer = new TransportContainer(waterBarrel);
		
		extractContainer = new TransportContainer(StdContainers.CHEST);		
	}

	@Test
	public void testAddIngredient() {
		transmogifierInLab.addIngredient(waterContainer);
		assertTrue(waterVial.isTerminated());
		assertTrue(transmogifierInLab.getInputAt(1).hasIngredient());
		assertEquals(waterBarrel,transmogifierInLab.getInputAt(1).getIngredient());
	}

	@Test
	public void testProcess() {
		assertEquals(States.LIQUID, transmogifierInLab.getInputAt(1).getIngredient().getState());
		transmogifierInLab.process();
		assertEquals(States.POWDER, transmogifierInLab.getOutputAt(1).getIngredient().getState());
		transmogifierInLab.getOutputAt(1).getIngredient().move(transmogifierInLab.getInputAt(1));
		transmogifierInLab.process();
		assertEquals(States.LIQUID, transmogifierInLab.getOutputAt(1).getIngredient().getState());
		AlchemicIngredient ingr= transmogifierInLab.getOutputAt(1).getIngredient();
		transmogifierInLab.process();
		assertTrue(ingr.isTerminated());
		// Check that the output get cleared after each process
		assertFalse(transmogifierInLab.getOutputAt(1).hasIngredient());
	}

	@Test
	public void testAddTo() {
		transmogifierNoLab.addTo(emptyLab);
		assertEquals(emptyLab,transmogifierNoLab.getLaboratory());
		assertEquals(transmogifierNoLab,emptyLab.getTransmogifier());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasLab() {
		transmogifierInLab.addTo(emptyLab);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasCoolingBox() {
		transmogifierNoLab.addTo(fullLab);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddToNull() {
		transmogifierNoLab.addTo(null);
	}

	@Test
	public void testRemoveFromLab() {
		transmogifierInLab.removeFromLab();
		assertFalse(transmogifierInLab.hasLaboratory());
		assertFalse(fullLab.hasTransmogifier());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testRemoveFromLabNoLab() {
		transmogifierNoLab.removeFromLab();
	}

	@Test
	public void testTransmogifier() {
		transmogifierTest = new Transmogifier();
		assertEquals(transmogifierTest.getNbOfOutputs(), 0);
		assertEquals(transmogifierTest.getNbOfInputs(), 0);
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetOutputAt(){
		transmogifierInLab.getOutputAt(0);
	}

	@Test
	public void testGetNbOfInputs() {
		assertEquals(0, transmogifierNoLab.getNbOfInputs());
		assertEquals(1, transmogifierInLab.getNbOfInputs());
	}

	@Test
	public void testFindFirstEmpty() {
		assertEquals(1,transmogifierNoLab.findFirstEmpty());
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetInputAt(){
		transmogifierInLab.getOutputAt(0);
	}

	@Test
	public void testGetNbOfOutputs() {
		assertEquals(0, transmogifierNoLab.getNbOfOutputs());
		transmogifierInLab.process();
		assertEquals(1, transmogifierInLab.getNbOfOutputs());
	}

	@Test
	public void testExtractIngredient() {
		transmogifierInLab.process();
		AlchemicIngredient ingr = transmogifierInLab.getOutputAt(1).getIngredient();
		transmogifierInLab.extractIngredient(extractContainer, 1);
		assertFalse(transmogifierInLab.getOutputAt(1).hasIngredient());
		assertTrue(ingr.hasContainer());
		extractContainer.hasIngredient();
	}
	
	@Test
	public void testHasLaboratory() {
		assertFalse(transmogifierNoLab.hasLaboratory());
		assertTrue(transmogifierInLab.hasLaboratory());
	}

}
