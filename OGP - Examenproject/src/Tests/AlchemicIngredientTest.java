package Tests;

import static org.junit.Assert.*;

import Alchemy.*;

import org.junit.Before;
import org.junit.Test;

public class AlchemicIngredientTest {
	
	
	AlchemicType alch1,alch2,alch3;
	AlchemicIngredient ingredient1, ingredient2, ingredient3,ingredient4,water, terminated, unfilled, illegalname, legalname;
	Temperature standard, zero, maximum, minimum, temp;
	States	liquid, powder;
	Quantity empty,spoon,pinches, drops;
	TransportContainer container1,container2,terminatedcontainer;

	@Before
	public void setUp() throws Exception {
		standard=new Temperature(0,20);
		temp= new Temperature(0,50);
		zero=new Temperature(0,0);
		maximum=new Temperature(0,Temperature.getMaximumTemperature());
		minimum=new Temperature(Temperature.getMaximumTemperature(),0);
		liquid=States.LIQUID;
		powder=States.POWDER;
		empty=new Quantity(0);
		spoon=new Quantity(24);
		pinches=new Quantity(18);
		drops=new Quantity(32);
		alch1=new AlchemicTypeUnmixed("Test",powder,zero,.5);
		alch2=new AlchemicTypeMixed("Mercuric Acid mixed with Name, Something and Test", liquid,maximum,1,"Special");
		alch3=new AlchemicTypeUnmixed("Something",powder,temp,.75);
		ingredient1=new AlchemicIngredient(alch1,liquid,spoon,standard);
		ingredient2=new AlchemicIngredient(alch2,liquid,drops,maximum);
		ingredient3=new AlchemicIngredient(alch3,powder,pinches,minimum);
		water=new AlchemicIngredient(spoon);
		terminated=new AlchemicIngredient(alch3,powder,pinches,minimum);
		legalname=new AlchemicIngredient("Name", liquid,temp,0);
		terminated.terminate();
		container1=new TransportContainer(StdContainers.SPOON);
		container2=new TransportContainer(StdContainers.SPOON);
		terminatedcontainer=new TransportContainer(StdContainers.SPOON);
		terminatedcontainer.terminate();
		ingredient4=new AlchemicIngredient(alch1,liquid,spoon,standard,container1);
	}

	@Test
	public void testAlchemicIngredientAlchemicTypeStatesQuantityTemperature() {
		assertEquals(ingredient1.getType(),alch1);
		assertEquals(ingredient1.getState(),liquid);
		assertEquals(ingredient1.getQuantity(),spoon);
		assertEquals(ingredient1.getTemperature(),standard);
		assertTrue(ingredient1.getVolatilityDeviation()<=.1&&ingredient1.getVolatilityDeviation()>=-.1);
		assertEquals(ingredient2.getType(),alch2);
		assertEquals(ingredient2.getState(),liquid);
		assertEquals(ingredient2.getQuantity(),drops);
		assertEquals(ingredient2.getTemperature(),maximum);
		assertTrue(ingredient2.getVolatilityDeviation()<=.1&&ingredient2.getVolatilityDeviation()>=-.1);
		
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientAlchemicTypeStatesQuantityTemperatureIllegalCase()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(alch3,powder,empty,temp);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientAlchemicTypeStatesQuantityTemperatureQuantityNull()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(alch3,powder,null,temp);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientAlchemicTypeStatesQuantityTemperatureTypeNull()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(null,powder,spoon,temp);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientAlchemicTypeStatesQuantityTemperatureStateNull()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(alch3,null,spoon,temp);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientAlchemicTypeStatesQuantityTemperatureTemperatureNull()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(null,powder,empty,null);
	}	

	@Test
	public void testAlchemicIngredientStringStatesTemperatureDouble() {
		assertEquals(legalname.getType().getSimpleName(),"Name");
		assertEquals(legalname.getType().getStandardState(),liquid);
		assertTrue(legalname.getType().getStandardTemperature().equals(temp));
		assertTrue(legalname.getType().getTheoreticVolatility()==0);
		assertEquals(legalname.getState(),liquid);
		assertTrue(legalname.getTemperature().equals(temp));
		assertTrue(legalname.getQuantity().equals(spoon));
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientStringStatesTemperatureDoubleIllegalCase()throws IllegalArgumentException{
		illegalname=new AlchemicIngredient("name",liquid,temp,0);
	}

	@Test
	public void testAlchemicIngredientAlchemicTypeStatesQuantityTemperatureIngredientContainer() {
		assertEquals(ingredient4.getType(),alch1);
		assertEquals(ingredient4.getState(),liquid);
		assertEquals(ingredient4.getQuantity(),spoon);
		assertEquals(ingredient4.getTemperature(),standard);
		assertEquals(ingredient4.getContainer(),container1);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientStringStatesTemperatureDoubleContainerIllegalCaseTerminated()throws IllegalArgumentException{
		illegalname=new AlchemicIngredient(alch1,liquid,spoon,standard,terminatedcontainer);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientStringStatesTemperatureDoubleContainerIllegalState()throws IllegalArgumentException{
		illegalname=new AlchemicIngredient(alch1,liquid,spoon,standard,new TransportContainer(StdContainers.SACHEL));
	}
	@Test
	public void testAlchemicIngredientQuantity() {
		assertEquals(water.getType().getSimpleName(),"Water");
		assertEquals(water.getType().getStandardState(),liquid);
		assertTrue(water.getType().getStandardTemperature().equals(standard));
		assertTrue(water.getType().getTheoreticVolatility()==0);
		assertEquals(water.getState(),liquid);
		assertEquals(water.getQuantity(),spoon);
		assertTrue(water.getTemperature().equals(standard));
	}
	
	@Test
	public void testAlchemicIngredientTypeQuantityDouble(){
		AlchemicIngredient ing = new AlchemicIngredient(water.getType(), new Quantity(Quantity.BARRELAMOUNT), 0.05);
		assertEquals(ing.getVolatilityDeviation(), 0.05, 0.001);
		assertEquals(ing.getType(), water.getType());
		assertEquals(ing.getQuantity().getQuantity(), Quantity.BARRELAMOUNT);
		assertEquals(ing.getTemperature(), water.getType().getStandardTemperature());
		assertEquals(ing.getState(), water.getType().getStandardState());
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientTypeQuantityDoubleIllegalCase()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(alch3,empty,0);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientTypeQuantityDoubleQuantityNull()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(alch3,powder,null,temp);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testAlchemicIngredientTypeQuantityDoubleTypeNull()throws IllegalArgumentException{
		unfilled=new AlchemicIngredient(null,spoon,0);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testAlchemicIngredientTypeQuantityVolatilityZeroQuantity(){
		new AlchemicIngredient(water.getType(), new Quantity(0), 0.5);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAlchemicIngredientTypeQuantityVolatilityIllegalDeviation(){
		new AlchemicIngredient(water.getType(), new Quantity(0), -0.5);
	}
	
	@Test 
	public void testIsValidType(){
		assertTrue(AlchemicIngredient.isValidType(new AlchemicTypeUnmixed()));
		assertFalse(AlchemicIngredient.isValidType(null));
	}
	
	@Test
	public void testCanHaveAsQuantity(){
		assertFalse(ingredient1.canHaveAsQuantity(new Quantity(0)));
		assertTrue(ingredient1.canHaveAsQuantity(new Quantity(1)));
		assertFalse(ingredient1.canHaveAsQuantity(null));
		assertTrue(terminated.canHaveAsQuantity(null));
		assertFalse(terminated.canHaveAsQuantity(new Quantity(50)));
	}
	
	@Test
	public void testCanHaveAsTemperature(){
		assertTrue(ingredient1.canHaveAsTemperature(zero));
		assertFalse(ingredient1.canHaveAsTemperature(null));
		assertFalse(terminated.canHaveAsTemperature(zero));
		assertTrue(terminated.canHaveAsTemperature(null));
	}
	
	@Test
	public void testIsValidVolatilityDeviation(){
		assertTrue(AlchemicIngredient.isValidVolatilityDeviation(0));
		assertFalse(AlchemicIngredient.isValidVolatilityDeviation(-0.11));
		assertFalse(AlchemicIngredient.isValidVolatilityDeviation(0.11));
	}
	
	@Test
	public void TestCanHaveAsState(){
		assertTrue(ingredient1.canHaveAsState(States.LIQUID));
		assertFalse(ingredient1.canHaveAsState(null));
		assertTrue(terminated.canHaveAsState(null));
		assertFalse(terminated.canHaveAsState(States.LIQUID));
	}

	@Test
	public void testHasContainer() {
		assertTrue(ingredient4.hasContainer());
		assertFalse(ingredient1.hasContainer());
	}
	
	@Test
	public void testCanHaveAsContainer() {
		IngredientContainer container3=new TransportContainer(StdContainers.VIAL);
		AlchemicIngredient ingredient5=new AlchemicIngredient(new Quantity(25));
		assertFalse(terminated.canHaveAsContainer(container1));
		assertTrue(ingredient1.canHaveAsContainer(container3));
		assertFalse(ingredient5.canHaveAsContainer(container2));
		assertFalse(ingredient3.canHaveAsContainer(container3));
	}
	
	@Test
	public void testHasPropperContainer() {
		assertTrue(ingredient4.hasPropperContainer());
		assertTrue(ingredient3.hasPropperContainer());
	}

	@Test
	public void testMove() {
		ingredient4.move(container2);
		assertEquals(container2,ingredient4.getContainer());
		container2.empty();
		ingredient3.move(container2);
		assertEquals(container2,ingredient3.getContainer());
	}
	@Test(expected=IllegalArgumentException.class)
	public void testMoveNull()throws IllegalArgumentException{
		ingredient4.move(null);
	}
	@Test(expected=IllegalStateException.class)
	public void testMoveIllegalState()throws IllegalStateException{
		ingredient3.move(container1);
	}
	@Test(expected=IllegalStateException.class)
	public void testMoveIllegalIngredient() throws IllegalStateException{
		terminated.move(container1);
	}

	@Test
	public void testChangeState() {
		ingredient1.changeState();
		assertEquals(ingredient1.getState(),powder);
		ingredient3.changeState();
		assertEquals(ingredient3.getState(),liquid);
	}

	@Test
	public void testSetQuantity() {
		AlchemicIngredient ingredient=ingredient1.setQuantity(drops);
		assertEquals(ingredient.getType(),alch1);
		assertEquals(ingredient.getState(),liquid);
		assertEquals(ingredient.getQuantity(),drops);
		assertEquals(ingredient.getTemperature(),standard);
		assertTrue(ingredient.getVolatilityDeviation()<=.1&&ingredient1.getVolatilityDeviation()>=-.1);
		
	}

	@Test
	public void testSetTemperature() {
		ingredient1.setTemperature(zero);
		assertTrue(ingredient1.getTemperature().equals(zero));
	}
	
	@Test
	public void testGetEffectiveVolatilityDeviation() {
		assertTrue(ingredient2.getEffectiveVolatilityDeviation()<=0&&ingredient2.getEffectiveVolatilityDeviation()>=-.1);
		assertTrue(water.getEffectiveVolatilityDeviation()==0);
		assertTrue(Math.abs(ingredient1.getEffectiveVolatilityDeviation())<=.05);
		assertTrue(Math.abs(ingredient3.getEffectiveVolatilityDeviation())<=.075);
	}

	@Test
	public void testGetIntrinsicVolatility() {
		assertTrue(ingredient2.getIntrinsicVolatility()>=.9);
		assertTrue(water.getIntrinsicVolatility()==0);
		assertTrue(ingredient1.getIntrinsicVolatility()>=.45&&ingredient1.getIntrinsicVolatility()<=.55);
	}
	
	@Test
	public void testGetVolatilityAtStandardTemperature() {
		assertTrue(ingredient1.getVolatilityAtStandardTemperature()==0);
		assertTrue(ingredient3.getVolatilityAtStandardTemperature()==10*ingredient3.getIntrinsicVolatility()*50);
		AlchemicIngredient ingredient5=new AlchemicIngredient("Test",liquid,minimum,0);
		assertTrue(ingredient5.getVolatilityAtStandardTemperature()==0);
	}
	@Test
	public void testGetEffectiveVolatility() {
		assertTrue(ingredient1.getEffectiveVolatility()==Double.MAX_VALUE);
		assertTrue(ingredient2.getEffectiveVolatility()==ingredient2.getVolatilityAtStandardTemperature());
		Temperature standard3=ingredient3.getType().getStandardTemperature();
		assertTrue(ingredient3.getEffectiveVolatility()==ingredient3.getIntrinsicVolatility()*ingredient3.getVolatilityAtStandardTemperature()*(standard3.compare(ingredient3.getTemperature())/(standard3.getHotness()+standard3.getColdness())));
		AlchemicIngredient voltest=new AlchemicIngredient(water.getType(),liquid,spoon,temp);
		assertTrue(voltest.getEffectiveVolatility()==voltest.getVolatilityAtStandardTemperature()+300*(20/30));
		Temperature coldstandard=new Temperature(20,0);
		Temperature coldtemp=new Temperature(50,0);
		AlchemicIngredient coldvoltest= new AlchemicIngredient(new AlchemicTypeUnmixed("Test Volatility",powder,coldstandard,0),powder,spoon,coldtemp);
		assertTrue(coldvoltest.getEffectiveVolatility()==0);
		coldvoltest.setTemperature(new Temperature(10,0));
		assertTrue(coldvoltest.getEffectiveVolatility()==coldvoltest.getIntrinsicVolatility()*300*.5);
		coldvoltest.setTemperature(temp);
		assertTrue(coldvoltest.getEffectiveVolatility()==coldvoltest.getIntrinsicVolatility()*300*(7/2));
	}

	@Test
	public void testGetCompleteName() {
		assertEquals(ingredient1.getCompleteName(),"Volatile Heated "+ingredient1.toString());
		assertEquals(ingredient2.getCompleteName(),"Volatile "+ingredient2.toString()+" Danger");
		ingredient2.setTemperature(new Temperature(0,10));
		assertEquals(ingredient2.getCompleteName(),"Inert Cooled "+ingredient2.toString()+" Danger");
		assertEquals(ingredient3.getCompleteName(),"Volatile Cooled "+ingredient3.toString());
		assertEquals(water.getCompleteName(),water.toString());
	}

	@Test
	public void testIsTerminated() {
		assertTrue(terminated.isTerminated());
		assertFalse(ingredient1.isTerminated());
	}

	@Test
	public void testTerminate() {
		ingredient4.terminate();
		assertTrue(ingredient4.isTerminated());
		assertEquals(ingredient4.getTemperature(),null);
		assertEquals(ingredient4.getQuantity(),null);
		assertEquals(ingredient4.getContainer(),null);
		assertEquals(container1.getIngredient(),null);
		
	}

	@Test
	public void testToString() {
		assertEquals(ingredient2.toString(),"Mercuric Acid mixed with Name, Something and Test, Special");
		assertEquals(water.toString(),"Water");
		assertEquals(ingredient1.toString(),"Test");
	}

}
