package Tests;
import Alchemy.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RecipeBookTest {
	RecipeBook emptyBook, fullBook, recipeBookTest;
	Recipe rec1, rec2,rec3, rec4;

	@Before
	public void setUp() throws Exception {
		emptyBook = new RecipeBook();
		fullBook = new RecipeBook();
		rec1 = new Recipe(fullBook);
		rec2 = new Recipe(fullBook);
		rec3 = new Recipe(fullBook);
	}

	@Test
	public void testRecipeBook() {
		recipeBookTest = new RecipeBook();
		assertEquals(recipeBookTest.getNbOfPages(), 0);
	}

	@Test
	public void testGetNbOfPages() {
		assertEquals(emptyBook.getNbOfPages(),0);
		assertEquals(fullBook.getNbOfPages(),3);
		rec1.moveRecipe(emptyBook);
		assertEquals(fullBook.getNbOfPages(),3);
		assertEquals(emptyBook.getNbOfPages(),1);
	}

	@Test
	public void testGetRecipeAt() {
		assertEquals(fullBook.getRecipeAt(1), rec1);
		assertEquals(fullBook.getRecipeAt(2), rec2);
		assertEquals(fullBook.getRecipeAt(3), rec3);
		rec1.moveRecipe(emptyBook);
		assertEquals(fullBook.getRecipeAt(1), null);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetRecipeAtOutOfBounds() {
		fullBook.getRecipeAt(0);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testRemoveRecipeAtIllegalArgument() {
		fullBook.removeRecipeAt(1);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testRemoveRecipeAtOutOfBounds() {
		fullBook.removeRecipeAt(0);
	}

	@Test 
	public void testGetPageOf() {
		assertEquals(fullBook.getPageOf(rec1), 1);
		rec1.moveRecipe(emptyBook);
		assertEquals(fullBook.getPageOf(rec1), -1);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testRemoveRecipeInvalidState() {
		fullBook.removeRecipe(rec1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testRemoveRecipeNotInBook() {
		rec1.moveRecipe(emptyBook);
		fullBook.removeRecipe(rec1);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testAddRecipeIllegalArgument() {
		emptyBook.addRecipe(rec1);
	}

	@Test
	public void testHasAsRecipe() {
		assertTrue(fullBook.hasAsRecipe(rec1));
		rec1.moveRecipe(emptyBook);
		assertFalse(fullBook.hasAsRecipe(rec1));
	}

}
