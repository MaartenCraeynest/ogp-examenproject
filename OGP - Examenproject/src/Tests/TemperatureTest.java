package Tests;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Alchemy.Temperature;


public class TemperatureTest {
	Temperature temp;
	Temperature maxtemp;
	Temperature negtemp;
	Temperature mintemp;
	Temperature coldtemp;
	Temperature multitemp;
	Temperature hottemp;
	

	@Before
	public void setUp() throws Exception {
	temp=new Temperature((long)0,(long)100);
	maxtemp=new Temperature((long)0,Temperature.getMaximumTemperature());
	negtemp=new Temperature((long)-50,(long)0);
	mintemp=new Temperature(Temperature.getMaximumTemperature(),(long)0);
	coldtemp=new Temperature((long)100,(long)0);
	multitemp=new Temperature((long)50,(long)25);
	hottemp=new Temperature((long)30,(long)150);
	}
	@Test
	public void testTemperatureLongLong(){
		assertEquals(temp.getColdness(),0);
		assertEquals(temp.getHotness(),100);
		assertEquals(negtemp.getColdness(),50);
		assertEquals(Temperature.getMaximumTemperature(),maxtemp.getHotness());
		assertEquals(Temperature.getMaximumTemperature(),mintemp.getColdness());
		assertEquals(coldtemp.getColdness(),(long)100);
		assertEquals(multitemp.getColdness(),(long)25);
		assertEquals(multitemp.getHotness(),(long)0);
		assertEquals(hottemp.getHotness(),(long)120);
		assertEquals(hottemp.getColdness(),(long)0);
	}
	@Test
	public void testGetColdness() {
		assertEquals(coldtemp.getColdness(),100);
		assertEquals(mintemp.getColdness(),Temperature.getMaximumTemperature());
		assertEquals(hottemp.getColdness(),0);
	}

	@Test
	public void testGetHotness() {
		assertEquals(coldtemp.getHotness(),0);
		assertEquals(maxtemp.getHotness(),Temperature.getMaximumTemperature());
		assertEquals(hottemp.getHotness(),120);
	}

	@Test
	public void testHeat() {
		hottemp=hottemp.heat(50);
		assertEquals(hottemp.getHotness(),170);
		coldtemp=coldtemp.heat(120);
		assertEquals(coldtemp.getHotness(),20);
		assertEquals(coldtemp.getColdness(),0);
		maxtemp=maxtemp.heat(Long.MAX_VALUE);
		assertEquals(maxtemp.getHotness(),Temperature.getMaximumTemperature());
		maxtemp=maxtemp.heat(50);
		assertEquals(maxtemp.getHotness(),Temperature.getMaximumTemperature());
	}

	@Test
	public void testCool() {
		hottemp=hottemp.cool(170);
		assertEquals(hottemp.getColdness(),50);
		assertEquals(coldtemp.getHotness(),0);
		coldtemp=coldtemp.cool(120);
		assertEquals(coldtemp.getColdness(),220);
		mintemp=mintemp.cool(50);
		assertEquals(mintemp.getColdness(),Temperature.getMaximumTemperature());
	}

	@Test
	public void testIsValidTemperature() {
		assertTrue(Temperature.isValidTemperature(0));
		assertTrue(Temperature.isValidTemperature(100));
		assertFalse(Temperature.isValidTemperature(Long.MAX_VALUE));
	}
	@Test
	public void testCompare(){
		assertEquals(hottemp.compare(coldtemp),220);
		assertEquals(hottemp.compare(negtemp),170);
		assertEquals(coldtemp.compare(hottemp),-220);
		assertEquals(coldtemp.compare(multitemp),-75);
		assertEquals(multitemp.compare(coldtemp),75);
		assertEquals(temp.compare(hottemp),-20);
		assertEquals(hottemp.compare(temp),20);
		if(Temperature.getMaximumTemperature()==Long.MAX_VALUE){
			assertEquals(maxtemp.compare(mintemp),Long.MAX_VALUE);
			assertEquals(mintemp.compare(maxtemp),Long.MIN_VALUE);
		}
	}
	@Test
	public void testIsGreaterThan(){
		assertTrue(hottemp.isGreaterThan(coldtemp));
		assertFalse(hottemp.isGreaterThan(hottemp));
		assertFalse(mintemp.isGreaterThan(coldtemp));
		assertTrue(maxtemp.isGreaterThan(hottemp));
	}
	
	@Test
	public void testEquals(){
		assertFalse(hottemp.equals(null));
		assertFalse(hottemp.equals(new Object()));
		assertTrue(hottemp.equals(hottemp));
		assertFalse(hottemp.equals(coldtemp));
		assertTrue(mintemp.equals(mintemp));
		assertFalse(mintemp.equals(coldtemp));
		assertTrue(maxtemp.equals(maxtemp));
		assertFalse(maxtemp.equals(hottemp));
	}
	
	@Test 
	public void testHashCode(){
		assertEquals(Long.hashCode(hottemp.getHotness() - hottemp.getColdness()),
					 hottemp.hashCode());
	}

}