package Tests;
import Alchemy.Quantity;
import Alchemy.Recipe;
import Alchemy.RecipeBook;
import Alchemy.RecipeStep;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RecipeTest {
	RecipeBook defaultBook, emptyBook;
	Recipe emptyRecipe, recipeRecipeBookTest, fullRecipe;
	@Before
	public void setUp() throws Exception {
		defaultBook = new RecipeBook();
		emptyBook = new RecipeBook();
		
		emptyRecipe = new Recipe(defaultBook);
		
		fullRecipe = new Recipe(defaultBook);
		fullRecipe.addRecipeStepAt(RecipeStep.COOL, fullRecipe.getNbOfSteps()+1);
		fullRecipe.addRecipeStepAt(RecipeStep.HEAT, fullRecipe.getNbOfSteps()+1);
		fullRecipe.addRecipeStepAt(RecipeStep.MIX, fullRecipe.getNbOfSteps()+1);
		fullRecipe.addIngredientToRecipeAt("7 Vials Water", fullRecipe.getNbOfSteps()+1);
	}

	

	@Test
	public void testRecipeRecipeBook() {
		recipeRecipeBookTest = new Recipe(emptyBook);
		assertEquals(recipeRecipeBookTest.getNbOfIngredients(), 0);
		assertEquals(recipeRecipeBookTest.getNbOfSteps(), 0);
		assertEquals(recipeRecipeBookTest.getRecipeBook(), emptyBook);
		assertTrue(emptyBook.hasAsRecipe(recipeRecipeBookTest));
		assertTrue(recipeRecipeBookTest.hasValidAmountOfIngredients());
		assertTrue(recipeRecipeBookTest.hasValidRecipeBook());
		assertEquals(emptyBook.getRecipeAt(1), recipeRecipeBookTest);
	}

	@Test
	public void testHasValidRecipeBook() {
		assertTrue(emptyRecipe.hasValidRecipeBook());
	}

	@Test
	public void testGetRecipeCommandAt() {
		assertEquals(fullRecipe.getRecipeCommandAt(1), RecipeStep.COOL);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetRecipeCommandAtOutOfBounds() {
		fullRecipe.getRecipeCommandAt(0);
	}

	@Test
	public void testGetIngredientAt() {
		assertEquals(fullRecipe.getIngredientAt(1), "7 Vials Water");
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetIngredientAtOutOfBounds() {
		fullRecipe.getIngredientAt(0);
	}

	@Test
	public void testGetNbOfIngredients() {
		assertEquals(fullRecipe.getNbOfIngredients(), 1);
		assertEquals(emptyRecipe.getNbOfIngredients(), 0);
	}

	@Test
	public void testHasValidAmountOfIngredients() {
		assertTrue(fullRecipe.hasValidAmountOfIngredients());
	}

	@Test
	public void testIsValidIngredient() {
		assertTrue(Recipe.isValidIngredient("1 Drop IngredientX"));
		assertTrue(Recipe.isValidIngredient("99 Boxes Luftballons"));
		assertTrue(Recipe.isValidIngredient("1 Spoon Bad Tasting Syrup"));
		assertFalse(Recipe.isValidIngredient("4 8 15 16 23 42 Pinches Lost"));
		assertFalse(Recipe.isValidIngredient("3.1415 Boxes Halfa Tau"));
		assertFalse(Recipe.isValidIngredient("0 Barrel Rolls"));
		assertFalse(Recipe.isValidIngredient("300 spoons Fus"));
		assertFalse(Recipe.isValidIngredient("50 Sacks ro"));
		assertFalse(Recipe.isValidIngredient("Drop Dah"));
		
		assertTrue(Recipe.isValidIngredient("5 Boxes Of Phrasing"));
		assertTrue(Recipe.isValidIngredient("500 Storerooms Easer Eggs"));
		assertFalse(Recipe.isValidIngredient("0101010 Boxes Of The Answer To Everything"));
		assertTrue(Recipe.isValidIngredient("50 Vials Of The Force"));
		assertFalse(Recipe.isValidIngredient("The Force 50 Vials Of"));
	}

	@Test
	public void testAddRecipeStepAt() {
		fullRecipe.addRecipeStepAt(RecipeStep.COOL, 2);
		assertEquals(fullRecipe.getRecipeCommandAt(1), RecipeStep.COOL);
		assertEquals(fullRecipe.getRecipeCommandAt(2), RecipeStep.COOL);
		assertEquals(fullRecipe.getRecipeCommandAt(3), RecipeStep.HEAT);
		assertEquals(fullRecipe.getRecipeCommandAt(4), RecipeStep.MIX);
		assertEquals(fullRecipe.getRecipeCommandAt(5), RecipeStep.ADD);
		assertEquals(fullRecipe.getNbOfSteps(), 5);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testAddRecipeStepAtOutOfBounds() {
		fullRecipe.addRecipeStepAt(RecipeStep.COOL, fullRecipe.getNbOfSteps()+2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddRecipeStepAtIllegalArgument() {
		fullRecipe.addRecipeStepAt(RecipeStep.ADD, fullRecipe.getNbOfSteps());
	}

	@Test
	public void testAddIngredientToRecipeAt() {
		fullRecipe.addIngredientToRecipeAt("1 Box Chocolates Never Know What You Will Find", 2);
		assertEquals(fullRecipe.getRecipeCommandAt(1), RecipeStep.COOL);
		assertEquals(fullRecipe.getRecipeCommandAt(2), RecipeStep.ADD);
		assertEquals(fullRecipe.getRecipeCommandAt(3), RecipeStep.HEAT);
		assertEquals(fullRecipe.getRecipeCommandAt(4), RecipeStep.MIX);
		assertEquals(fullRecipe.getRecipeCommandAt(5), RecipeStep.ADD);
		assertEquals(fullRecipe.getNbOfSteps(), 5);
		assertEquals(fullRecipe.getNbOfIngredients(), 2);
		assertEquals(fullRecipe.getIngredientAt(1), "1 Box Chocolates Never Know What You Will Find");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddIngredientToRecipeAtIllegalArgument() {
		fullRecipe.addIngredientToRecipeAt("Infinite Pinches Of Posibilities", 2);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testAddIngredientToRecipeAtOutOfBounds() {
		fullRecipe.addIngredientToRecipeAt("1 Chest Straw", 0);
	}
	

	@Test
	public void testRemoveStepAt() {
		fullRecipe.removeStepAt(4);
		assertEquals(fullRecipe.getRecipeCommandAt(1), RecipeStep.COOL);
		assertEquals(fullRecipe.getRecipeCommandAt(2), RecipeStep.HEAT);
		assertEquals(fullRecipe.getRecipeCommandAt(3), RecipeStep.MIX);
		assertEquals(fullRecipe.getNbOfSteps(), 3);
		assertEquals(fullRecipe.getNbOfIngredients(), 0);
		
		fullRecipe.removeStepAt(1);
		assertEquals(fullRecipe.getRecipeCommandAt(1), RecipeStep.HEAT);
		assertEquals(fullRecipe.getRecipeCommandAt(2), RecipeStep.MIX);
		assertEquals(fullRecipe.getNbOfSteps(), 2);
		assertEquals(fullRecipe.getNbOfIngredients(), 0);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testRemoveStepAtIndexOutOfBounds() {
		fullRecipe.removeStepAt(0);
	}

	@Test
	public void testMoveRecipe() {
		emptyRecipe.moveRecipe(emptyBook);
		assertEquals(emptyRecipe.getRecipeBook(), emptyBook);
		assertFalse(defaultBook.hasAsRecipe(emptyRecipe));
		assertTrue(emptyBook.hasAsRecipe(emptyRecipe));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testMoveRecipeIllegalArgument() {
		emptyRecipe.moveRecipe(null);
	}
	
	@Test
	public void testExtractName(){
		assertEquals(null, Recipe.extractName(null));
		assertEquals(null, Recipe.extractName("IllegalString"));
		assertEquals("Kechup", Recipe.extractName("1 Bottle Kechup"));
	}
	
	@Test
	public void testExtractQuantity(){
		assertEquals(null, Recipe.extractQuantity(null));
		assertEquals(null, Recipe.extractQuantity("IllegalString"));
		assertEquals(new Quantity(5*Quantity.BOTTLEAMOUNT).getQuantity(), 
					Recipe.extractQuantity("5 Bottle Kechup").getQuantity());
	}


}
