package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import Alchemy.*;

public class ExecuteRecipeTest {
	AlchemicTypeUnmixed coffee, milk;
	AlchemicIngredient waterBarrel, coffeeBarrel, milkSpoon;
	Laboratory emptyLabHasIngredients;
	Laboratory fullLabNoIngredients;
	Laboratory fullLabIngredients;
	RecipeBook recipeBook;
	Recipe emptyRecipe;
	
	@Before
	public void setUp() throws Exception {
		emptyLabHasIngredients = new Laboratory(5);
		coffee = new AlchemicTypeUnmixed("Coffee", States.LIQUID, new Temperature(0,30), 0.5);
		milk = new AlchemicTypeUnmixed("Milk", States.LIQUID, new Temperature(0, 5), 0.1);
		
		waterBarrel = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
		coffeeBarrel = new AlchemicIngredient(coffee,
												States.LIQUID,
												new Quantity(Quantity.BARRELAMOUNT),
												new Temperature(0,30));

		milkSpoon = new AlchemicIngredient(milk,
											States.LIQUID,
											new Quantity(Quantity.SPOONAMOUNT),
											new Temperature(0,5));
		emptyLabHasIngredients.storeIngredient(new TransportContainer(waterBarrel));
		emptyLabHasIngredients.storeIngredient(new TransportContainer(coffeeBarrel));
		emptyLabHasIngredients.storeIngredient(new TransportContainer(milkSpoon));
		
		fullLabNoIngredients = new Laboratory(5);
		new Kettle().addTo(fullLabNoIngredients);
		new CoolingBox(new Temperature(0,0)).addTo(fullLabNoIngredients);
		new Transmogifier().addTo(fullLabNoIngredients);
		new Oven(new Temperature(0,100)).addTo(fullLabNoIngredients);
		
		
		fullLabIngredients = new Laboratory(5);
		
		coffee = new AlchemicTypeUnmixed("Coffee", States.LIQUID, new Temperature(0,30), 0.5);
		milk = new AlchemicTypeUnmixed("Milk", States.LIQUID, new Temperature(0, 5), 0.1);
		
		waterBarrel = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
		coffeeBarrel = new AlchemicIngredient(coffee,
												States.LIQUID,
												new Quantity(Quantity.BARRELAMOUNT),
												new Temperature(0,30));

		milkSpoon = new AlchemicIngredient(milk,
											States.LIQUID,
											new Quantity(Quantity.SPOONAMOUNT),
											new Temperature(0,5));
		
		fullLabIngredients.storeIngredient(new TransportContainer(waterBarrel));
		fullLabIngredients.storeIngredient(new TransportContainer(coffeeBarrel));
		fullLabIngredients.storeIngredient(new TransportContainer(milkSpoon));
		new Kettle().addTo(fullLabIngredients);
		new CoolingBox(new Temperature(0,0)).addTo(fullLabIngredients);
		new Transmogifier().addTo(fullLabIngredients);
		new Oven(new Temperature(0,100)).addTo(fullLabIngredients);
		
		recipeBook = new RecipeBook();
		emptyRecipe = new Recipe(recipeBook);
	}
	
	@After
	public void checkInvars(){
		assertTrue(fullLabIngredients.canHaveAsCurrentStorage());
		assertTrue(emptyLabHasIngredients.canHaveAsCurrentStorage());
		assertTrue(fullLabNoIngredients.canHaveAsCurrentStorage());
	}
	
	@Test
	public void testEmptyRecipe(){
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertTrue(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertTrue(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testMixOnlyNoIngredient(){
		emptyRecipe.addRecipeStepAt(RecipeStep.MIX, 1);
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertTrue(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testHeatOnlyNoIngredient(){
		emptyRecipe.addRecipeStepAt(RecipeStep.HEAT, 1);
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertTrue(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testCoolOnlyNoIngredient(){
		emptyRecipe.addRecipeStepAt(RecipeStep.COOL, 1);
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertTrue(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testAddOnly(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertTrue(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testMixOnlyOnIngredient(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		emptyRecipe.addRecipeStepAt(RecipeStep.MIX, 2);
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testHeatOnlyOnIngredient(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		emptyRecipe.addRecipeStepAt(RecipeStep.HEAT, 2);
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testCoolOnlyOnIngredient(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		emptyRecipe.addRecipeStepAt(RecipeStep.COOL, 2);
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
	}
	
	@Test
	public void testMixOnlyTwoIngredients(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Coffee", 2);
		emptyRecipe.addRecipeStepAt(RecipeStep.MIX, 3);	
		
		Quantity coffeeBefore = fullLabIngredients.getStoredQuantityOf("Coffee");
		Quantity waterBefore = fullLabIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(waterBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(2*Quantity.SPOONAMOUNT), 
				fullLabIngredients.getStoredQuantityOf("Coffee mixed with Water"));
		
		coffeeBefore = emptyLabHasIngredients.getStoredQuantityOf("Coffee");
		waterBefore = emptyLabHasIngredients.getStoredQuantityOf("Water");
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(waterBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				emptyLabHasIngredients.getStoredQuantityOf("Coffee mixed with Water"));
		
		coffeeBefore = fullLabNoIngredients.getStoredQuantityOf("Coffee");
		waterBefore = fullLabNoIngredients.getStoredQuantityOf("Water");
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				fullLabNoIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(waterBefore,
				fullLabNoIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				fullLabNoIngredients.getStoredQuantityOf("Coffee mixed with Water"));
	}
	
	@Test
	public void testMixManualthreeIngredients(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Coffee", 2);
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Milk", 3);
		emptyRecipe.addRecipeStepAt(RecipeStep.MIX, 4);
		
		Quantity coffeeBefore = fullLabIngredients.getStoredQuantityOf("Coffee");
		Quantity waterBefore = fullLabIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		Quantity milkBefore = fullLabIngredients.getStoredQuantityOf("Milk");
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(milkBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Milk"));
		assertEquals(waterBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(3*Quantity.SPOONAMOUNT), 
				fullLabIngredients.getStoredQuantityOf("Coffee mixed with Milk and Water"));
		
		coffeeBefore = emptyLabHasIngredients.getStoredQuantityOf("Coffee");
		waterBefore = emptyLabHasIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		milkBefore = emptyLabHasIngredients.getStoredQuantityOf("Milk");
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(milkBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Milk"));
		assertEquals(waterBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				emptyLabHasIngredients.getStoredQuantityOf("Coffee mixed with Milk and Water"));
		
		coffeeBefore = fullLabNoIngredients.getStoredQuantityOf("Coffee");
		waterBefore = fullLabNoIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		milkBefore = fullLabNoIngredients.getStoredQuantityOf("Milk");
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				fullLabNoIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(milkBefore,
				fullLabNoIngredients.getStoredQuantityOf("Milk"));
		assertEquals(waterBefore,
				fullLabNoIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				fullLabNoIngredients.getStoredQuantityOf("Coffee mixed with Milk and Water"));
	}
	
	@Test
	public void testMixAutomaticthreeIngredients(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Coffee", 2);
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Milk", 3);
		
		Quantity coffeeBefore = fullLabIngredients.getStoredQuantityOf("Coffee");
		Quantity waterBefore = fullLabIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		Quantity milkBefore = fullLabIngredients.getStoredQuantityOf("Milk");
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(milkBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Milk"));
		assertEquals(waterBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(3*Quantity.SPOONAMOUNT), 
				fullLabIngredients.getStoredQuantityOf("Coffee mixed with Milk and Water"));
		
		coffeeBefore = emptyLabHasIngredients.getStoredQuantityOf("Coffee");
		waterBefore = emptyLabHasIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		milkBefore = emptyLabHasIngredients.getStoredQuantityOf("Milk");
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(milkBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Milk"));
		assertEquals(waterBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				emptyLabHasIngredients.getStoredQuantityOf("Coffee mixed with Milk and Water"));
		
		coffeeBefore = fullLabNoIngredients.getStoredQuantityOf("Coffee");
		waterBefore = fullLabNoIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		milkBefore = fullLabNoIngredients.getStoredQuantityOf("Milk");
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				fullLabNoIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(milkBefore,
				fullLabNoIngredients.getStoredQuantityOf("Milk"));
		assertEquals(waterBefore,
				fullLabNoIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				fullLabNoIngredients.getStoredQuantityOf("Coffee mixed with Milk and Water"));
	}

	
	@Test
	public void testMixAutomaticTwoIngredients(){
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Water", 1);
		emptyRecipe.addIngredientToRecipeAt("1 Spoon Coffee", 2);
		
		Quantity coffeeBefore = fullLabIngredients.getStoredQuantityOf("Coffee");
		Quantity waterBefore = fullLabIngredients.getStoredQuantityOf(Recipe.extractName(emptyRecipe.getIngredientAt(1)));
		assertTrue(fullLabIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(waterBefore.subStract(new Quantity(Quantity.SPOONAMOUNT)),
						fullLabIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(2*Quantity.SPOONAMOUNT), 
				fullLabIngredients.getStoredQuantityOf("Coffee mixed with Water"));
		
		coffeeBefore = emptyLabHasIngredients.getStoredQuantityOf("Coffee");
		waterBefore = emptyLabHasIngredients.getStoredQuantityOf("Water");
		assertFalse(emptyLabHasIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(waterBefore,
				emptyLabHasIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				emptyLabHasIngredients.getStoredQuantityOf("Coffee mixed with Water"));
		
		coffeeBefore = fullLabNoIngredients.getStoredQuantityOf("Coffee");
		waterBefore = fullLabNoIngredients.getStoredQuantityOf("Water");
		assertFalse(fullLabNoIngredients.excecuteRecipe(emptyRecipe));
		assertEquals(coffeeBefore,
				fullLabNoIngredients.getStoredQuantityOf("Coffee"));
		assertEquals(waterBefore,
				fullLabNoIngredients.getStoredQuantityOf("Water"));
		assertEquals(new Quantity(0), 
				fullLabNoIngredients.getStoredQuantityOf("Coffee mixed with Water"));
	}

}
