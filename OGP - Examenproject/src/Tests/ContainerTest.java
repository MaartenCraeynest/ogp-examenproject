package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Alchemy.*;

public class ContainerTest {
	IngredientContainer ingredientContainerTest, universalContainerEmpty, universalContainerFull, 
							universalContainerFullSecond;
	AlchemicIngredient water, waterNotInContainer;
	
	@Before
	public void setUp() throws Exception {
		universalContainerEmpty = new IngredientContainer();
		universalContainerFull = new IngredientContainer();
		universalContainerFullSecond = new IngredientContainer();
				
		water = new AlchemicIngredient(new Quantity(Quantity.BOTTLEAMOUNT));
		water.move(universalContainerFull);
		waterNotInContainer = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
	}

	@Test
	public void testIngredientContainer() {
		ingredientContainerTest = new IngredientContainer();
		assertTrue(ingredientContainerTest.hasProperIngredient());
		assertFalse(ingredientContainerTest.hasIngredient());
		assertTrue(ingredientContainerTest.getCapacity().isGreaterOrEqualTo(
										new Quantity(Quantity.MAXQUANTITY)));
		for  (States state : States.values())
			assertTrue(ingredientContainerTest.canContain(state));
	}

	@Test
	public void testCanContain() {
		for  (States state : States.values())
			assertTrue(universalContainerEmpty.canContain(state));
	}

	@Test
	public void testHasIngredient() {
		assertFalse(universalContainerEmpty.hasIngredient());
		assertTrue(universalContainerFull.hasIngredient());
	}

	@Test
	public void testHasProperIngredient() {
		assertTrue(universalContainerEmpty.hasProperIngredient());
		assertTrue(universalContainerFull.hasProperIngredient());
	}

	@Test
	public void testCanHaveAsIngredient() {
		assertTrue(universalContainerEmpty.canHaveAsIngredient(null));
		assertTrue(universalContainerEmpty.canHaveAsIngredient(water));
	}

	@Test
	public void testEmpty() {
		universalContainerEmpty.empty();
		assertEquals(null, universalContainerEmpty.getIngredient());
		
		universalContainerFull.empty();
		assertEquals(null, universalContainerFull.getIngredient());
		assertEquals(null, water.getContainer());
	}

	@Test
	public void testFill() {
		universalContainerEmpty.fill(waterNotInContainer);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testFillIllegalStateAlreadyInContainer() {
		universalContainerEmpty.fill(water);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testFillIllegalStateNullIngredient() {
		universalContainerEmpty.fill(null);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testFillIllegalStateContainerIsNotEmpty() {
		universalContainerFull.fill(waterNotInContainer);
	}
}
