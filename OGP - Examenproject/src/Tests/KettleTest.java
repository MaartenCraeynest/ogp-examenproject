package Tests;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Alchemy.*;

public class KettleTest {
	Laboratory emptyLab, fullLab;
	Kettle kettleNoLab, kettleTest, kettleInLab;
	AlchemicIngredient waterVial, waterBarrel;
	TransportContainer waterBarrelContainer, extractContainer,waterVialContainer;

	@Before
	public void setUp() throws Exception {
		emptyLab = new Laboratory(1);
		
		fullLab = new Laboratory(1);
		kettleInLab = new Kettle();
		kettleInLab.addTo(fullLab);
		
		kettleNoLab = new Kettle();
			
		waterVial = new AlchemicIngredient(new Quantity(Quantity.VIALAMOUNT));
		waterVialContainer = new TransportContainer(waterVial);
			
		waterBarrel = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
		waterBarrelContainer = new TransportContainer(waterBarrel);
		
		extractContainer = new TransportContainer(StdContainers.BARREL);		
	}

	@Test
	public void testAddIngredient() {
		kettleInLab.addIngredient(waterBarrelContainer);
		assertTrue(kettleInLab.getInputAt(1).hasIngredient());
		assertEquals(waterBarrel,kettleInLab.getInputAt(1).getIngredient());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddIngredientOverflow(){
		while (true){
			kettleInLab.addIngredient(new TransportContainer(
										new AlchemicIngredient(
											new Quantity(Quantity.BARRELAMOUNT))));
		}
	}

	@Test
	public void testProcessOneIngredient() {
		AlchemicIngredient ingr = new AlchemicIngredient(
				new Quantity(Quantity.BARRELAMOUNT));
		kettleInLab.addIngredient(new TransportContainer(ingr));
		kettleInLab.process();
		assertEquals(ingr, kettleInLab.getOutputAt(1).getIngredient());
	}
	
	@Test
	public void testProcessTwoIngredient() {
		AlchemicType state1 
			= new AlchemicTypeUnmixed("Nutella",States.LIQUID, new Temperature(10,0), 0.5);
		AlchemicType state2 
			= new AlchemicTypeUnmixed("Bread",States.POWDER, new Temperature(0,500), 0);
		AlchemicIngredient ingr1 = 
				new AlchemicIngredient(state1,
										States.LIQUID,
										new Quantity(Quantity.SPOONAMOUNT), 
										new Temperature(0,20));
		AlchemicIngredient ingr2 = 
				new AlchemicIngredient(state2,
										States.POWDER,
										new Quantity(Quantity.SACHELAMOUNT), 
										new Temperature(0,0));
		// Create clones for the ingredients because the original ones will be terminated
		AlchemicIngredient ingr1Clone = 
				new AlchemicIngredient(state1,
										States.LIQUID,
										new Quantity(Quantity.SPOONAMOUNT), 
										new Temperature(0,20));
		AlchemicIngredient ingr2Clone = 
				new AlchemicIngredient(state2,
										States.POWDER,
										new Quantity(Quantity.SACHELAMOUNT), 
										new Temperature(0,0));
		AlchemicIngredient[] ingredientList = new AlchemicIngredient[2];
		ingredientList[0] = ingr1Clone;
		ingredientList[1] = ingr2Clone;
		
		// add original ones to kettle
		kettleInLab.addIngredient(new TransportContainer(ingr1));
		kettleInLab.addIngredient(new TransportContainer(ingr2));
		
		kettleInLab.process();
		
		//compare
		AlchemicIngredient result = kettleInLab.getOutputAt(1).getIngredient();
		assertEquals(AlchemicTypeMixed.getEquivalentMixedState(ingredientList), result.getState());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedTemperature(ingredientList).getHotness(), result.getTemperature().getHotness());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedTemperature(ingredientList).getColdness(), result.getTemperature().getColdness());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedQuantity(ingredientList).getQuantity(), result.getQuantity().getQuantity());
		
		assertEquals(AlchemicTypeMixed.getEquivalentMixedType(ingredientList).getSimpleName(),result.getType().getSimpleName());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedType(ingredientList).getStandardTemperature().getColdness(),result.getType().getStandardTemperature().getColdness());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedType(ingredientList).getStandardTemperature().getHotness(),result.getType().getStandardTemperature().getHotness());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedType(ingredientList).getStandardState(),result.getType().getStandardState());
		// third argument is the allowed deviation (from rounding errors)
		assertEquals(AlchemicTypeMixed.getEquivalentMixedType(ingredientList).getTheoreticVolatility(),result.getType().getTheoreticVolatility(), 0.001);

	}
	
	@Test
	public void testProcessTwoIngredientEqualType() {
		AlchemicType type
			= new AlchemicTypeUnmixed("Nutella",States.LIQUID, new Temperature(10,0), 0.5);
		AlchemicIngredient ingr1 = 
				new AlchemicIngredient(type,
										States.LIQUID,
										new Quantity(Quantity.SPOONAMOUNT), 
										new Temperature(0,20));
		AlchemicIngredient ingr2 = 
				new AlchemicIngredient(type,
										States.POWDER,
										new Quantity(Quantity.SACHELAMOUNT), 
										new Temperature(0,0));
		// Create clones for the ingredients because the original ones will be terminated
		AlchemicIngredient ingr1Clone = 
				new AlchemicIngredient(type,
										States.LIQUID,
										new Quantity(Quantity.SPOONAMOUNT), 
										new Temperature(0,20));
		AlchemicIngredient ingr2Clone = 
				new AlchemicIngredient(type,
										States.POWDER,
										new Quantity(Quantity.SACHELAMOUNT), 
										new Temperature(0,0));
		AlchemicIngredient[] ingredientList = new AlchemicIngredient[2];
		ingredientList[0] = ingr1Clone;
		ingredientList[1] = ingr2Clone;
		
		// add original ones to kettle
		kettleInLab.addIngredient(new TransportContainer(ingr1));
		kettleInLab.addIngredient(new TransportContainer(ingr2));
		
		kettleInLab.process();
		
		//compare
		AlchemicIngredient result = kettleInLab.getOutputAt(1).getIngredient();
		assertTrue(result.getType() instanceof AlchemicTypeUnmixed);
		assertEquals(ingr1Clone.getType(), result.getType());
		assertEquals(ingr1Clone.getState(), result.getState());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedTemperature(ingredientList).getHotness(), result.getTemperature().getHotness());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedTemperature(ingredientList).getColdness(), result.getTemperature().getColdness());
		assertEquals(AlchemicTypeMixed.getEquivalentMixedQuantity(ingredientList).getQuantity(), result.getQuantity().getQuantity());
		
		// third argument is the allowed deviation (from rounding errors)
		assertEquals(AlchemicTypeMixed.getEquivalentMixedType(ingredientList).getTheoreticVolatility(),result.getType().getTheoreticVolatility(), 0.001);

	}

	@Test
	public void testAddTo() {
		kettleNoLab.addTo(emptyLab);
		assertEquals(emptyLab,kettleNoLab.getLaboratory());
		assertEquals(kettleNoLab,emptyLab.getKettle());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasLab() {
		kettleInLab.addTo(emptyLab);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasCoolingBox() {
		kettleNoLab.addTo(fullLab);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddToNull() {
		kettleNoLab.addTo(null);
	}

	@Test
	public void testRemoveFromLab() {
		kettleInLab.removeFromLab();
		assertFalse(kettleInLab.hasLaboratory());
		assertFalse(fullLab.hasKettle());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testRemoveFromLabNoLab() {
		kettleNoLab.removeFromLab();
	}

	@Test
	public void testTransmogifier() {
		kettleTest = new Kettle();
		assertEquals(kettleTest.getNbOfOutputs(), 0);
		assertEquals(kettleTest.getNbOfInputs(), 0);
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetOutputAt(){
		kettleInLab.getOutputAt(0);
	}
	
	@Test
	public void testGetNbOfInputs() {
		assertEquals(0, kettleInLab.getNbOfInputs());
		kettleInLab.addIngredient(waterBarrelContainer);
		assertEquals(1, kettleInLab.getNbOfInputs());
	}

	@Test
	public void testFindFirstEmpty() {
		assertEquals(1,kettleNoLab.findFirstEmpty());
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetInputAt(){
		kettleInLab.getOutputAt(0);
	}

	@Test
	public void testGetNbOfOutputs() {
		assertEquals(0, kettleInLab.getNbOfOutputs());
		kettleInLab.addIngredient(waterBarrelContainer);
		kettleInLab.process();
		assertEquals(1, kettleInLab.getNbOfOutputs());
	}

	@Test
	public void testExtractIngredient() {
		kettleInLab.addIngredient(waterBarrelContainer);
		kettleInLab.process();
		AlchemicIngredient ingr = kettleInLab.getOutputAt(1).getIngredient();
		kettleInLab.extractIngredient(extractContainer, 1);
		assertFalse(kettleInLab.getOutputAt(1).hasIngredient());
		assertTrue(ingr.hasContainer());
		extractContainer.hasIngredient();
	}
		
	@Test
	public void testHasLaboratory() {
		assertFalse(kettleNoLab.hasLaboratory());
		assertTrue(kettleInLab.hasLaboratory());
	}	
}
