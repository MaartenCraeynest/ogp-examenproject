package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Alchemy.*;

public class CoolingBoxTest {
	Laboratory emptyLab, fullLab;
	CoolingBox coolingBoxNoLab, coolingBoxInLab, coolingBoxTest;
	AlchemicIngredient waterVial, waterBarrel;
	TransportContainer waterContainer, extractContainer;

	@Before
	public void setUp() throws Exception {
		emptyLab = new Laboratory(1);
		
		fullLab = new Laboratory(1);
		coolingBoxInLab = new CoolingBox(new Temperature(0,0));
		coolingBoxInLab.addTo(fullLab);
		
		coolingBoxNoLab = new CoolingBox(new Temperature(0,0));
		
		waterVial = new AlchemicIngredient(new Quantity(Quantity.VIALAMOUNT));
		waterContainer = new TransportContainer(waterVial);
		
		coolingBoxInLab.addIngredient(waterContainer);
		
		waterBarrel = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
		waterContainer = new TransportContainer(waterBarrel);
		
		extractContainer = new TransportContainer(StdContainers.BARREL);
	}

	@Test
	public void testProcess() {
		coolingBoxInLab.process();
		assertEquals(coolingBoxInLab.getTemperature(),coolingBoxInLab.getOutputAt(1).getIngredient().getTemperature());
		AlchemicIngredient ingr= coolingBoxInLab.getOutputAt(1).getIngredient();
		coolingBoxInLab.process();
		assertTrue(ingr.isTerminated());
		// Check that the output get cleared after each process
		assertFalse(coolingBoxInLab.getOutputAt(1).hasIngredient());
	}

	@Test
	public void testAddTo() {
		coolingBoxNoLab.addTo(emptyLab);
		assertEquals(emptyLab,coolingBoxNoLab.getLaboratory());
		assertEquals(coolingBoxNoLab,emptyLab.getCoolingBox());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasLab() {
		coolingBoxInLab.addTo(emptyLab);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasCoolingBox() {
		coolingBoxNoLab.addTo(fullLab);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddToNull() {
		coolingBoxNoLab.addTo(null);
	}

	@Test
	public void testRemoveFromLab() {
		coolingBoxInLab.removeFromLab();
		assertFalse(coolingBoxInLab.hasLaboratory());
		assertFalse(fullLab.hasCoolingBox());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testRemoveFromLabNoLab() {
		coolingBoxNoLab.removeFromLab();
	}

	@Test
	public void testCoolingBox() {
		coolingBoxTest = new CoolingBox(new Temperature(0,0));
		assertEquals(coolingBoxTest.getNbOfOutputs(), 0);
		assertEquals(coolingBoxTest.getNbOfInputs(), 0);
		assertEquals(coolingBoxTest.getTemperature().getHotness(), 0);
		assertEquals(coolingBoxTest.getTemperature().getColdness(), 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testCoolingBoxNull() {
		coolingBoxTest = new CoolingBox(null);
	}

	@Test
	public void testIsValidTemperature() {
		assertTrue(CoolingBox.isValidTemperature(new Temperature(0,0)));
		assertFalse(CoolingBox.isValidTemperature(null));
	}

	@Test
	public void testGetNbOfInputs() {
		assertEquals(0, coolingBoxNoLab.getNbOfInputs());
		assertEquals(1, coolingBoxInLab.getNbOfInputs());
	}

	@Test
	public void testAddIngredient() {
		coolingBoxInLab.addIngredient(waterContainer);
		assertTrue(waterVial.isTerminated());
		assertTrue(coolingBoxInLab.getInputAt(1).hasIngredient());
		assertEquals(waterBarrel,coolingBoxInLab.getInputAt(1).getIngredient());
	}

	@Test
	public void testFindFirstEmpty() {
		assertEquals(1,coolingBoxNoLab.findFirstEmpty());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testFindFirstEmptyNoFree() {
		assertEquals(1,coolingBoxInLab.findFirstEmpty());
	}

	@Test
	public void testGetNbOfOutputs() {
		assertEquals(0, coolingBoxNoLab.getNbOfOutputs());
		coolingBoxInLab.process();
		assertEquals(1, coolingBoxInLab.getNbOfOutputs());
	}

	@Test
	public void testExtractIngredient() {
		coolingBoxInLab.process();
		AlchemicIngredient ingr = coolingBoxInLab.getOutputAt(1).getIngredient();
		coolingBoxInLab.extractIngredient(extractContainer, 1);
		assertFalse(coolingBoxInLab.getOutputAt(1).hasIngredient());
		assertTrue(ingr.hasContainer());
		extractContainer.hasIngredient();
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testExtractIngredientEmpty() {
		coolingBoxInLab.extractIngredient(extractContainer, 1);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testExtractIngredientTooSmall() {
		coolingBoxInLab.process();
		coolingBoxInLab.extractIngredient(new TransportContainer(StdContainers.SPOON), 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testExtractIngredientNull() {
		coolingBoxInLab.process();
		coolingBoxInLab.extractIngredient(null, 1);
	}
	
	@Test
	public void testHasLaboratory() {
		assertFalse(coolingBoxNoLab.hasLaboratory());
		assertTrue(coolingBoxInLab.hasLaboratory());
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetOutputAt(){
		coolingBoxInLab.getOutputAt(0);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetInputAt(){
		coolingBoxInLab.getOutputAt(0);
	}
	
}
