package Tests;
import Alchemy.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class QuantityTest {
	Quantity oneDrop, oneSpoon;
	Quantity quantityTest, oneSpoonOneVialOneBottle, incrementLiquid, incrementSolid;
	@Before
	public void setUp() throws Exception {
		oneDrop 		= new Quantity(Quantity.DROPAMOUNT);
		oneSpoon		= new Quantity(Quantity.SPOONAMOUNT);
		
		oneSpoonOneVialOneBottle = new Quantity(Quantity.SPOONAMOUNT + Quantity.VIALAMOUNT + Quantity.BOTTLEAMOUNT);
		incrementLiquid = new Quantity(Quantity.DROPAMOUNT + 2*Quantity.SPOONAMOUNT + 1*Quantity.VIALAMOUNT + 2*Quantity.BOTTLEAMOUNT + 1*Quantity.JUGAMOUNT+
										2*Quantity.BARRELAMOUNT + 3*Quantity.STOREROOMAMOUNT);
		incrementSolid = new Quantity(Quantity.PINCHAMOUNT + 2*Quantity.SPOONAMOUNT + 3*Quantity.SACHELAMOUNT + 2*Quantity.BOXAMOUNT + 1*Quantity.SACKAMOUNT +
										2*Quantity.CHESTAMOUNT+ 3* Quantity.STOREROOMAMOUNT);
		
	}


	@Test
	public void testQuantity() {
		quantityTest = new Quantity(112358132134L);
		assertEquals(quantityTest.getQuantity(), 112358132134L);
	}

	@Test
	public void testIsGreaterOrEqualTo() {
		assertFalse(oneDrop.isGreaterOrEqualTo(oneSpoon));
		assertTrue(oneDrop.isGreaterOrEqualTo(oneDrop));
		assertTrue(oneSpoon.isGreaterOrEqualTo(oneDrop));
	}

	@Test
	public void testAdd() {
		assertEquals(oneSpoon.add(oneSpoon).getQuantity(), 2*Quantity.SPOONAMOUNT);
	}

	@Test
	public void testSubStract() {
		assertEquals(oneSpoon.subStract(oneDrop).getQuantity(), Quantity.SPOONAMOUNT-Quantity.DROPAMOUNT);
	}

	@Test
	public void testRoundTo() {
		assertEquals(oneDrop.roundTo(Quantity.SPOONAMOUNT).getQuantity(), 0);
		Quantity result = oneSpoonOneVialOneBottle.roundTo(Quantity.VIALAMOUNT);
		assertEquals(result.getExactNbOfSpoonsLiquid(), 0);
		assertEquals(result.getExactNbOfVials(), 1);
		assertEquals(result.getExactNbOfBottles(), 1);
	}
	
	@Test
	public void testEquals() {
		assertFalse(oneDrop.equals(new Object()));
		assertFalse(oneDrop.equals(null));
		assertTrue(oneDrop.equals(new Quantity(Quantity.DROPAMOUNT)));
		assertFalse(oneDrop.equals(oneSpoon));
	}

	@Test
	public void testGetExactNbOfDrops() {
		assertEquals(incrementLiquid.getExactNbOfDrops(), 1);
	}

	@Test
	public void testGetExactNbOfSpoonsLiquid() {
		assertEquals(incrementLiquid.getExactNbOfSpoonsLiquid(), 2);
	}

	@Test
	public void testGetExactNbOfVials() {
		assertEquals(incrementLiquid.getExactNbOfVials(), 1);
	}

	@Test
	public void testGetExactNbOfBottles() {
		assertEquals(incrementLiquid.getExactNbOfBottles(), 2);
	}

	@Test
	public void testGetExactNbOfJugs() {
		assertEquals(incrementLiquid.getExactNbOfJugs(), 1);
	}
	
	@Test
	public void testGetExactNbOfBarrels(){
		assertEquals(incrementLiquid.getExactNbOfBarrels(), 2);
	}

	@Test
	public void testGetExactNbOfStorerooms() {
		assertEquals(incrementLiquid.getExactNbOfStorerooms(), 3);
	}

	@Test
	public void testGetExactNbOfPinches() {
		assertEquals(incrementSolid.getExactNbOfPinches(), 1);
	}

	@Test
	public void testGetExactNbOfSpoonsSolid() {
		assertEquals(incrementSolid.getExactNbOfSpoonsSolid(), 2);
	}

	@Test
	public void testGetExactNbOfSachels() {
		assertEquals(incrementSolid.getExactNbOfSachels(), 3);
	}

	@Test
	public void testGetExactNbOfBoxes() {
		assertEquals(incrementSolid.getExactNbOfBoxes(), 2);
	}

	@Test
	public void testGetExactNbOfSacks() {
		assertEquals(incrementSolid.getExactNbOfSacks(), 1);
	}

	@Test
	public void testGetExactNbOfChests() {
		assertEquals(incrementSolid.getExactNbOfChests(), 2);
	}

}
