package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Alchemy.*;

public class StdContainersTest {
	@Test
	public void testGetBestFit() {
		assertEquals(StdContainers.SPOON,
				StdContainers.getBestFit(new AlchemicIngredient(
						new Quantity(Quantity.SPOONAMOUNT))));
		assertEquals(StdContainers.VIAL,
				StdContainers.getBestFit(new AlchemicIngredient(
						new Quantity(Quantity.VIALAMOUNT))));
		assertEquals(StdContainers.BOTTLE,
				StdContainers.getBestFit(new AlchemicIngredient(
						new Quantity(Quantity.BOTTLEAMOUNT))));
		assertEquals(StdContainers.JUG,
				StdContainers.getBestFit(new AlchemicIngredient(
						new Quantity(Quantity.JUGAMOUNT))));
		assertEquals(StdContainers.BARREL,
				StdContainers.getBestFit(new AlchemicIngredient(
						new Quantity(Quantity.BARRELAMOUNT))));
		
		AlchemicTypeUnmixed solidtype = new AlchemicTypeUnmixed("Cheetos",States.POWDER,new Temperature(0,20),0);
		assertEquals(StdContainers.SPOON,
				StdContainers.getBestFit(
						new AlchemicIngredient( solidtype,
												solidtype.getStandardState(),
												new Quantity(Quantity.SPOONAMOUNT),
												solidtype.getStandardTemperature())));
		
		assertEquals(StdContainers.SACHEL,
				StdContainers.getBestFit(
						new AlchemicIngredient( solidtype,
												solidtype.getStandardState(),
												new Quantity(Quantity.SACHELAMOUNT),
												solidtype.getStandardTemperature())));
		
		assertEquals(StdContainers.BOX,
				StdContainers.getBestFit(
						new AlchemicIngredient( solidtype,
												solidtype.getStandardState(),
												new Quantity(Quantity.BOXAMOUNT),
												solidtype.getStandardTemperature())));
		
		assertEquals(StdContainers.SACK,
				StdContainers.getBestFit(
						new AlchemicIngredient( solidtype,
												solidtype.getStandardState(),
												new Quantity(Quantity.SACKAMOUNT),
												solidtype.getStandardTemperature())));
		
		assertEquals(StdContainers.CHEST,
				StdContainers.getBestFit(
						new AlchemicIngredient( solidtype,
												solidtype.getStandardState(),
												new Quantity(Quantity.CHESTAMOUNT),
												solidtype.getStandardTemperature())));
		
	}

	@Test
	public void testBiggestContainerOf() {
		assertEquals(Quantity.BARRELAMOUNT,StdContainers.biggestContainerOf(States.LIQUID).getQuantity());
		assertEquals(Quantity.CHESTAMOUNT, StdContainers.biggestContainerOf(States.POWDER).getQuantity());
	}

}
