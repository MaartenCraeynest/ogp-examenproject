package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Alchemy.*;

public class TransportContainerTest {
	AlchemicIngredient waterBottle, waterBarrel, terminatedIngredient, waterWithoutContainer;
	TransportContainer bottle, barrel, emptyBottle, emptyChest, emptyBarrel, emptySpoon,terminatedContainer,
						transportContainerStdContainerTest, transportContainerIngredient, transportContainerTest;
	
	@Before
	public void setUp() throws Exception {
		waterBottle = new AlchemicIngredient(new Quantity(Quantity.BOTTLEAMOUNT));
		waterBarrel = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
		waterWithoutContainer = new AlchemicIngredient(new Quantity(Quantity.VIALAMOUNT));
		
		bottle = new TransportContainer(StdContainers.BOTTLE);
		barrel = new TransportContainer(StdContainers.BARREL);
		
		waterBottle.move(bottle);
		waterBarrel.move(barrel);
		
		emptyBottle = new TransportContainer(StdContainers.BOTTLE);
		emptyBarrel = new TransportContainer(StdContainers.BARREL);
		emptySpoon  = new TransportContainer(StdContainers.SPOON);
		emptyChest  = new TransportContainer(StdContainers.CHEST);
		
		terminatedContainer = new TransportContainer(StdContainers.JUG);
		terminatedContainer.terminate();
		
		terminatedIngredient = new AlchemicIngredient(new Quantity(Quantity.DROPAMOUNT));
		terminatedIngredient.terminate();
	}

	@Test
	public void testCanHaveAsIngredient() {
		assertFalse(emptyBottle.canHaveAsIngredient(waterBarrel));
		assertTrue(terminatedContainer.canHaveAsIngredient(null));
		assertFalse(terminatedContainer.canHaveAsIngredient(waterBarrel));
		assertFalse(emptyChest.canHaveAsIngredient(waterBarrel));
	}

	@Test
	public void testTransportContainerStdContainers() {
		transportContainerStdContainerTest = new TransportContainer(StdContainers.BARREL);
		assertEquals(StdContainers.BARREL.getCapacity(),transportContainerStdContainerTest.getCapacity());
		for (States state : StdContainers.BARREL.getAllowedStates())
			assertTrue(transportContainerStdContainerTest.canContain(state));
		assertFalse(transportContainerStdContainerTest.hasIngredient());
	}
	
	@Test
	public void testTransportContainerAlchemicIngredient() {
		transportContainerIngredient = new TransportContainer(waterWithoutContainer);
		assertEquals(StdContainers.VIAL.getCapacity(), transportContainerIngredient.getCapacity());
		assertTrue(transportContainerIngredient.canContain(waterWithoutContainer.getState()));
		assertEquals(transportContainerIngredient,  waterWithoutContainer.getContainer());
		assertEquals(waterWithoutContainer, transportContainerIngredient.getIngredient());
	}

	@Test
	public void testTerminate() {
		emptyBottle.terminate();
		assertTrue(emptyBottle.isTerminated());
		assertFalse(emptyBottle.hasIngredient());
		
		bottle.terminate();
		assertTrue(emptyBottle.isTerminated());
		assertFalse(emptyBottle.hasIngredient());
		assertFalse(waterBottle.hasContainer());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testTerminateTerminated() {
		terminatedContainer.terminate();
	}

	@Test
	public void testCanContain() {
		assertTrue(emptyBottle.canContain(States.LIQUID));
		assertFalse(emptyBottle.canContain(States.POWDER));
		
		assertFalse(emptyChest.canContain(States.LIQUID));
		assertTrue(emptyChest.canContain(States.POWDER));
		
		assertTrue(emptySpoon.canContain(States.LIQUID));
		assertTrue(emptySpoon.canContain(States.POWDER));
	}

	@Test
	public void testHasIngredient() {
		assertFalse(emptySpoon.hasIngredient());
		assertTrue(bottle.hasIngredient());
	}

	@Test
	public void testHasProperIngredient() {
		assertTrue(emptySpoon.hasProperIngredient());
		assertTrue(bottle.hasProperIngredient());
		assertTrue(terminatedContainer.hasProperIngredient());
	}

	@Test
	public void testEmpty() {
		emptyBarrel.empty();
		assertFalse(emptyBarrel.hasIngredient());
		
		bottle.empty();
		assertFalse(bottle.hasIngredient());
		assertFalse(waterBottle.hasContainer());
		
		terminatedContainer.empty();
		assertFalse(emptyBarrel.hasIngredient());
	}

	@Test
	public void testFill() {
		emptyBarrel.fill(waterWithoutContainer);
		assertEquals(emptyBarrel, waterWithoutContainer.getContainer());
		assertEquals(waterWithoutContainer, emptyBarrel.getIngredient());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testFillNull(){
		emptyBarrel.fill(null);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testFillHasContainer(){
		emptyBarrel.fill(waterBarrel);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testFillHasIngredient(){
		barrel.fill(waterWithoutContainer);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testFillCannotHaveIngredient(){
		emptySpoon.fill(waterWithoutContainer);
	}
}
