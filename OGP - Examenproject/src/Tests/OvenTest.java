package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Alchemy.*;

public class OvenTest {
	Laboratory emptyLab, fullLab;
	Oven ovenNoLab, ovenInLab, ovenTest;
	AlchemicIngredient waterVial, waterBarrel;
	TransportContainer waterContainer, extractContainer;

	@Before
	public void setUp() throws Exception {
		emptyLab = new Laboratory(1);
		
		fullLab = new Laboratory(1);
		ovenInLab = new Oven(new Temperature(0,0));
		ovenInLab.addTo(fullLab);
		
		ovenNoLab = new Oven(new Temperature(0,0));
		
		waterVial = new AlchemicIngredient(new Quantity(Quantity.VIALAMOUNT));
		waterContainer = new TransportContainer(waterVial);
		
		ovenInLab.addIngredient(waterContainer);
		
		waterBarrel = new AlchemicIngredient(new Quantity(Quantity.BARRELAMOUNT));
		waterContainer = new TransportContainer(waterBarrel);
		
		extractContainer = new TransportContainer(StdContainers.BARREL);		
	}

	@Test
	public void testProcess() {
		Temperature originalT = ovenInLab.getInputAt(1).getIngredient().getTemperature();
		//oven is colder than ingredient
		ovenInLab.process();
		assertEquals(originalT, ovenInLab.getOutputAt(1).getIngredient().getTemperature());
		ovenInLab.getOutputAt(1).getIngredient().move(ovenInLab.getInputAt(1));
		ovenInLab.setTemperature(originalT.heat(1000));
		
		//oven is hotter than ingredient
		ovenInLab.process();
		assertTrue(ovenInLab.getTemperature().getHotness()*0.95 <= ovenInLab.getOutputAt(1).getIngredient().getTemperature().getHotness());
		assertTrue(ovenInLab.getTemperature().getColdness()*0.95 <= ovenInLab.getOutputAt(1).getIngredient().getTemperature().getColdness());
		
		assertTrue((ovenInLab.getTemperature().getHotness()*1.05 >= ovenInLab.getOutputAt(1).getIngredient().getTemperature().getHotness()) ||
						ovenInLab.getTemperature().getHotness()*1.05 > Temperature.getMaximumTemperature());
		assertTrue((ovenInLab.getTemperature().getColdness()*1.05 >= ovenInLab.getOutputAt(1).getIngredient().getTemperature().getColdness()) ||
						ovenInLab.getTemperature().getColdness()*1.05 > Temperature.getMaximumTemperature());;	
		AlchemicIngredient ingr= ovenInLab.getOutputAt(1).getIngredient();
		ovenInLab.process();
		assertTrue(ingr.isTerminated());
		// Check that the output get cleared after each process
		assertFalse(ovenInLab.getOutputAt(1).hasIngredient());
	}

	@Test
	public void testAddTo() {
		ovenNoLab.addTo(emptyLab);
		assertEquals(emptyLab,ovenNoLab.getLaboratory());
		assertEquals(ovenNoLab,emptyLab.getOven());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasLab() {
		ovenInLab.addTo(emptyLab);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testAddToHasOven() {
		ovenNoLab.addTo(fullLab);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAddToNull() {
		ovenNoLab.addTo(null);
	}

	@Test
	public void testRemoveFromLab() {
		ovenInLab.removeFromLab();
		assertFalse(ovenInLab.hasLaboratory());
		assertFalse(fullLab.hasOven());
	}

	@Test
	public void testOven() {
		ovenTest = new Oven(new Temperature(0,0));
		assertEquals(ovenTest.getNbOfOutputs(), 0);
		assertEquals(ovenTest.getNbOfInputs(), 0);
		assertEquals(ovenTest.getTemperature().getHotness(), 0);
		assertEquals(ovenTest.getTemperature().getColdness(), 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOvenNull() {
		ovenTest = new Oven(null);
	}

	@Test
	public void testAddIngredient() {
		ovenInLab.addIngredient(waterContainer);
		assertTrue(waterVial.isTerminated());
		assertTrue(ovenInLab.getInputAt(1).hasIngredient());
		assertEquals(waterBarrel,ovenInLab.getInputAt(1).getIngredient());
	}

	@Test
	public void testIsValidTemperature() {
		assertTrue(Oven.isValidTemperature(new Temperature(0,0)));
		assertFalse(Oven.isValidTemperature(null));
	}

	@Test
	public void testGetNbOfInputs() {
		assertEquals(0, ovenNoLab.getNbOfInputs());
		assertEquals(1, ovenInLab.getNbOfInputs());
	}

	@Test
	public void testFindFirstEmpty() {
		assertEquals(1,ovenNoLab.findFirstEmpty());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testFindFirstEmptyNoFree() {
		assertEquals(1,ovenInLab.findFirstEmpty());
	}

	@Test
	public void testGetNbOfOutputs() {
		assertEquals(0, ovenNoLab.getNbOfOutputs());
		ovenInLab.process();
		assertEquals(1, ovenInLab.getNbOfOutputs());
	}

	@Test
	public void testExtractIngredient() {
		ovenInLab.process();
		AlchemicIngredient ingr = ovenInLab.getOutputAt(1).getIngredient();
		ovenInLab.extractIngredient(extractContainer, 1);
		assertFalse(ovenInLab.getOutputAt(1).hasIngredient());
		assertTrue(ingr.hasContainer());
		extractContainer.hasIngredient();
	}

	@Test (expected = IllegalArgumentException.class)
	public void testExtractIngredientEmpty() {
		ovenInLab.extractIngredient(extractContainer, 1);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testExtractIngredientTooSmall() {
		ovenInLab.process();
		ovenInLab.extractIngredient(new TransportContainer(StdContainers.SPOON), 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testExtractIngredientNull() {
		ovenInLab.process();
		ovenInLab.extractIngredient(null, 1);
	}

	@Test
	public void testHasLaboratory() {
		assertFalse(ovenNoLab.hasLaboratory());
		assertTrue(ovenInLab.hasLaboratory());
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetOutputAt(){
		ovenInLab.getOutputAt(0);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testGetInputAt(){
		ovenInLab.getOutputAt(0);
	}
}
