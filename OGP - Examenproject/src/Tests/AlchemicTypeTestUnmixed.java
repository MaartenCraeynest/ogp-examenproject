package Tests;
import Alchemy.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AlchemicTypeTestUnmixed {
	AlchemicTypeUnmixed alch;
	States liquid;
	Temperature temp;
	@Before
	public void setUp() throws Exception {
		liquid=States.LIQUID;
		temp=new Temperature(0,50);
		alch=new AlchemicTypeUnmixed("Name",liquid,temp,0);
	}
	@Test
	public void testStringStatesTemperatureDoubleLegalCase(){
		assertEquals(alch.getSimpleName(),"Name");
		assertEquals(alch.getStandardState(),liquid);
		assertEquals(alch.getStandardTemperature(),temp);
		assertTrue(alch.getTheoreticVolatility()==0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleIllegalName(){
		new AlchemicTypeUnmixed("name",liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleNullTemp(){
		new AlchemicTypeUnmixed("name",liquid,null,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleNullState(){
		new AlchemicTypeUnmixed("name",null,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleIllegalTheoreticVolatility(){
		new AlchemicTypeUnmixed("Name",liquid,temp,50);

	}
	@Test 
	public void testConstructorLegalCase(){
		String c="Een Test";
		AlchemicTypeUnmixed nametest=new AlchemicTypeUnmixed(c,liquid,temp,0);
		assertEquals(c,nametest.getSimpleName());
		String e="Abc";
		nametest=new AlchemicTypeUnmixed(e,liquid,temp,0);
		assertEquals(e,nametest.getSimpleName());
		String g="Don't A())";
		nametest=new AlchemicTypeUnmixed(g,liquid,temp,0);
		assertEquals(g,nametest.getSimpleName());
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseShort(){
		String f="Ab";
		new AlchemicTypeUnmixed(f,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseInvalid(){
		String a="1234";
		new AlchemicTypeUnmixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseWhite(){
		String a="				";
		new AlchemicTypeUnmixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCasePartShort(){
		String a="Test T";
		new AlchemicTypeUnmixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseCapital(){
		String a="test";
		new AlchemicTypeUnmixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseAddition(){
		String a="Cooled";
		new AlchemicTypeUnmixed(a,liquid,temp,0);
	}
	@Test
	public void testCanHaveAsSimpleNameUnmixed() {
		String a="a";
		String b="    ";
		String c="Een Test";
		String d="Een Foute T";
		String e="Abc";
		String f="Ab";
		String g="Don't A())";
		String h="4587";
		String i="test";
		String j="Test mixed with Test";
		String k="Test  Spaties";
		String l="Cooled";
		assertFalse(alch.canHaveAsSimpleName(a));
		assertFalse(alch.canHaveAsSimpleName(b));
		assertTrue(alch.canHaveAsSimpleName(c));
		assertFalse(alch.canHaveAsSimpleName(d));
		assertTrue(alch.canHaveAsSimpleName(e));
		assertFalse(alch.canHaveAsSimpleName(f));
		assertTrue(alch.canHaveAsSimpleName(g));
		assertFalse(alch.canHaveAsSimpleName(h));
		assertFalse(alch.canHaveAsSimpleName(i));
		assertFalse(alch.canHaveAsSimpleName(j));
		assertFalse(alch.canHaveAsSimpleName(k));
		assertFalse(alch.canHaveAsSimpleName(l));
	}
	
	@Test
	public void testIsValidVolatility(){
		assertTrue(AlchemicType.isValidTheoreticVolatility(0));
		assertFalse(AlchemicType.isValidTheoreticVolatility(-0.1));
	}
	
	@Test
	public void testIsValidStandardTemperature(){
		assertTrue(AlchemicType.isValidStandardTemperature(temp));
		assertFalse(AlchemicType.isValidStandardTemperature(null));
	}
	
	@Test
	public void testIsValidStandardState(){
		assertTrue(AlchemicType.isValidStandardState(States.LIQUID));
		assertFalse(AlchemicType.isValidStandardState(null));
	}


  }
