package Tests;
import Alchemy.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class AlchemicTypeTestMixed {
	AlchemicTypeMixed alch;
	AlchemicTypeMixed special;
	States liquid;
	Temperature temp;
	Temperature standard;
	@Before
	public void setUp() throws Exception {
	liquid=States.LIQUID;
	temp=new Temperature(0,50);
	alch=new AlchemicTypeMixed("Name",liquid,temp,0);
	special= new AlchemicTypeMixed("Name mixed with Water",liquid,temp,0,"Special");
	standard=new Temperature(0,20);
	}
	@Test
	public void testStringStatesTemperatureDoubleLegalCase(){
		assertEquals(alch.getSimpleName(),"Name");
		assertEquals(alch.getStandardState(),liquid);
		assertEquals(alch.getStandardTemperature(),temp);
		assertTrue(alch.getTheoreticVolatility()==0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleNullTemp(){
		new AlchemicTypeUnmixed("name",liquid,null,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleNullState(){
		new AlchemicTypeUnmixed("name",null,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleIllegalName(){
		new AlchemicTypeMixed("name",liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleIllegalTheoreticVolatility(){
		new AlchemicTypeMixed("Name",liquid,temp,50);
	}
	@Test 
	public void testConstructorNameLegalCase(){
		String c="Een Test";
		AlchemicTypeMixed nametest=new AlchemicTypeMixed(c,liquid,temp,0);
		assertEquals(c,nametest.getSimpleName());
		String e="Abc";
		nametest=new AlchemicTypeMixed(e,liquid,temp,0);
		assertEquals(e,nametest.getSimpleName());
		String g="Don't A())";
		nametest=new AlchemicTypeMixed(g,liquid,temp,0);
		assertEquals(g,nametest.getSimpleName());
		String o="Ice mixed with Something, Test and Water";
		nametest=new AlchemicTypeMixed(o,liquid,temp,0);
		assertEquals(o,nametest.getSimpleName());
		String j="Something mixed with Test";
		nametest=new AlchemicTypeMixed(j,liquid,temp,0);
		assertEquals(j,nametest.getSimpleName());
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseShort(){
		String f="Ab";
		new AlchemicTypeMixed(f,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseInvalid(){
		String a="1234";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseWhite(){
		String a="				";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCasePartShort(){
		String a="Test T";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseCapital(){
		String a="test";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseAddition(){
		String a="Cooled";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseWithComma(){
		String a="Test, Name mixed with Water";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseAndBeforeComma(){
		String a="Test and Name, Water";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseDoubleWith(){
		String a="Something mixed with Something mixed with Something";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testConstructorIllegalCaseDoubleAnd(){
		String a="Something and Something and Something";
		new AlchemicTypeMixed(a,liquid,temp,0);
	}
	@Test
	public void testStringStatesTemperatureDoubleStringLegalCase(){
		assertEquals(special.getSimpleName(),"Name mixed with Water");
		assertEquals(alch.getStandardState(),liquid);
		assertEquals(alch.getStandardTemperature(),temp);
		assertTrue(alch.getTheoreticVolatility()==0);
		assertEquals(special.getSpecialName(),"Special");
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleStringIllegalName(){
		new AlchemicTypeMixed("Name mixed with Water",liquid,temp,0,"special");
	}
	@Test(expected=IllegalArgumentException.class)
	public void testStringStatesTemperatureDoubleStringIllegalTheoreticVolatility(){
		new AlchemicTypeMixed("Name mixed with Water",liquid,temp,50,"Special");
	}
	@Test
	public void testCanHaveAsSimpleNameMixed(){
		String a="a";
		String b="    ";
		String c="Een Test";
		String d="Een Foute T";
		String e="Abc";
		String f="Ab";
		String g="Don't A())";
		String h="4587";
		String i="test";
		String j="Something mixed with Test";
		String k="Spaties  Test";
		String l="Cooled";
		String m="Water mixed with Water mixed with Water";
		String n="Water and Water and Water";
		String o="Apples mixed with Ice, Oranges and Water";
		String p="Test and Test and Test";
		String q="Xyz mixed with Abc";		
		String r="Milk and Something mixed with Test";
		String s="Milk, Something mixed with Test";
		String t="Potatoes mixed with Tomatoes, Water";
		String u="Milk and Something, Test";
		assertFalse(alch.canHaveAsSimpleName(a));
		assertFalse(alch.canHaveAsSimpleName(b));
		assertTrue(alch.canHaveAsSimpleName(c));
		assertFalse(alch.canHaveAsSimpleName(d));
		assertTrue(alch.canHaveAsSimpleName(e));
		assertFalse(alch.canHaveAsSimpleName(f));
		assertTrue(alch.canHaveAsSimpleName(g));
		assertFalse(alch.canHaveAsSimpleName(h));
		assertFalse(alch.canHaveAsSimpleName(i));
		assertTrue(alch.canHaveAsSimpleName(j));
		assertFalse(alch.canHaveAsSimpleName(k));
		assertFalse(alch.canHaveAsSimpleName(l));
		assertFalse(alch.canHaveAsSimpleName(m));
		assertFalse(alch.canHaveAsSimpleName(n));
		assertTrue(alch.canHaveAsSimpleName(o));
		assertFalse(alch.canHaveAsSimpleName(p));
		assertFalse(alch.canHaveAsSimpleName(q));
		assertFalse(alch.canHaveAsSimpleName(r));
		assertFalse(alch.canHaveAsSimpleName(s));
		assertFalse(alch.canHaveAsSimpleName(t));
		assertFalse(alch.canHaveAsSimpleName(u));
	}
	
	@Test 
	public void testSetSpecialNameLegalCase(){
		String c="Een Test";
		alch.setSpecialName(c);
		assertEquals(c,alch.getSpecialName());
		String e="Abc";
		alch.setSpecialName(e);
		assertEquals(e,alch.getSpecialName());
		String g="Don't A())";
		alch.setSpecialName(g);
		assertEquals(g,alch.getSpecialName());
	}
	@Test (expected=IllegalArgumentException.class)
	public void testSetSpecialNameIllegalCaseShort(){
		String f="Ab";
		alch.setSpecialName(f);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testSetSpecialNameIllegalCaseInvalid(){
		String a="1234";
		alch.setSpecialName(a);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testSetSpecialNameIllegalCaseWhite(){
		String a="				";
		alch.setSpecialName(a);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testSetSpecialNameIllegalCasePartShort(){
		String a="Test T";
		alch.setSpecialName(a);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testSetSpecialNameIllegalCaseCapital(){
		String a="test";
		alch.setSpecialName(a);
	}
	@Test(expected=IllegalArgumentException.class)
	public void testSetSpecialNameIllegalCaseAddition(){
		String a="Cooled";
		alch.setSpecialName(a);
	}
	
	@Test
	public void testCanHaveAsSpecialName(){
		assertTrue(alch.canHaveAsSimpleName("Specialname"));
		assertFalse(alch.canHaveAsSpecialName(null));
		assertTrue(alch.canHaveAsSpecialName(""));
		assertFalse(alch.canHaveAsSpecialName("Heated"));
	}
	@Test 
	public void testGetEquivalentMixedName(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,temp,0),liquid,new Quantity(24),temp);
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",liquid,temp,0),liquid, new Quantity(24),temp);
		AlchemicIngredient[] ingredients = {alchone,alchtwo};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedName(ingredients),"Name mixed with Water");
		AlchemicIngredient[] alchingr={alchone,alchmix};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedName(alchingr),"Bcd mixed with Name, Nop and Xyz");
		AlchemicIngredient[] ingredients2={alchmix,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedName(ingredients2),"Bcd mixed with Efg, Hij, Klm, Nop and Xyz");
		AlchemicIngredient[] ingredientsall={alchone,alchtwo,alchmix,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedName(ingredientsall),"Bcd mixed with Efg, Hij, Klm, Name, Nop, Water and Xyz");
		AlchemicIngredient[] ingredientsame={alchone,alchone};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedName(ingredientsame), "Name");
		AlchemicIngredient[] ingredients1={alchone};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedName(ingredients1), "Name");

	}
	@Test 
	public void  testGetEquivalentMixedStandardState(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,new Temperature(Temperature.getMaximumTemperature(),0),0),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",States.POWDER,standard,0),States.POWDER, new Quantity(24),standard);
		AlchemicIngredient[] alchpowder={alchone,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedStandardState(alchpowder), States.POWDER);
		AlchemicIngredient[] alchliquid={alchtwo,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedStandardState(alchliquid), States.LIQUID);
		AlchemicIngredient[] ingredientsone={alchmix};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedStandardState(ingredientsone), States.LIQUID);
		AlchemicIngredient[] ingredientliquid={alchmix,alchtwo};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedStandardState(ingredientliquid), States.LIQUID);
		AlchemicIngredient[] ingredientliquid2={alchone,alchtwo};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedStandardState(ingredientliquid2), States.LIQUID);
		AlchemicIngredient[] multiple={alchone,alchmix,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedStandardState(multiple),States.POWDER);
	}

	@Test 
	public void testGetEquivalentMixedStandardTemperature(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchthree=new AlchemicIngredient(new AlchemicTypeMixed("Test",liquid,new Temperature(0,60),0),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,new Temperature(Temperature.getMaximumTemperature(),0),0),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",States.POWDER,new Temperature(10,0),0),States.POWDER, new Quantity(24),new Temperature(10,0));
		AlchemicIngredient[] alchhot={alchone,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(alchhot).equals( temp));
		AlchemicIngredient[] alchcold={alchtwo,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(alchcold).equals(standard));
		AlchemicIngredient[] ingredientsone={alchmix};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(ingredientsone).equals( new Temperature(Temperature.getMaximumTemperature(),0)));
		AlchemicIngredient[] ingredientcold={alchmix2,alchthree};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(ingredientcold).equals( new Temperature(10,0)));
		AlchemicIngredient[] ingredientcold2={alchone,alchtwo};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(ingredientcold2).equals( new Temperature(0,20)));
		AlchemicIngredient[] multiple={alchone,alchmix,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(multiple).equals(new Temperature(0,50)));
	}
	@Test 
	public void testGetEquivalentMixedTheoreticVolatility(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,new Temperature(Temperature.getMaximumTemperature(),0),.5),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",States.POWDER,standard,1),States.POWDER, new Quantity(24),standard);
		AlchemicIngredient[] vol={alchmix,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTheoreticVolatility(vol)==.75);
		AlchemicIngredient[] vol2={alchone,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTheoreticVolatility(vol2)==.5);
		AlchemicIngredient[] vol3={alchone};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTheoreticVolatility(vol3)==0);
		AlchemicIngredient[] vol4={alchone,alchtwo};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTheoreticVolatility(vol4)==0);
		AlchemicIngredient[] vol5={alchone,alchtwo,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTheoreticVolatility(vol5)==.25);
		
		
	}

	@Test 
	public void  testGetEquivalentMixedState(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,new Temperature(Temperature.getMaximumTemperature(),0),0),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",States.POWDER,new Temperature(0,20),0),States.POWDER, new Quantity(24),new Temperature(0,20));
		AlchemicIngredient[] alchpowder={alchone,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedState(alchpowder), States.POWDER);
		AlchemicIngredient[] alchliquid={alchtwo,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedState(alchliquid), States.LIQUID);
		AlchemicIngredient[] ingredientsone={alchmix};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedState(ingredientsone), States.LIQUID);
		AlchemicIngredient[] ingredientliquid={alchmix,alchtwo};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedState(ingredientliquid), States.LIQUID);
		AlchemicIngredient[] ingredientliquid2={alchone,alchtwo};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedState(ingredientliquid2), States.LIQUID);
		AlchemicIngredient[] multiple={alchone,alchmix,alchmix2};
		assertEquals(AlchemicTypeMixed.getEquivalentMixedState(multiple),States.POWDER);
	}

	@Test 
	public void testGetEquivalentMixedTemperature(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,new Temperature(Temperature.getMaximumTemperature(),0),.5),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",States.POWDER,standard,1),States.POWDER, new Quantity(24),new Temperature(10,0));
		AlchemicIngredient[] temp={alchmix,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTemperature(temp).equals(new Temperature(Math.round((24*Temperature.getMaximumTemperature()+24*10)/48),0)));
		AlchemicIngredient[] temp2={alchone,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTemperature(temp2).equals(new Temperature(0,20)));
		AlchemicIngredient[] temp3={alchone};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTemperature(temp3).equals(new Temperature(0,50)));
		AlchemicIngredient[] temp4={alchone,alchtwo};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTemperature(temp4).equals(new Temperature(0,30)));
		AlchemicIngredient[] temp5={alchone,alchtwo,alchmix};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedTemperature(temp5).equals(new Temperature(Math.round((24*Temperature.getMaximumTemperature()-24*50-48*20)/96),0)));
	}

	@Test 
	public void  testGetEquivalentMixedQuantity(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchthree=new AlchemicIngredient(alch,liquid,new Quantity(40),temp);
		AlchemicIngredient alchfour=new AlchemicIngredient(alch,States.POWDER,new Quantity(40),temp);
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,new Temperature(Temperature.getMaximumTemperature(),0),.5),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",States.POWDER,standard,1),States.POWDER, new Quantity(5*Quantity.PINCHAMOUNT),new Temperature(10,0));
		AlchemicIngredient[] quantity={alchone,alchtwo};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity).getQuantity()==72);
		AlchemicIngredient[] quantity2={alchone,alchtwo,alchmix};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity2).getQuantity()==96);
		AlchemicIngredient[] quantity3={alchone,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity3).getQuantity()==54);
		AlchemicIngredient[] quantity4={alchtwo,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity4).getQuantity()==72);
		AlchemicIngredient[] quantity5={alchone};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity5).getQuantity()==24);
		AlchemicIngredient[] quantity6={alchthree,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity6).getQuantity()==54);
		AlchemicIngredient[] quantity7={alchfour,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity7).getQuantity()==70);
		AlchemicIngredient[] quantity8={alchthree,alchone};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedQuantity(quantity8).getQuantity()==64);
		
	}

	@Test 
	public void testGetEquivalentMixedVolatilityDeviation(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient alchmix=new AlchemicIngredient(new AlchemicTypeMixed("Bcd mixed with Nop and Xyz",liquid,new Temperature(Temperature.getMaximumTemperature(),0),.5),liquid,new Quantity(24),new Temperature(Temperature.getMaximumTemperature(),0));
		AlchemicIngredient alchmix2=new AlchemicIngredient(new AlchemicTypeMixed("Efg mixed with Hij and Klm",States.POWDER,standard,1),States.POWDER, new Quantity(24),standard);
		AlchemicIngredient[] vol={alchmix,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(vol)==(alchmix.getEffectiveVolatilityDeviation()*24+24*alchmix2.getEffectiveVolatilityDeviation())/48);
		AlchemicIngredient[] vol2={alchone,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(vol2)==(alchone.getEffectiveVolatilityDeviation()*24+24*alchmix2.getEffectiveVolatilityDeviation())/48);
		AlchemicIngredient[] vol3={alchone};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(vol3)==(alchone.getEffectiveVolatilityDeviation()));
		AlchemicIngredient[] vol4={alchone,alchtwo};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(vol4)==(alchone.getEffectiveVolatilityDeviation()*24+48*alchtwo.getEffectiveVolatilityDeviation())/72);
		AlchemicIngredient[] vol5={alchone,alchtwo,alchmix2};
		assertTrue(AlchemicTypeMixed.getEquivalentMixedVolatilityDeviation(vol5)==(alchone.getEffectiveVolatilityDeviation()*24+48*alchtwo.getEffectiveVolatilityDeviation()+alchmix2.getEffectiveVolatilityDeviation()*24)/96);
	}
		
	@Test
	public void testGetEquivalentMixedType(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient[] ingredients={alchone,alchtwo};
		AlchemicType a= AlchemicTypeMixed.getEquivalentMixedType(ingredients);
		assertTrue(a.getSimpleName().equals(AlchemicTypeMixed.getEquivalentMixedName(ingredients)));
		assertTrue(a.getStandardTemperature().equals(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(ingredients)));
		assertTrue(a.getStandardState().equals(AlchemicTypeMixed.getEquivalentMixedStandardState(ingredients)));
		assertTrue(a.getTheoreticVolatility()==AlchemicTypeMixed.getEquivalentMixedTheoreticVolatility(ingredients));
		
	}
	@Test
	public void testGetEquivalentMixedWithSpecialNameType(){
		AlchemicIngredient alchone=new AlchemicIngredient(alch,liquid,new Quantity(24),temp);
		AlchemicIngredient alchtwo=new AlchemicIngredient(new Quantity(48));
		AlchemicIngredient[] ingredients={alchone,alchtwo};
		AlchemicTypeMixed a= AlchemicTypeMixed.getEquivalentMixedTypeWithSpecialName(ingredients,"Special");
		assertTrue(a.getSimpleName().equals(AlchemicTypeMixed.getEquivalentMixedName(ingredients)));
		assertTrue(a.getStandardTemperature().equals(AlchemicTypeMixed.getEquivalentMixedStandardTemperature(ingredients)));
		assertTrue(a.getStandardState().equals(AlchemicTypeMixed.getEquivalentMixedStandardState(ingredients)));
		assertTrue(a.getTheoreticVolatility()==AlchemicTypeMixed.getEquivalentMixedTheoreticVolatility(ingredients));
		assertEquals(a.getSpecialName(),"Special");
		
	}
	
	
	
}

